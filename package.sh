#!/bin/bash
# Packages the generated executable with the required libraries and resources

getJSON() {
	python3 -c "import sys, json; print(json.load(sys.stdin)$1)"
}

source config.sh

GAME_NAME=`cat config.json | getJSON '["game_name"]'`
VERSION_MAJOR=`cat config.json | getJSON '["game_version"][0]'`
VERSION_MINOR=`cat config.json | getJSON '["game_version"][1]'`
VERSION_PATCH=`cat config.json | getJSON '["game_version"][2]'`

build_dir="build"
if [ -n "$1" ]; then
	build_dir="$1"
fi

package_dir="pkg"
if [ "$package_dir" != "." ]; then
	mkdir -p $package_dir
fi

package_file="$GAME_NAME-$VERSION_MAJOR.$VERSION_MINOR.$VERSION_PATCH-build$(date +%Y.%m.%d).tar.xz"
if [ -n "$2" ]; then
	package_file="$2-build$(date +%Y.%m.%d).tar.xz"
	if [[ $2 == engine* ]]; then
		GAME_NAME="$2"
	fi
fi

if [ -d "$GAME_NAME" ]; then
	echo "Failed to package: temporary directory \"$GAME_NAME\" already exists"
	exit 1
fi

./build.sh norun "$build_dir"

echo "Copying files..."

mkdir "$GAME_NAME"
mkdir -p "$GAME_NAME/bee/"

cp "$build_dir/$EXECUTABLE" "$GAME_NAME/$EXECUTABLE"
if [ "$(cat $build_dir/last_build_type.txt)" == "release" ]; then
	strip "$GAME_NAME/$EXECUTABLE"
fi

cat << "EOF" > "$GAME_NAME/start.sh"
#!/bin/bash
cd $(dirname $0)
./engine
EOF
chmod +x "$GAME_NAME/start.sh"

if [[ $2 != engine* ]]; then
	cp "./config.json" "$GAME_NAME/"
	cp -r "./main/" "$GAME_NAME/"
	cp -r "./maps/" "$GAME_NAME/"
fi
cp -r "./bee/resources" "$GAME_NAME/bee/"

# Copy dynamic libraries
mkdir "$GAME_NAME/lib"
libraries="$(ldd $build_dir/$EXECUTABLE | grep so | grep '=>' | sed -e '/^[^\t]/ d' | sed -e 's/\t//' | sed -e 's/.*=..//' | sed -e 's/ (0.*)//' | uniq | sort | xargs basename -a)"
: 'libraries="
libarchive.so.13
libassimp.so.4
libc.so.6
libcrypto.so.1.1
libcurl.so.4
libfreetype.so.6
libGLEW.so.2.1
libglut.so.3
liblzma.so.5
libm.so.6
libpcre.so.1
libpng16.so.16
libpthread.so.0
libSDL2-2.0.so.0
libSDL2_image-2.0.so.0
libSDL2_mixer-2.0.so.0
libSDL2_net-2.0.so.0
libSDL2_ttf-2.0.so.0
libssh2.so.1
libssl.so.1.1
libstdc++.so.6
libz.so.1
"'
for l in $libraries; do
	cp "/usr/lib/$l" "$GAME_NAME/lib/"
done

# Copy Python libraries
mkdir -p "$GAME_NAME/lib/cpython/build"
cp -r "./lib/cpython/build/lib.linux-x86_64-3.7" "$GAME_NAME/lib/cpython/build/lib.linux-x86_64-3.7"
cp -r "./lib/cpython/Lib" "$GAME_NAME/lib/cpython/Lib/"
# Remove bulky test module data
rm -r "$GAME_NAME/lib/cpython/Lib/__pycache__"
rm -r "$GAME_NAME/lib/cpython/Lib/test"

echo -n "Compressing"
tar --checkpoint=.100 -cJf "$package_dir/$package_file" "$GAME_NAME/"
echo -e "\n$GAME_NAME packaged in \"$package_dir/$package_file\"!"

if [ "$(cat $build_dir/last_build_type.txt)" == "debug" ]; then
	echo "Warning: packaged a debug build, was this desired?"
fi

rm -r "$GAME_NAME"

exit 0
