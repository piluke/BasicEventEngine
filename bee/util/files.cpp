/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

// File handling functions

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

#include "files.hpp" // Include the function definitions

#include "platform.hpp" // Include the required platform-specific file operation functions
#include "real.hpp"

namespace util {
	namespace internal {
		std::string temp_dir;
	}
	std::string internal::dir_get_temp() {
		return temp_dir;
	}

/**
* @param fname the filename to check
*
* @returns whether the given filename is the location of a file
*/
bool file_exists(const std::string& fname) {
	return std::ifstream(fname).good();
}
/**
* @param fname the filename to check
*
* @returns the size of the given file in bytes
*/
size_t file_size(const std::string& fname) {
	if (file_exists(fname)) {
		return platform::file_size(fname);
	}
	return 0;
}
/**
* Delete the given file.
* @param fname the name of the file to delete
*
* @retval 0 success
* @retval nonzero failed to remove file
*/
int file_delete(const std::string& fname) {
	return platform::remove(fname);
}
/**
* Rename the given file.
* @param old_fname the original name of the given file
* @param new_fname the new name of the given file
*
* @retval 0 success
* @retval nonzero failed to rename file
*/
int file_rename(const std::string& old_fname, const std::string& new_fname) {
	return rename(old_fname.c_str(), new_fname.c_str());
}
/**
* Copy the given file to a new file.
* @param fname the original file to copy
* @param new_fname the file to be copied to
*
* @retval 0 success
* @retval 1 failed to open source file
* @retval 1 failed to open destination file
*/
int file_copy(const std::string& fname, const std::string& new_fname) {
	// Open the original file in binary mode
	std::ifstream source (fname, std::ios::binary);
	if (!source.is_open()) {
		return 1;
	}

	// Open the copy in binary mode
	std::ofstream dest (new_fname, std::ios::binary);
	if (!dest.is_open()) {
		return 2;
	}

	dest << source.rdbuf(); // Read the entire file from the original to the copy

	// Close both files
	source.close();
	dest.close();

	return 0;
}
/**
* @param fname the file to read
*
* @returns a string of the contents of the given file or an empty string if it could not be opened
*/
std::string file_get_contents(const std::string& fname) {
	// Open the given file for reading
	std::ifstream input (fname, std::ios::binary);
	if (!input.is_open()) {
		std::cerr << "UTIL FILES Failed to open \"" << fname << "\" for reading\n"; // Output an error if the file could not be opened
		return "";
	}

	std::ostringstream contents;
	contents << input.rdbuf();

	input.close();

	return contents.str();
}
/**
* Write the given string to the given file.
* @param fname the file to write to
* @param contents the string to write to the file
*
* @returns the amount of bytes written
*/
std::streamoff file_put_contents(const std::string& fname, const std::string& contents) {
	// Open the given file for writing
	std::ofstream output (fname, std::ios::binary);
	if (!output.is_open()) {
		std::cerr << "UTIL FILES Failed to open \"" << fname << "\" for writing\n"; // Output an error if the file could not be opened
		return 0;
	}

	output.write(contents.c_str(), contents.size()); // Write to the file
	std::streamoff n = output.tellp();

	output.close(); // Close the file

	return n;
}
/**
* @param fname the file to compute the checksum of
*
* @returns the checksum of the file data
*/
unsigned int file_get_checksum(const std::string& fname) {
	if (!file_exists(fname)) {
		return 0;
	}

	std::string f = file_get_contents(fname);
	std::vector<unsigned char> v;
	for (auto& c : f) {
		v.push_back(c);
	}

	return checksum::get(v);
}

/**
* @param dirname the path of the directory to check
*
* @returns whether the given directory exists
*/
bool directory_exists(const std::string& dirname) {
	return (platform::dir_exists(dirname) == 1);
}
/**
* Create a directory at the given path.
* @param dirname the path of the new directory
*
* @retval 0 success
* @retval -1 failed to create directory
*/
int directory_create(const std::string& dirname) {
	return platform::mkdir(dirname, 0755);
}
/**
* @returns the path of a temporary directory to store temporary game files in
*/
std::string directory_get_temp() {
	std::string& d (internal::temp_dir);
	if ((d.empty())||(!directory_exists(d))) {
		d = platform::mkdtemp("/tmp/bee-XXXXXX"); // Call the cross-platform mkdtemp function with the format specifier
		if (!d.empty()) {
			d += "/";
		}
	}
	return d;
}
/**
* @param dirname the path of the directory to list
*
* @returns the names of the files contained in the directory
* @note In order to recursively discover files, use this function in combination with directory_exists()
*/
std::vector<std::string> directory_get_files(const std::string& dirname) {
	return platform::dir_list(dirname);
}
/**
* Get the checksum of the directory by appending the checksums of its files.
* @param dirname the path of the directory to compute the checksum of
* @param include the names of the files to include, if empty include all
*
* @returns the checksum of the directory data
*/
unsigned int directory_get_checksum(const std::string& dirname, const std::set<std::string>& include) {
	if (!directory_exists(dirname)) {
		return 0;
	}

	std::vector<unsigned char> d;
	std::vector<std::string> fnames = directory_get_files(dirname);
	for (auto& f : fnames) {
		if ((!include.empty())&&(include.count(f) == 0)) {
			continue;
		}

		std::string path (dirname+"/"+f);
		std::string cs;
		if (directory_exists(path)) {
			cs = std::to_string(directory_get_checksum(path));
		} else {
			cs = std::to_string(file_get_checksum(path));
		}

		for (auto& c : cs) {
			d.push_back(c);
		}
		d.push_back('\n');
	}

	return checksum::get(d);
}
/**
* Get the checksum of the directory by appending the checksums of its files.
* @param dirname the path of the directory to compute the checksum of
*
* @returns the checksum of the directory data
*/
unsigned int directory_get_checksum(const std::string& dirname) {
	return directory_get_checksum(dirname, {});
}

/**
* @note For the path "/tmp/bee.log", the basename is "bee.log".
* @param path the path to evaluate
*
* @returns the basename of the given path
*/
std::string file_basename(const std::string& path) {
	return {
		std::find_if(path.rbegin(), path.rend(), // Iterating in reverse over the path,
			[] (char c) {
				return ((c == '/')||(c == '\\')); // Return the first character (from the end) that is a slash, i.e. the end of a directory
			}
		).base(),
		path.end()
	}; // Return the string from the last directory to the end
}
/**
* @note For the path "/tmp/bee.log", the dirname is "/tmp/".
* @note For a path with no slashes, the dirname is "." (the current directory).
* @param path the path to evaluate
*
* @returns the dirname of the given path
*/
std::string file_dirname(const std::string& path) {
	// Find the last slash
	size_t fslash = path.rfind("/");
	size_t bslash = path.rfind("\\");
	if ((fslash == std::string::npos)&&(bslash == std::string::npos)) {
		return ".";
	}

	// Return the string from the beginning to the last directory
	if (fslash == std::string::npos) {
		return path.substr(0, bslash);
	}
	return path.substr(0, fslash);
}
/**
* @note For the path "/tmp/bee.log", the plain name is "/tmp/bee"
* @param path the path to evaluate
*
* @returns the plain name of the given path
*/
std::string file_plainname(const std::string& path) {
	// Find the last period
	size_t period = path.rfind(".");
	if (period == std::string::npos) {
		return path;
	}

	return path.substr(0, period);
}
/**
* @note For the path "/tmp/bee.log", the extension is ".log".
* @param path the path to evaluate
*
* @returns the extension of the given path
*/
std::string file_extname(const std::string& path) {
	// Find the last period
	size_t period = path.rfind(".");
	if (period == std::string::npos) {
		return "";
	}

	return path.substr(period);
}

}
