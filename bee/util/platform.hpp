/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_UTIL_PLATFORM_H
#define BEE_UTIL_PLATFORM_H 1

#include <string>
#include <vector>

#ifdef _WIN32
	typedef int mode_t;
#endif

namespace util {
	enum class E_PLATFORM {
		UNKNOWN,
		LINUX,
		WINDOWS,
		MACOS
	};
namespace platform {

E_PLATFORM get_platform();

std::string get_path();
int setenv(const std::string&, const std::string&, int);

size_t file_size(const std::string&);
int remove(const std::string&);
int dir_exists(const std::string&);
std::vector<std::string> dir_list(const std::string&);
int mkdir(const std::string&, mode_t);
std::string mkdtemp(const std::string&);

std::string inet_ntop(const void*);

bool has_commandline_input();
void commandline_color(std::ostream*, int);
void commandline_color_reset(std::ostream*);
void commandline_clear();

}}

#endif // BEE_UTIL_PLATFORM_H
