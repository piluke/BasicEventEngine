/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include "client.hpp"

namespace bee {
	NetworkClient::NetworkClient() :
		sock(nullptr),
		channel(-1),
		last_recv(0),
		id(-1),
		name()
	{}
	NetworkClient::NetworkClient(UDPsocket _sock, int _channel) :
		sock(_sock),
		channel(_channel),
		last_recv(0),
		id(-1),
		name()
	{}
}
