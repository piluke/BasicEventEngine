/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include "event.hpp"

namespace bee {
	NetworkEvent::NetworkEvent() :
		type(E_NETEVENT::NONE),
		id(-1),
		data()
	{}
	NetworkEvent::NetworkEvent(E_NETEVENT _type) :
		type(_type),
		id(-1),
		data()
	{}
}
