/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include "console.hpp"

namespace bee { namespace console {
	void reset() {}

	int internal::init() {
		return -1;
	}
	int internal::init_ui() {
		return -1;
	}
	void internal::close() {}

	ScriptInterface* internal::get_interface() {
		return nullptr;
	}
	void internal::update_ui() {}

	void internal::handle_input(const SDL_Event* e) {}
	int internal::run(const std::string& command, bool is_silent) {
		return -1;
	}
	std::vector<Variant> internal::complete(Instance* textentry, const std::string& input) {
		return {};
	}
	void internal::resize() {}
	void internal::draw() {}

	void open() {}
	void close() {}
	void toggle() {}
	bool get_is_open() {
		return false;
	}
	void clear() {}

	bool has_var(const std::string& name) {
		return false;
	}
	void set_var(const std::string& name, const Variant& value) {}
	Variant get_var(const std::string& name) {
		return Variant();
	}
	int add_command(const std::string& name, std::function<Variant (const std::vector<Variant>&)> func) {
		return -1;
	}

	int run(const std::string& command) {
		return -1;
	}
	void log(E_MESSAGE type, const std::string& str) {}
}}
