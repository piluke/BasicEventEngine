/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include <sstream>
#include <algorithm>
#include <regex>

#include <GL/glew.h> // Include the required OpenGL headers
#include <SDL2/SDL_opengl.h>
#include "../util/windefine.hpp"

#include <Python.h>

#include "console.hpp"

#include "../engine.hpp"

#include "../util/real.hpp"
#include "../util/string.hpp"

#include "../init/gameoptions.hpp"

#include "../messenger/messenger.hpp"

#include "instance.hpp"
#include "enginestate.hpp"
#include "rooms.hpp"
#include "window.hpp"

#include "../input/kb.hpp"
#include "../input/keybind.hpp"

#include "../render/drawing.hpp"
#include "../render/renderer.hpp"
#include "../render/rgba.hpp"
#include "../render/viewport.hpp"

#include "../resource/font.hpp"
#include "../resource/script.hpp"
#include "../resource/room.hpp"

#include "../python/python.hpp"
#include "../python/interface.hpp"

#include "../ui/ui.hpp"

#include "../resources/headers.hpp"

namespace bee { namespace console {
	namespace internal {
		struct ConsoleCommand {
			PyObject_HEAD
			std::function<Variant (const std::vector<Variant>&)> func;
			PyMethodDef ml;

			static void del(PyObject* obj) {
				delete reinterpret_cast<ConsoleCommand*>(obj);
			}
		};
		PyTypeObject ConsoleCommandType = [] () {
			PyTypeObject cct = {};
			cct.tp_name = "bee.internal.ConsoleCommandType";
			cct.tp_basicsize = sizeof(ConsoleCommand);
			cct.tp_dealloc = reinterpret_cast<destructor>(ConsoleCommand::del);
			cct.tp_flags = Py_TPFLAGS_DEFAULT;
			cct.tp_doc = "ConsoleCommand objects";
			return cct;
		}();

		bool is_open = false;

		Script* scr_console = nullptr;

		std::vector<std::string> history;
		int history_index = -1;

		std::stringstream log;
		TextData* td_log = nullptr;
		size_t page_index = 0;

		// Set the drawing sizes
		SDL_Rect rect = {0, 0, 800, 500};
		unsigned int char_height = 20;
		unsigned int char_width = 10;

		Instance* ui_handle = nullptr;
		Instance* ui_text_entry = nullptr;

		std::map<std::string,ConsoleCommand*> commands;
	}

	/**
	* Reset the console log.
	*/
	void reset() {
		internal::history.clear();
		internal::history_index = -1;

		internal::log.clear();
		internal::page_index = 0;

		internal::rect = {0, 0, 800, 500};
		internal::char_height = 20;
		internal::char_width = 10;

		internal::ui_text_entry->ev_update();
		internal::ui_text_entry->ev_io(Variant(std::vector<Variant>{"set_input", ""}));
		internal::ui_text_entry->ev_io(Variant(std::vector<Variant>{"reset_completion"}));

		internal::update_ui();

		if (internal::td_log != nullptr) {
			delete internal::td_log;
			internal::td_log = nullptr;
		}
	}

	/**
	* Initialize the console and the default commands.
	* @retval 0 success
	* @retval 1 failed to load console Script
	*/
	int internal::init() {
		scr_console = Script::add("scr_console", "$/console.py");
		if (scr_console == nullptr) {
			return 1;
		}

 		// Store the default drawing line dimensions for later console drawing operations
		char_height = engine->font_default->get_string_height();
		char_width = engine->font_default->get_string_width();

		// Register the console logger
		messenger::internal::register_protected("consolelog", {"engine", "console"}, true, [] (const MessageContents& msg) {
			log << msg.descr << "\n";
		});

		return 0;
	}
	/**
	* Init the console ui.
	* @retval 0 success
	* @retval 1 failed to create a UI element
	*/
	int internal::init_ui() {
		ui_handle = bee::ui::create_handle(rect.x, rect.y, rect.w, 10, nullptr);
		if (ui_handle == nullptr) {
			return 1;
		}

		ui_handle->set_is_persistent(true);
		ui_handle->set_data("is_visible", false);
		ui_handle->set_data("is_relative", false);
		ui_handle->set_data("color", Variant(std::vector<Variant>{
			Variant('\x00'),
			Variant('\x00'),
			Variant('\x00'),
			Variant('\xff')
		}));

		ui_text_entry = bee::ui::create_text_entry(rect.x, rect.y+rect.h, 1, 80, [] (Instance* text_entry, const std::string& input) {
			// Reset the console state
			text_entry->ev_update();
			text_entry->ev_io(Variant(std::vector<Variant>{"set_input", ""}));
			text_entry->ev_io(Variant(std::vector<Variant>{"reset_completion"}));
			text_entry->set_data("has_focus", true);
			page_index = 0;

			bee::console::run(input); // Run the command
		});
		if (ui_text_entry == nullptr) {
			return 1;
		}

		ui_text_entry->set_is_persistent(true);
		ui_text_entry->set_data("input_tmp", Variant(""));
		ui_text_entry->set_data("is_visible", false);
		ui_text_entry->set_data("is_relative", false);
		ui_text_entry->set_data("color", Variant(std::vector<Variant>{
			Variant('\x7f'),
			Variant('\x7f'),
			Variant('\x7f'),
			Variant('\x7f')
		}));

		bee::ui::add_text_entry_completor(ui_text_entry, [] (Instance* text_entry, const std::string& input) -> std::vector<Variant> {
			return complete(text_entry, input);
		});
		bee::ui::add_text_entry_handler(ui_text_entry, [] (Instance* text_entry, const std::string& input, const SDL_Event* e) {
			switch (e->key.keysym.sym) {
				case SDLK_UP: { // The up arrow cycles through completion commands or reverse history
					text_entry->ev_update();

					std::vector<Variant> completions = text_entry->get_data("completions").v;
					if (completions.size() > 1) { // If a command is being completed
						int completion_index = text_entry->get_data("completion_index").i;
						if (completion_index > 0) { // If a completion command is already selected, lower the index and set the input line to the given command
							completion_index = util::fit_bounds(completion_index-1, 0, static_cast<int>(completions.size())-1);
							text_entry->ev_io(Variant(std::vector<Variant>{"set_input", *(completions.begin()+completion_index)}));
							text_entry->set_data("completion_index", Variant(completion_index));
						} else { // If the first completion command is selected, reset the input line to the previous user input
							text_entry->set_data("completion_index", Variant(-1));
							text_entry->ev_io(Variant(std::vector<Variant>{"set_input", text_entry->get_data("input_tmp")}));
						}
					} else { // If a command is not being completed, cycle through history
						if (history.size() > 0) { // If there is a history to look up
							history_index = util::fit_bounds(history_index+1, 0, static_cast<int>(history.size())-1); // Prevent the index from going past the end
							// Replace the command line with the history item
							text_entry->ev_io(Variant(std::vector<Variant>{"set_input", (history.rbegin()+history_index)->c_str()}));
						}
					}
					break;
				}
				case SDLK_DOWN: { // The down arrow cycles through the forward history
					text_entry->ev_update();

					std::vector<Variant> completions = text_entry->get_data("completions").v;
					if (completions.size() > 1) { // If a command is being completed
						int completion_index = text_entry->get_data("completion_index").i;
						if (completion_index == -1) { // If not completion command has been selected, store the previous user input
							text_entry->set_data("input_tmp", Variant(input));
						}
						// Raise the index and set the input line to the given command
						completion_index = util::fit_bounds(completion_index+1, 0, static_cast<int>(completions.size())-1);
						text_entry->ev_io(Variant(std::vector<Variant>{"set_input", *(completions.begin()+completion_index)}));
						text_entry->set_data("completion_index", Variant(completion_index));
					} else { // If a command is not being completed, cycle through history
						if (history_index > 0) { // If the index is in previous history
							history_index = util::fit_bounds(history_index-1, 0, static_cast<int>(history.size())-1); // Prevent the index from going past the front
							// Replace the command line with the history item
							text_entry->ev_io(Variant(std::vector<Variant>{"set_input", (history.rbegin()+history_index)->c_str()}));
						} else { // If the index is new history
							text_entry->ev_io(Variant(std::vector<Variant>{"set_input", ""}));
							history_index = -1;
						}
					}
					break;
				}
				case SDLK_ESCAPE: {
					text_entry->ev_update();
					text_entry->ev_io(Variant(std::vector<Variant>{"set_input", ""}));
					text_entry->ev_io(Variant(std::vector<Variant>{"reset_completion"}));
					break;
				}
				default: {
					// Remove bind key from the input
					if ((e->key.keysym.sym == kb::get_keybind("ConsoleToggle").key)&&(!input.empty())) {
						std::string ks (SDL_GetKeyName(e->key.keysym.sym));
						if ((ks.length() == 1)&&(input.back() == ks.front())) {
							text_entry->ev_update();
							text_entry->ev_io(Variant(std::vector<Variant>{"set_input", input.substr(0, input.length()-1).c_str()}));
						}
					}
				}
			}
		});

		resize();

		return 0;
	}
	/**
	* Close the console subsystem and free its memory.
	* @note For some reason this function has documentation from other functions below.
	*/
	void internal::close() {
		// Remove all added commands
		while (!commands.empty()) {
			const std::string& cmd_name (commands.begin()->first);
			if (remove_command(cmd_name)) {
				messenger::send({"engine", "console"}, E_MESSAGE::ERROR, "Failed to remove some console commands including \"" + cmd_name + "\"");
				break;
			}
		}

		if (scr_console == nullptr) {
			delete scr_console;
			scr_console = nullptr;
		}

		// Destroy the UI elements
		Room* room = get_current_room();
		if (room != nullptr) {
			ui_handle->set_is_persistent(false);
			ui_text_entry->set_is_persistent(false);
			room->destroy(ui_text_entry);
			room->destroy();
			ui_handle = nullptr;
			ui_text_entry = nullptr;
		} else {
			ui_handle = nullptr;
			ui_text_entry = nullptr;
		}
	}

	/**
	* @returns the ScriptInterface for the console Script
	*/
	ScriptInterface* internal::get_interface() {
		return scr_console->get_interface();
	}
	/**
	* Update the UI visibility to match the console visibility.
	*/
	void internal::update_ui() {
		ui_text_entry->set_data("has_focus", is_open);
	}

	/**
	* Handle the input as a console keypress.
	* @param e the keyboard input event
	*/
	void internal::handle_input(const SDL_Event* e) {
		if ((e->key.state == SDL_PRESSED)&&(e->key.repeat == 0)) {
			// Handle certain key presses in order to manipulate history or the command line
			switch (e->key.keysym.sym) {
				case SDLK_PAGEUP: { // The pageup key scrolls backward through the console log
					++page_index; // This value is checked against the page amount in internal::draw()
					break;
				}
				case SDLK_PAGEDOWN: { // The pagedown key scrolls forward through the console log
					if (page_index > 0) { // Limit the page index to greater than 0
						--page_index;
					}
					break;
				}
			}

			// Handle the console toggle KeyBind
			if (e->key.keysym.sym == kb::get_keybind("ConsoleToggle").key) {
				toggle();
			}
		}
	}
	/**
	* Run a command in the console
	* @param command the command string to run, including the arguments
	* @param is_silent whether to append the command to history and display it in console
	*
	* @returns the script's return value
	* @see Script::run_string() and PythonScriptInterface::run_string()
	*/
	int internal::run(const std::string& command, bool is_silent) {
		std::string tc (util::trim(command));
		// If the command is not silent, then log it
		if (!is_silent) {
			if ((!tc.empty())&&(command[0] != ' ')) { // If the first character is not a space, then add it to the console history
				// Fetch the last command if one exists
				std::string last_command = "";
				if (!history.empty()) {
					last_command = history.back();
				}

				if (tc != last_command) { // If the current command is not identical to the previous command, then add it to the console history
					history.push_back(tc);
				}
			}

			if (!get_option("is_headless").i) {
				messenger::send({"engine", "console"}, E_MESSAGE::INFO, "> " + tc); // Output the command to the messenger log
			}
		}
		history_index = -1; // Reset the history index
		if (tc.empty()) {
			return -1;
		}

		// Run the command
		Variant value;
		int r = scr_console->run_string(command, &value);
		if (r == 0) {
			if (value.get_type() != E_DATA_TYPE::NONE) {
				messenger::send({"engine", "console"}, E_MESSAGE::INFO, value.to_str(true));
			} else if (value.p != nullptr) {
				// FIXME: ugly and probably only works for Python
				PyObject* str = PyObject_Str(python::variant_to_pyobj(value));
				std::string _str (PyUnicode_AsUTF8(str));
				messenger::send({"engine", "console"}, E_MESSAGE::INFO, _str);
			}
		} else if (r > 1) {
			messenger::send({"engine", "console"}, E_MESSAGE::ERROR, python::get_traceback());
		}

		return r;
	}
	/**
	* Complete console commands.
	* @param textentry the console text entry to complete for
	* @param input the command to complete
	*
	* @returns the vector of possible command completions
	*/
	std::vector<Variant> internal::complete(Instance* textentry, const std::string& input) {
		PythonScriptInterface* psi (dynamic_cast<PythonScriptInterface*>(get_interface()));
		std::vector<Variant> completions = psi->complete(input);

		std::sort(completions.begin(), completions.end()); // Sort the completion commands alphabetically

		return completions;
	}
	void internal::resize() {
		if ((ui_handle == nullptr)||(ui_text_entry == nullptr)) {
			return;
		}

		// Get maximum possible console size
		rect.w = std::min(get_window_size().first, 800);
		rect.h = std::min<long>(get_window_size().second - ui_text_entry->get_data("h").i - 10, 500);
		if (rect.w < 800) {
			ui_handle->set_corner(0.0, ui_handle->get_corner().second);
		}
		if (rect.h < 500) {
			ui_handle->set_corner(ui_handle->get_corner().first, 0.0);
		}

		ui_handle->set_data("w", rect.w);

		ui_text_entry->ev_update();
		ui_text_entry->ev_io(Variant(std::vector<Variant>{Variant("set_size"), ui_text_entry->get_data("rows"), Variant(rect.w / ui_text_entry->get_data("char_width").i)}));
		ui_text_entry->set_data("w", rect.w);
	}
	/**
	* Draw the console and its output.
	*/
	void internal::draw() {
		if (!get_is_open()) {
			return;
		}

		// Base the console corner on the handle
		int cx = static_cast<int>(ui_handle->get_corner().first);
		int cy = static_cast<int>(ui_handle->get_corner().second) + 10;

		// Set the drawing sizes
		unsigned int input_line_y = rect.h - 30;

		// Draw console log rectangle
		render::draw_quad({cx, cy, 0}, {rect.w, rect.h, 0}, -1, {127, 127, 127, 225});

		// Remove the top of the console log if it doesn't fit
		size_t line_amount = input_line_y/char_height+1; // Calculate the total lines that can be stored in the console window
		std::vector<std::string> lines = util::splitv(log.str(), '\n', false); // Separate the console log by each newline
		size_t total_lines = lines.size(); // Store the total line number for the below page number calculation

		// Split lines if they are wider than the console window
		std::vector<std::string> full_lines (lines);
		lines.clear();
		const int cutoff = rect.w / char_width;
		for (auto it=full_lines.begin(); it!=full_lines.end(); ++it) {
			std::string line (*it);

			while (line.length() * char_width > static_cast<unsigned int>(rect.w)) {
				const size_t tab_amount = util::string::replace(line, "\t", "").length() - line.length();

				lines.emplace_back(line.substr(0, cutoff+4*tab_amount));
				line = line.substr(cutoff+4*tab_amount);

				++total_lines;
			}

			if (!line.empty()) {
				lines.emplace_back(line);
			}
		}

		if (page_index*line_amount > total_lines) {
			page_index = total_lines / line_amount;
		}

		if (total_lines > line_amount) { // If there are more lines than can fit in the console, then remove the ones which shouldn't be rendered
			if (lines.size() > line_amount + page_index*line_amount + 1) { // If the console page has non-rendered lines before it, then remove them
				const size_t size_goal = lines.size() - line_amount - page_index*line_amount - 1;

				std::vector<std::string>::iterator it = lines.begin();
				for (size_t i=0; i<size_goal; ++i) {
					++it;
				}

				lines.erase(lines.begin(), it);
			}

			if (page_index > 0) { // If the console page has non-rendered lines after it, then remove them
				const size_t size_goal = lines.size() - line_amount - (page_index-1)*line_amount - 1;

				std::vector<std::string>::iterator it = lines.begin();
				for (size_t i=0; i<size_goal; ++i) {
					++it;
				}

				lines.erase(it, lines.end());
			}
		}

		std::string short_log = util::joinv(lines, '\n'); // Create a shorter log from the cropped log
		if ((page_index > 0)&&(lines.size() < line_amount)) { // If the console page has less lines than are renderable (e.g. it's the first page), then prepend extra newlines
			short_log = util::string::repeat(line_amount - lines.size() + 1, "\n") + short_log;
		}
		td_log = engine->font_default->draw(td_log, cx, cy, short_log, {0, 0, 0, 255}); // Draw the console log

		// Define several drawing colors
		RGBA c_text (E_RGB::BLACK);

		// Draw the console page number
		std::string p = std::to_string(page_index+1) + "/" + std::to_string(total_lines/line_amount+1);
		engine->font_default->draw_fast(cx + rect.w - 10 * static_cast<int>(p.length()), cy + rect.h - char_height, p, c_text);

		// Draw the console handle and text_entry
		ui_handle->set_data("is_visible", true);
		ui_handle->ev_update();
		ui_handle->ev_draw();
		ui_handle->set_data("is_visible", false);

		ui_text_entry->set_data("is_visible", true);
		ui_text_entry->set_corner(cx, cy+rect.h);
		ui_text_entry->ev_update();
		ui_text_entry->ev_draw();
		ui_text_entry->set_data("is_visible", false);
	}

	/**
	* Open the console.
	*/
	void open() {
		internal::is_open = true;

		internal::update_ui();
	}
	/**
	* Close the console.
	*/
	void close() {
		internal::is_open = false;

		internal::update_ui();
	}
	/**
	* Toggle the open/close state of the console.
	*/
	void toggle() {
		internal::is_open = !internal::is_open;

		internal::update_ui();
	}
	/**
	* Return whether the console is open or not.
	*/
	bool get_is_open() {
		return internal::is_open;
	}
	/**
	* Clear the console log and reset the page index.
	*/
	void clear() {
		internal::log.str(std::string());
        internal::log.clear();
        internal::page_index = 0;
	}

	/**
	* @param name the name of the variable to check for
	*
	* @returns whether a variable with the given name exists in the console script
	*/
	bool has_var(const std::string& name) {
		return internal::get_interface()->has_var(name);
	}
	/**
	* Set a console variable.
	* @param name the name of the variable
	* @param value the value to set it to
	*/
	void set_var(const std::string& name, const Variant& value) {
		internal::get_interface()->set_var(name, value);
	}
	/**
	* @param name the name of the variable
	*
	* @returns the value of a console variable
	*/
	Variant get_var(const std::string& name) {
		return internal::get_interface()->get_var(name);
	}
	/**
	* @param name the name to bind to the function
	* @param func the function callback
	*
	* @retval 0 success
	* @retval 1 failed since another command with the same name exists
	* @retval 2 failed since the console ScriptInterface is invalid
	* @retval 3 failed to bind the name to the function
	*/
	int add_command(const std::string& name, std::function<Variant (const std::vector<Variant>&)> func) {
		if (internal::commands.count(name)) {
			return 1;
		}

		PythonScriptInterface* psi = dynamic_cast<PythonScriptInterface*>(internal::get_interface());
		if (psi == nullptr) {
			return 2;
		}

		auto* cmd = new internal::ConsoleCommand {
			PyObject_HEAD_INIT(nullptr)
			func,
			{
				name.c_str(),
				[] (PyObject* self, PyObject* args) -> PyObject* {
					Variant a = python::pyobj_to_variant(args);
					if (a.get_type() != E_DATA_TYPE::VECTOR) {
						return nullptr;
					}

					auto _self = reinterpret_cast<internal::ConsoleCommand*>(self);
					return python::variant_to_pyobj(_self->func(a.v));
				},
				METH_VARARGS,
				nullptr
			}
		};
		PyObject_INIT(cmd, &internal::ConsoleCommandType);
		PyObject* _func = PyCFunction_New(&cmd->ml, reinterpret_cast<PyObject*>(cmd));

		if (psi->set_var(name, _func)) {
			Py_XDECREF(_func);
			delete cmd;
			return 3;
		}

		internal::commands.emplace(name, cmd);

		return 0;
	}
	/**
	* @param name the name of the command to remove
	*
	* @retval 0 success
	* @retval 1 failed since the console ScriptInterface is invalid
	* @retval 2 failed to unbind the name from the function
	*/
	int remove_command(const std::string& name) {
		auto cmd = internal::commands.find(name);
		if (cmd == internal::commands.end()) {
			return 0;
		}

		PythonScriptInterface* psi = dynamic_cast<PythonScriptInterface*>(internal::get_interface());
		if (psi == nullptr) {
			return 1;
		}

		if (psi->set_var(name, nullptr)) {
			return 2;
		}

		Py_DECREF(cmd->second);
		internal::commands.erase(cmd);

		return 0;
	}

	/**
	* Run a command in the console.
	* @param command the command string to run including the arguments
	*
	* @returns the script's return value
	* @see internal::run()
	*/
	int run(const std::string& command) {
		return internal::run(command, false); // Run the command in non-silent mode
	}
	/**
	* Log a message to the console.
	* @param type the message type
	* @param str the message to log
	*/
	void log(E_MESSAGE type, const std::string& str) {
		messenger::send({"engine", "console"}, type, str);
	}
}}
