/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_MESSENGER_MESSAGERECIPIENT_H
#define BEE_MESSENGER_MESSAGERECIPIENT_H 1

#include <functional>

#include "messagecontents.hpp"

namespace bee {
	/// Used to act on specific message tags
	struct MessageRecipient {
		std::string name;
		std::vector<std::string> tags;
		bool is_strict;
		std::function<void (const MessageContents&)> func = nullptr;

		MessageRecipient();
		MessageRecipient(const std::string&, const std::vector<std::string>&, bool, std::function<void (const MessageContents&)>);

		bool operator<(const MessageRecipient&) const;
	};
}

#endif // BEE_MESSENGER_MESSAGERECIPIENT_H
