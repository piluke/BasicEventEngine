/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_MESSENGER_H
#define BEE_MESSENGER_H 1

#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <functional>

#include "../util/windefine.hpp"

#include "../enum.hpp"

namespace bee {
	class Variant;

	struct MessageContents;
	struct MessageRecipient;
namespace messenger {
	int clear(bool);
	int clear();

	namespace internal {
		int output_msg(const MessageContents&);
		int print_msg(const std::string&, const MessageContents&);
		bool should_print(E_OUTPUT, E_MESSAGE);
		std::exception_ptr call_recipients(const MessageContents&);
		std::exception_ptr handle_recipient(const MessageRecipient&, const MessageContents&);

		void register_protected(const MessageRecipient&);
		void register_protected(const std::string&, const std::vector<std::string>&, bool, std::function<void (const MessageContents&)>);
		void unregister_protected(const std::string&);
		void send_urgent(const MessageContents&);
		void send_urgent(const std::vector<std::string>&, E_MESSAGE, const std::string&, const Variant&);

		size_t remove_messages(std::function<bool (const MessageContents&)>);
	}

	int register_recipient(const MessageRecipient&);
	int register_recipient(const std::string&, const std::vector<std::string>&, bool, std::function<void (const MessageContents&)>);
	int unregister(const std::string&);
	int unregister_all();

	void send(const MessageContents&);
	void send(const std::vector<std::string>&, E_MESSAGE, const std::string&, const Variant&);
	void send(const std::vector<std::string>&, E_MESSAGE, const std::string&);
	void log(const std::string&);

	size_t add_filter(const std::string&);
	bool set_filter_blacklist(bool);
	void reset_filter();

	int add_log(const std::string&, E_OUTPUT);
	int remove_log(const std::string&, bool);
	int clear_logs(bool);

	E_OUTPUT set_level(E_OUTPUT);
	E_OUTPUT get_level();

	size_t handle();
	std::string get_type_string(E_MESSAGE);
}}

#endif // BEE_MESSENGER_H
