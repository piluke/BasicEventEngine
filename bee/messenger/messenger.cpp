/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include <iostream>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <set>

#include "messenger.hpp"

#include "../engine.hpp"

#include "../util/string.hpp"
#include "../util/platform.hpp"
#include "../util/debug.hpp"

#include "../init/gameoptions.hpp"

#include "messagerecipient.hpp"
#include "messagecontents.hpp"

#include "../util/files.hpp"
#include "../util/windefine.hpp"

namespace bee { namespace messenger{
	namespace internal {
		std::unordered_map<std::string,std::list<MessageRecipient>> recipients;
		const std::set<std::string> protected_tags = {"engine", "console"};
		std::vector<MessageContents> messages;

		std::list<std::string> filter;
		bool is_filter_blacklist = true;

		std::unordered_map<std::string,std::pair<E_OUTPUT,std::ofstream*>> logfiles = {{"stdout", {E_OUTPUT::NORMAL, nullptr}}};
	}

	/**
	* Clear all recipients and messages for when the game ends.
	* @param should_warn whether to print a warning if there are unprocessed messages still in queue
	*
	* @retval 0 success
	* @retval 1 failed to delete some log files
	*/
	int clear(bool should_warn) {
		if ((!internal::messages.empty())&&(should_warn)) {
			internal::send_urgent(MessageContents(
				get_ticks(),
				{"engine", "close"},
				E_MESSAGE::WARNING,
				"Messenger closing with " + std::to_string(internal::messages.size()) + " messages left in the queue",
				{}
			));
		}

		internal::recipients.clear();
		internal::messages.clear();

		if (clear_logs(false)) {
			return 1;
		}

		return 0;
	}
	int clear() {
		return clear(true);
	}

	/**
	* Output the given message if the verbosity level is high enough.
	* @param msg the message to process
	*
	* @retval 0 success
	* @retval -1 output the message in the headless mode compact format
	* @retval 1 did not output since the message was filtered
	*/
	int internal::output_msg(const MessageContents& msg) {
		bool is_filtered = false;
		for (auto& t : msg.tags) {
			for (auto& f : filter) {
				if (t == f) {
					is_filtered = true;
					break;
				}
			}

			if (is_filtered) {
				break;
			}
		}
		if (!(is_filtered ^ is_filter_blacklist)) {
			return 1;
		}

		if ((engine != nullptr)&&((get_option("is_headless").i)&&(!get_option("is_debug_enabled").i))) {
			std::stringstream h; // Combine the message metadata
			h << msg.tickstamp << "ms> ";

			print_msg(h.str(), msg);

			return -1;
		}

		// Create a string of the message's tags
		std::string tags = util::joinv(msg.tags, ',');

		std::stringstream h; // Combine the message metadata
		h << "MSG (" << msg.tickstamp << "ms)[" << get_type_string(msg.type) << "]<" << tags << ">: ";

		print_msg(h.str(), msg);

		return 0;
	}
	/**
	* Print the given message description with the given header.
	* @param header the metadata to print before the message description
	* @param msg the message whose description should be printed
	*
	* @returns how many logs were printed to
	*/
	int internal::print_msg(const std::string& header, const MessageContents& msg) {
		int logs_printed = 0;

		for (auto& lf : logfiles) {
			if (!should_print(lf.second.first, msg.type)) {
				continue;
			}

			std::ostream* o = lf.second.second;

			if (lf.first == "stdout") {
				// Output to the appropriate stream
				o = &std::cout;
				if ((msg.type == E_MESSAGE::WARNING)||(msg.type == E_MESSAGE::ERROR)) {
					o = &std::cerr;
				}

				// Change the output color depending on the message type
				if (msg.type == E_MESSAGE::WARNING) {
					util::platform::commandline_color(o, 11); // Yellow
				} else if (msg.type == E_MESSAGE::ERROR) {
					util::platform::commandline_color(o, 9); // Red
				}
			}

			// Output the message metadata
			*o << header;

			// Output the message description
			if (msg.descr.find("\n") != std::string::npos) { // If the description is multiple liness, indent it as necessary
				*o << "\n";
				*o << util::debug_indent(msg.descr, 1);
			} else { // Otherwise, output it as normal
				*o << msg.descr << "\n";
			}

			if (
				(lf.first == "stdout")
				&&((msg.type == E_MESSAGE::WARNING)||(msg.type == E_MESSAGE::ERROR))
			) {
				util::platform::commandline_color_reset(o); // Reset the output color
			}

			std::flush(*o); // Flush the output buffer after printing
			logs_printed++;
		}

		return logs_printed;
	}
	/**
	* @param level the output level
	* @param type the message type
	*
	* @returns whether the given message type should be printed at the given output level
	*/
	bool internal::should_print(E_OUTPUT level, E_MESSAGE type) {
		switch (level) {
			case E_OUTPUT::NONE: { // When the verbosity is NONE, skip all message types
				return false;
			}
			case E_OUTPUT::QUIET: { // When the verbosity is QUIET, skip all types except warnings and errors
				if (
					(type != E_MESSAGE::WARNING)
					&&(type != E_MESSAGE::ERROR)
				) 		{
					return false;
				}
				break;
			}
			case E_OUTPUT::NORMAL: // When the verbosity is NORMAL, skip internal messages
			default: {
				if (type == E_MESSAGE::INTERNAL) {
					return false;
				}
				break;
			}
			case E_OUTPUT::VERBOSE: { // When the verbosity is VERBOSE, don't skip any message types
				break;
			}
		}
		return true;
	}
	/**
	* Call the recipients who are registered for the given message's tags.
	* @param msg the message to pass to the recipients
	*
	* @returns the first exception thrown by a recipient or nullptr if none was thrown
	*/
	std::exception_ptr internal::call_recipients(const MessageContents& msg) {
		if (msg.tags.empty()) {
			return nullptr;
		}

		std::exception_ptr ep = nullptr;
		if (msg.tags.at(0) == "direct") { // Send the message directly to a recipient rather than handling tags
			auto rt = recipients.find("direct"); // Recipients must register for the direct tag in order to receive direct messages
			if (rt != recipients.end()) {
				for (auto& tag : msg.tags) { // Iterate over the message's tags as recipient names
					for (auto& recv : rt->second) {
						if (recv.name == tag) {
							std::exception_ptr e = handle_recipient(recv, msg);
							if (e != nullptr) {
								ep = e;
								break;
							}
						}

						if (ep != nullptr) {
							break;
						}
					}
				}
			}
		} else {
			std::set<MessageRecipient> has_received;
			for (auto& tag : msg.tags) { // Iterate over the message's tags
				auto rt = recipients.find(tag); // Make sure that the message tag exists
				if (rt != recipients.end()) {
					for (auto& recv : recipients.at(tag)) { // Iterate over the recipients who wish to process the tag
						if (has_received.find(recv) != has_received.end()) {
							continue;
						}

						std::exception_ptr e = handle_recipient(recv, msg);
						if (e != nullptr) {
							ep = e;
							break;
						}

						has_received.insert(recv);
					}
				}

				if (ep != nullptr) {
					break;
				}
			}
		}

		return ep;
	}
	/**
	* Handle the individual recipient.
	* @param recv the recipient to send the message to
	* @param msg the message to send
	*
	* @returns the exception thrown by the recipient or nullptr if none was thrown
	*/
	std::exception_ptr internal::handle_recipient(const MessageRecipient& recv, const MessageContents& msg) {
		std::exception_ptr ep = nullptr;

		if ((recv.is_strict)&&(msg.tags != recv.tags)) { // If the recipient is strict but the tags don't match
			if (recv.tags.at(0) != "direct") { // If the recipient does not accept direct messages
				return nullptr;
			}

			if (std::find(msg.tags.begin(), msg.tags.end(), recv.name) == msg.tags.end()) { // If the message is not intended for the recipient
				return nullptr;
			}
		}

		// Call the recipient's function
		if (recv.func != nullptr) {
			#ifdef NDEBUG
				try {
					recv.func(msg);
				} catch (int e) { // Catch several kinds of exceptions that the recipient might throw
					ep = std::current_exception();
					util::platform::commandline_color(&std::cerr, 9);
					std::cerr << "MSG ERR (" << get_ticks() << "ms): exception " << e << " thrown by recipient \"" << recv.name << "\"\n";
					util::platform::commandline_color_reset(&std::cerr);
				} catch (const char* e) {
					ep = std::current_exception();
					util::platform::commandline_color(&std::cerr, 9);
					std::cerr << "MSG ERR (" << get_ticks() << "ms): exception \"" << e << "\" thrown by recipient \"" << recv.name << "\"\n";
					util::platform::commandline_color_reset(&std::cerr);
				} catch (...) {
					ep = std::current_exception(); // Store any miscellaneous exceptions to be rethrown
				}
			#else // If in debugging mode, just let the recipient crash
				recv.func(msg);
			#endif
		}

		return ep;
	}

	/**
	* Register the given recipient with protected tags within the messaging system.
	* @param recv the recipient to register
	*/
	void internal::register_protected(const MessageRecipient& recv) {
		for (auto& tag : recv.tags) { // Iterate over the requested tags
			std::unordered_map<std::string,std::list<MessageRecipient>>::iterator rt (recipients.find(tag));
			if (rt == recipients.end()) { // If the tag doesn't exist, then create it
				recipients.emplace(tag, std::list<MessageRecipient>({recv}));
			} else {
				rt->second.push_back(recv); // Add the recipient to the list
			}
		}
	}
	/**
	* Register the given recipient within the messaging system.
	* @param name the name of the funciton
	* @param tags the tags to register the recipient with
	* @param is_strict whether the recipient should only take exact tags
	* @param func the function to use to handle the messages
	*/
	void internal::register_protected(const std::string& name, const std::vector<std::string>& tags, bool is_strict, std::function<void (const MessageContents&)> func) {
		register_protected(MessageRecipient(name, tags, is_strict, func));
	}
	/**
	* Unregister the given recipient from protected tags within the messaging system.
	* @param recv the recipient to unregister
	*/
	void internal::unregister_protected(const std::string& name) {
		for (auto& tag : internal::recipients) { // Iterate over all tags
			tag.second.erase(std::remove_if(tag.second.begin(), tag.second.end(), [&name] (const MessageRecipient& recv) {
				return (recv.name == name);
			}), tag.second.end());
		}
	}
	/**
	* Immediately process the given message to its recipients.
	* @note Warning: this can break things so use sparingly, most messages can wait a couple milliseconds for handle_messages() to process at the end of each frame.
	* @param msg the message to process
	*/
	void internal::send_urgent(const MessageContents& msg) {
		output_msg(msg);

		std::exception_ptr ep = call_recipients(msg);
		if (ep != nullptr) { // If an exception was thrown, throw it after finishing processing the message
			std::flush(std::cout);
			std::rethrow_exception(ep);
		}
	}
	/**
	* Immediately process the given message to its recipients.
	* @note When the function is called with separate message contents, create the message and call it again.
	* @param tags the list of tags that the message should be sent to
	* @param type the message type
	* @param descr the message description
	* @param data the message data
	*/
	void internal::send_urgent(const std::vector<std::string>& tags, E_MESSAGE type, const std::string& descr, const Variant& data) {
		MessageContents msg (get_ticks(), tags, type, descr, data);
		internal::send_urgent(msg);
	}

	/**
	* Remove messages from processing based on the given criterion function.
	* @param func the callback which determines whether to remove a message
	*
	* @returns how many messages were removed
	*/
	size_t internal::remove_messages(std::function<bool (const MessageContents&)> func) {
		size_t amount = messages.size();

		messages.erase(std::remove_if(messages.begin(), messages.end(), func), messages.end());

		amount -= messages.size();

		return amount;
	}

	/**
	* Register the given recipient within the messaging system.
	* @param recv the recipient to register
	*
	* @retval 0 success
	* @retval 1 failed to register for some tags, usually since they're protected
	*/
	int register_recipient(const MessageRecipient& recv) {
		bool failed_tag = false;

		for (auto& tag : recv.tags) { // Iterate over the requested tags
			if (internal::protected_tags.find(tag) != internal::protected_tags.end()) { // If the requested tag is protected, deny registration
				// Output an error message
				util::platform::commandline_color(&std::cerr, 11);
				std::cerr << "MSG failed to register recipient \"" << recv.name << "\" with protected tag \"" << tag << "\".\n";
				util::platform::commandline_color_reset(&std::cerr);

				failed_tag = true;

				continue;
			}

			std::unordered_map<std::string,std::list<MessageRecipient>>::iterator rt (internal::recipients.find(tag));
			if (rt == internal::recipients.end()) { // If the tag doesn't exist, then create it
				internal::recipients.emplace(tag, std::list<MessageRecipient>({recv}));
			} else {
				rt->second.push_back(recv); // Add the recipient to the list
			}
		}

		return failed_tag;
	}
	/**
	* Register the given recipient within the messaging system.
	* @param name the name of the funciton
	* @param tags the tags to register the recipient with
	* @param is_strict whether the recipient should only take exact tags
	* @param func the function to use to handle the messages
	*
	* @retval 0 success
	* @retval 1 failed to register for some tags, usually since they're protected
	*/
	int register_recipient(const std::string& name, const std::vector<std::string>& tags, bool is_strict, std::function<void (const MessageContents&)> func) {
		return register_recipient(MessageRecipient(name, tags, is_strict, func));
	}
	/**
	* Unregister the recipient with the given name within the messaging system.
	* @param name the name of the recipient to unregister
	*
	* @retval 0 success
	* @retval 1 failed to unregister from a protected tag
	*/
	int unregister(const std::string& name) {
		std::string protected_tag = "";
		for (auto& tag : internal::recipients) { // Iterate over all tags
			tag.second.erase(std::remove_if(tag.second.begin(), tag.second.end(), [&name, &tag, &protected_tag] (const MessageRecipient& recv) {
				if (recv.name == name) {
					if (internal::protected_tags.find(tag.first) != internal::protected_tags.end()) { // If the specific tag is protected, deny removal for any tags
						protected_tag = tag.first;
					}
					return protected_tag.empty();
				}
				return false;
			}), tag.second.end());
		}

		if (!protected_tag.empty()) {
			// Output an error message
			util::platform::commandline_color(&std::cerr, 11);
			std::cerr << "MSG failed to unregister recipient \"" << name << "\" because of protected tag \"" << protected_tag << "\".\n";
			util::platform::commandline_color_reset(&std::cerr);

			return 1;
		}

		return 0;
	}
	/**
	* Unregister all messaging system recipients.
	* @note This function is not able to protect functions which register for additional tags such as console commands.
	*   e.g. {"engine", "console", "help"} will be reduced to {"engine", "console"}
	*   This function may be protected in the future.
	*
	* @retval 0 success
	* @retval 1 failed to unregister some tags, usually since they're protected
	*/
	int unregister_all() {
		bool failed_tag = false;

		for (auto& tag : internal::recipients) { // Iterate over all recipient tags
			if (internal::protected_tags.find(tag.first) != internal::protected_tags.end()) { // If the specific tag is protected, deny removal
				failed_tag = true;
				continue;
			}

			tag.second.clear(); // Remove all recipients who are registered with this tag
		}

		// Remove all non-protected tags
		for (auto tag=internal::recipients.begin(); tag!=internal::recipients.end();) {
			if (internal::protected_tags.find(tag->first) == internal::protected_tags.end()) { // If the tag is not protected, remove it
				tag = internal::recipients.erase(tag);
			} else { // It the tag is protected, skip it
				++tag;
			}
		}

		return failed_tag;
	}

	/**
	* Queue the given message in the messaging system.
	* @param m the message to queue
	*/
	void send(const MessageContents& m) {
		internal::messages.push_back(m);
	}
	/**
	* Queue the given message in the messaging system.
	* @param tags the list of tags that the message should be sent to
	* @param type the message type
	* @param descr the message description
	* @param data the message data
	*/
	void send(const std::vector<std::string>& tags, E_MESSAGE type, const std::string& descr, const Variant& data) {
		send(MessageContents(get_ticks(), tags, type, descr, data));
	}
	/**
	* Queue the given message in the messaging system.
	* @param tags the list of tags that the message should be sent to
	* @param type the message type
	* @param descr the message description
	*/
	void send(const std::vector<std::string>& tags, E_MESSAGE type, const std::string& descr) {
		send(tags, type, descr, {});
	}
	/**
	* Queue the given string as a simple log message.
	* @param msg the string to queue
	*/
	void log(const std::string& msg) {
		send({"log"}, E_MESSAGE::INFO, msg);
	}

	/**
	* Add a tag to the filter list.
	* @param f the filter to add
	*
	* @returns the current amount of filters
	*/
	size_t add_filter(const std::string& f) {
		internal::filter.push_back(f);
		return internal::filter.size();
	}
	/**
	* Set the filter type.
	* @param type whether the filter should be a blacklist or whitelist
	*
	* @returns the previous value
	*/
	bool set_filter_blacklist(bool type) {
		bool old_is_filter_blacklist = internal::is_filter_blacklist;
		internal::is_filter_blacklist = type;
		return old_is_filter_blacklist;
	}
	/**
	* Remove all filters.
	*/
	void reset_filter() {
		internal::filter.clear();
	}

	/**
	* Add a filename as a log file.
	* @note This function can also be used to change the output level of existing log files.
	* @param filename the file to log to
	* @param level the output level to log
	*
	* @retval 0 success
	* @retval 1 failed since stdout can't be overwritten, use set_level() to change it's level
	* @retval 2 failed to open the log file
	*/
	int add_log(const std::string& filename, E_OUTPUT level) {
		if (filename == "stdout") { // Deny overwriting stdout via this function
			return 1;
		}

		if (internal::logfiles.find(filename) != internal::logfiles.end()) {
			internal::logfiles.at(filename).first = level;
			return 0;
		}

		std::ofstream* logfile = new std::ofstream(filename);
		if (!logfile->is_open()) {
			send({"engine", "messenger"}, E_MESSAGE::ERROR, "Failed to open log file \"" + filename + "\"");
			return 2;
		}

		internal::logfiles[filename] = std::make_pair(level, logfile);

		return 0;
	}
	/**
	* Remove the given filename from being a log file.
	* @param filename the filename to remove
	* @param should_delete whether the file should also be deleted
	*
	* @retval 0 success
	* @retval 1 failed since stdout can't be removed
	* @retval 2 failed since the log doesn't exist
	* @retval 3 failed to delete the log file
	*/
	int remove_log(const std::string& filename, bool should_delete) {
		if (filename == "stdout") { // Deny removing stdout via this function
			return 1;
		}

		std::unordered_map<std::string,std::pair<E_OUTPUT,std::ofstream*>>::iterator log (internal::logfiles.find(filename));
		if (log == internal::logfiles.end()) {
			send({"engine", "messenger"}, E_MESSAGE::WARNING, "The log file \"" + filename + "\" could not be found for removal");
			return 2;
		}

		log->second.second->close();
		delete log->second.second;

		internal::logfiles.erase(log);

		if (should_delete) {
			if (util::file_delete(filename)) {
				return 3;
			}
		}

		return 0;
	}
	/**
	* Clear all log files except stdout.
	* @param should_delete whether the log files should be deleted
	*
	* @retval 0 success
	* @retval >0 a log file failed to be deleted
	*/
	int clear_logs(bool should_delete) {
		int r = 0;
		for (auto it=internal::logfiles.begin(); it!=internal::logfiles.end(); ) {
			if (it->first == "stdout") {
				++it;
				continue;
			}

			it->second.second->close();
			delete it->second.second;

			if (should_delete) {
				r += !!util::file_delete(it->first);
			}

			internal::logfiles.erase(it++);
		}
		return r;
	}

	/**
	* Set the output level when printing message descriptions to stdout.
	* @param level the output level to use
	*
	* @returns the previous value
	*/
	E_OUTPUT set_level(E_OUTPUT level) {
		E_OUTPUT old_level = internal::logfiles.at("stdout").first;
		internal::logfiles.at("stdout").first = level;
		return old_level;
	}
	/**
	* @returns the output level when printing message descriptions to stdout
	*/
	E_OUTPUT get_level() {
		return internal::logfiles.at("stdout").first;
	}

	/**
	* Handle all queued messaged and execute the recipients' functions.
	* @note This function will be called from the main loop at the end of every frame
		and is guaranteed to send every queued message unless it's from the future.
	* @note In certain situations the engine might call this more than once per frame.
	*
	* @returns the number of messages processed
	*/
	size_t handle() {
		const Uint32 t = get_ticks(); // Get the current tick to compare with message tickstamps
		const std::vector<MessageContents> messages (internal::messages); // Swap the message list to prevent immediate processing of messages sent from recipients
		internal::messages.clear();

		// Print message descriptions
		for (auto& msg : messages) { // Iterate over the messages
			if ((t < msg.tickstamp)&&(engine != nullptr)) { // If the message should be processed in the future, skip it
				continue;
			}

			internal::output_msg(msg);
		}

		// Process messages with recipient functions
		size_t num_messages = 0;
		std::exception_ptr ep = nullptr; // Store any thrown values
		for (auto& msg : messages) { // Iterate over the messages
			if ((t < msg.tickstamp)&&(engine != nullptr)) { // If the message should be processed in the future, skip it
				internal::messages.push_back(msg); // Append it to the new message list
				continue;
			}

			num_messages++;
			std::exception_ptr e = internal::call_recipients(msg);
			if (e != nullptr) {
				ep = e;
			}
		}

		if (ep != nullptr) { // If an exception was thrown, throw it after finishing processing the message
			std::flush(std::cout);
			std::rethrow_exception(ep);
		}

		return num_messages;
	}
	/**
	* @param type the type of message to evaluate
	*
	* @returns the name describing the message type
	*/
	std::string get_type_string(E_MESSAGE type) {
		switch (type) { // Return the string for the requested type
			case E_MESSAGE::GENERAL:  return "general";
			case E_MESSAGE::START:    return "start";
			case E_MESSAGE::END:      return "end";
			case E_MESSAGE::INFO:     return "info";
			case E_MESSAGE::WARNING:  return "warning";
			case E_MESSAGE::ERROR:    return "error";
			case E_MESSAGE::INTERNAL: return "internal";
			default:                  return "unknown"; // Return "unknown" when an undefined type is provided
		}
	}
}}
