/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_MESSENGER_MESSAGECONTENTS_H
#define BEE_MESSENGER_MESSAGECONTENTS_H 1

#include <vector>

#include <SDL2/SDL.h> // Include the required SDL headers

#include "../enum.hpp"

#include "../data/variant.hpp"

namespace bee {
	/// Used to pass data through the messaging system to MessageRecipients
	struct MessageContents {
		Uint32 tickstamp;
		std::vector<std::string> tags;
		E_MESSAGE type;
		std::string descr;
		Variant data;

		MessageContents();
		MessageContents(Uint32, const std::vector<std::string>&, E_MESSAGE, const std::string&, const Variant&);
	};
}

#endif // BEE_MESSENGER_MESSAGECONTENTS_H
