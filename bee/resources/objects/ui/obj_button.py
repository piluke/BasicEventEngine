###############################################################################
# Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEE.
# BEE is free software and comes with ABSOLUTELY NO WARRANTY
# See LICENSE for more details.
###############################################################################

import bee

obj_element = bee._import("resources/objects/ui/obj_element.py")

def create(self):
	obj_element.create(self)

	if not self.has_data("font"):
		font = bee.Font("font_default")
		self.set_data("font", font)
	if not self.has_data("text"):
		self.set_data("text", "")

	if not self.has_data("press_func"):
		self.set_data("press_func", None)

def mouse_press(self, e):
	obj_element.mouse_press(self, e)

	if self.get_data("is_pressed") == True:
		bee.Sound("__snd_ui_button_press").play()
def mouse_release(self, e):
	obj_element.mouse_release(self, e)

	if self.get_data("has_focus") == True:
		bee.Sound("__snd_ui_button_release").play()
		bee.ui.button_callback(self)

def draw(self):
	if self.get_data("is_visible") != True:
		return

	bee.render.set_is_lightable(False)

	w = self.get_data("w")
	h = self.get_data("h")

	c_border = (0, 0, 0, 255)
	c_back = bee.RGBA(*self.get_data("color"))
	if self.get_data("has_hover") == True:
		c_back.add_hsv(v=-0.2)
	c_back = c_back.get()

	press_offset = 0
	if self.get_data("is_pressed") == True:
		press_offset = 4

	cx, cy = self.get_corner()

	bee.render.draw_quad((cx, cy+press_offset, 0), (w, h, 0), -1, c_back)
	bee.render.draw_quad((cx, cy+press_offset, 0), (w, h, 0), 6, c_border)

	text = self.get_data("text")
	if text:
		font = self.get_data("font")
		if not font:
			font = bee.Font("font_default")

		font.draw(
			cx + (w - font.get_string_width(text))/2,
			cy+press_offset + (h-h*5/4)/2,
			text
		)
	else:
		bee.messenger.log("empty button")

	bee.render.set_is_lightable(True)

def io(self, data: list):
	if data[0] in globals():
		globals()[data[0]](self, *data[1:])

for a in dir(obj_element):
	if not a.startswith("_") and a not in globals():
		globals()[a] = getattr(obj_element, a)

def center_width(self):
	font = self.get_data("font")
	if not font:
		font = bee.Font("font_default")

	text = self.get_data("text")
	w = font.get_string_width(" " + text + " ")
	h = font.get_string_height(text)
	self.set_data("w", w)
	self.set_data("h", h*5/4)

	self.set_corner(
		(bee.get_window()[2] - w)/2,
		self.get_corner()[1]
	)
