###############################################################################
# Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEE.
# BEE is free software and comes with ABSOLUTELY NO WARRANTY
# See LICENSE for more details.
###############################################################################

import bee

obj_element = bee._import("resources/objects/ui/obj_element.py")

def create(self):
	obj_element.create(self)

	if not self.has_data("type"):
		self.set_data("type", 0) # 0=checkbox, 1=radiobox
	if not self.has_data("option_height"):
		self.set_data("option_height", 32)

	if not self.has_data("adaptive_height"):
		self.set_data("adaptive_height", False)

	reset_options(self)

def mouse_release(self, e):
	obj_element.mouse_release(self, e)

	if self.get_data("has_focus") == True:
		mpos = bee.mouse.get_pos()
		index = get_option_at(self, *mpos)

		if index >= 0:
			state = self.get_data("option_state")[index]
			set_option_state(self, index, not state)

def draw(self):
	if self.get_data("is_visible") != True:
		return

	bee.render.set_is_lightable(False)

	oh = self.get_data("option_height")
	if self.get_data("adaptive_height") == True:
		self.set_data("h", len(self.get_data("options")) * oh)
	w = self.get_data("w")
	h = self.get_data("h")

	c_border = (0, 0, 0, 255)
	c_back = self.get_data("color")
	c_highlight = bee.RGBA(*c_back)
	c_highlight.add_hsv(v=-0.2)
	c_highlight = c_highlight.get()

	cx, cy = self.get_corner()

	bee.render.draw_quad((cx, cy, 0), (w, h, 0), -1, c_back)
	if self.get_data("has_hover") == True:
		mpos = bee.mouse.get_pos()
		bee.render.draw_quad(
			(cx, cy + get_option_at(self, *mpos)*oh, 0),
			(w, oh, 0),
			-1, c_highlight
		)
	bee.render.draw_quad((cx, cy, 0), (w, h, 0), 6, c_border)

	font = bee.Font("font_default")
	i = 0
	for option in self.get_data("options"):
		if self.get_data("type") == 0:
			bee.render.draw_quad(
				(cx+16, cy + i*oh + 8, 0),
				(16, 16, 0),
				1, c_border
			)
			if self.get_data("option_state")[i]:
				bee.render.draw_line(
					(cx+18, cy + i*oh + 16, 0),
					(cx+22, cy + i*oh + 22, 0),
					c_border
				)
				bee.render.draw_line(
					(cx+22, cy + i*oh + 22, 0),
					(cx+30, cy + i*oh + 10, 0),
					c_border
				)
		elif self.get_data("type") == 1:
			bee.render.draw_circle(
				(cx+24, cy + i*oh + 16, 0),
				8, 1, c_border
			)
			if self.get_data("option_state")[i]:
				bee.render.draw_circle(
					(cx+24, cy + i*oh + 16, 0),
					6, -1, c_border
				)

		font.draw_fast(cx+40, cy + i*oh + 8, option, c_border)

		bee.render.draw_line(
			(cx, cy + (i+1)*oh, 0),
			(cx+w, cy + (i+1)*oh, 0),
			c_border
		)

		i += 1

	bee.render.set_is_lightable(True)

def io(self, data: list):
	if data[0] in globals():
		globals()[data[0]](self, *data[1:])

for a in dir(obj_element):
	if not a.startswith("_") and a not in globals():
		globals()[a] = getattr(obj_element, a)

def reset_options(self):
	self.set_data("options", [])
	self.set_data("option_state", [])
	bee.ui.reset_optionbox_options(self)
def push_option(self, option: str, initial_state: bool, callback):
	self.get_data("options").append(option)
	self.get_data("option_state").append(initial_state)
	bee.ui.push_optionbox_option(self, callback)
def pop_option(self):
	self.get_data("options").pop()
	self.get_data("option_state").pop()
	bee.ui.pop_optionbox_option(self)

def get_option_at(self, mx: int, my: int) -> int:
	oh = self.get_data("option_height")
	if self.get_data("adaptive_height") == True:
		self.set_data("h", len(self.get_data("options")) * oh)
	w = self.get_data("w")
	h = self.get_data("h")

	cx, cy = self.get_corner()

	if (mx < cx-1) or (my < cy-1) or (mx > cx+1+w) or (my > cy+1+h):
		return -1

	return max(0, min((my-cy) / oh, len(self.get_data("options"))-1))
def get_selected_options(self) -> [int]:
	l = []
	for i in range(self.get_data("option_state")):
		if self.get_data("option_state")[i]:
			l.append(i)
	return l

def set_option_state(self, index: int, new_state: bool):
	if self.get_data("type") == 0:
		self.get_data("option_state")[index] = new_state
		bee.ui.optionbox_callback(self, index, new_state)
	elif self.get_data("type") == 1:
		if new_state:
			for i in range(self.get_data("option_state")):
				if i == index:
					continue

				if self.get_data("option_state")[i]:
					self.get_data("option_state")[i] = False
					bee.ui.optionbox_callback(self, i, False)

			if not self.get_data("option_state")[index]:
				self.get_data("option_state")[index] = True
				bee.ui.optionbox_callback(self, index, True)
