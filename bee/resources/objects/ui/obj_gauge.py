###############################################################################
# Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEE.
# BEE is free software and comes with ABSOLUTELY NO WARRANTY
# See LICENSE for more details.
###############################################################################

import bee

obj_element = bee._import("resources/objects/ui/obj_element.py")

def create(self):
	obj_element.create(self)

	if not self.has_data("range"):
		self.set_data("range", 100)
	if not self.has_data("value"):
		self.set_data("value", 0)
	if not self.has_data("pulse_state"):
		self.set_data("pulse_state", 0)

	if not self.has_data("color_back"):
		self.set_data("color_back", (0, 0, 0, 255))

def draw(self):
	if self.get_data("is_visible") != True:
		return

	bee.render.set_is_lightable(False)

	w = self.get_data("w")
	h = self.get_data("h")

	c_border = (0, 0, 0, 255)
	c_back = self.get_data("color_back")
	c_front = self.get_data("color")

	cx, cy = self.get_corner()

	bee.render.draw_quad((cx, cy, 0), (w, h, 0), -1, c_back)

	range = self.get_data("range")
	pulse_state = self.get_data("pulse_state")
	if pulse_state != 0:
		pulse_width = 20
		bee.render.draw_quad(
			(cx + w*self.get_data("value")/range, cy, 0),
			(w*pulse_width/range, h, 0),
			-1, c_front
		)

		inc = int(pulse_state * 100 * bee.get_delta())
		if pulse_state > 0 and self.get_data("value") + pulse_width + inc >= range:
			# If the pulse is increasing and has reached the maximum value, reverse the pulse
			if self.get_data("value") + pulse_width == range:
				self.set_data("pulse_state", -1)
			else:
				self.set_data("value", range - pulse_width)
		elif pulse_state < 0 and self.get_data("value") + inc <= 0:
			# If the pulse is decreasing and has reached the minimum value, reverse the pulse
			if self.get_data("value") == 0:
				self.set_data("pulse_state", 1)
			else:
				self.set_data("value", 0)
		else:
			# Increment the pulse in its current direction
			self.set_data("value", self.get_data("value")+inc)
	else:
		bee.render.draw_quad(
			(cx, cy, 0),
			(w*self.get_data("value")/range, h, 0),
			-1, c_front
		)

	bee.render.draw_quad((cx, cy, 0), (w, h, 0), 2, c_border)

	bee.render.set_is_lightable(True)

def io(self, data: list):
	if data[0] in globals():
		globals()[data[0]](self, *data[1:])

for a in dir(obj_element):
	if not a.startswith("_") and a not in globals():
		globals()[a] = getattr(obj_element, a)

def start_pulse(self):
	self.set_data("range", 100)
	self.set_data("value", 0)
	self.set_data("pulse_state", 1)
def stop_pulse(self):
	self.set_data("value", 0)
	self.set_data("pulse_state", 0)
