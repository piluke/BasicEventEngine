###############################################################################
# Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEE.
# BEE is free software and comes with ABSOLUTELY NO WARRANTY
# See LICENSE for more details.
###############################################################################

import bee

obj_element = bee._import("resources/objects/ui/obj_element.py")

def create(self):
	obj_element.create(self)

	if not self.has_data("font"):
		self.set_data("font", bee.Font("font_default"))
	font = self.get_data("font")
	self.set_data("char_width", font.get_string_width())
	self.set_data("char_height", font.get_string_height())

	if not self.has_data("input"):
		self.set_data("input", "")
		self.set_data("input_width", 0)
	else:
		set_input(self, self.get_data("input"))

	if not self.has_data("rows"):
		self.set_data("rows", 0)
	if not self.has_data("cols"):
		self.set_data("cols", 0)

	if not self.has_data("entry_func"):
		self.set_data("entry_func", None)

	self.set_data("repeat", "SDLK_UNKNOWN")
	self.set_data("repeat_amount", 0)

	reset_completion(self)

def keyboard_input(self, e):
	if self.get_data("has_focus") != True:
		return
	if e["state"] == bee.E_SDL_KEY_STATE["RELEASED"]:
		self.set_data("repeat", "SDLK_UNKNOWN")
		self.set_data("repeat_amount", 0)
		return

	if e["repeat"]:
		if self.get_data("repeat") != "SDLK_UNKNOWN" and e["keyname"] == self.get_data("repeat"):
			self.set_data("repeat_amount", self.get_data("repeat_amount")+1)

			if self.get_data("repeat_amount") < 25:
				return
		else:
			self.set_data("repeat", e["keyname"])
			self.set_data("repeat_amount", 1)
			return
	else:
		self.set_data("repeat", "SDLK_UNKNOWN")
		self.set_data("repeat_amount", 0)

	input = self.get_data("input")
	c, input = bee.kb.append_input(input, e["keysym"]["sym"], e["keysym"]["mod"])

	if e["keyname"] == "SDLK_TAB":
		if self.get_data("rows") == 1:
			input = input[:-1]
			complete(self, input)
	elif e["keyname"] == "SDLK_RETURN":
		if self.get_data("rows") == 1 or not (e["keysym"]["mod"] & bee.E_KMOD["SHIFT"]):
			input = input[:-1]

			set_input(self, input)
			bee.ui.text_entry_callback(self, input)
			return
	else:
		if not input:
			# If the input line is now empty (i.e. backspace), clear the completion commands
			reset_completion(self)

		if c != '\0':
			completions = self.get_data("completions")
			if len(completions) > 1:
				# Clear the completion commands when additional input is received
				complete(self, input)
				input = self.get_data("input")

		set_input(self, input)
		bee.ui.text_entry_handler(self, input, e)

def draw(self):
	if self.get_data("is_visible") != True:
		return

	bee.render.set_is_lightable(False)

	w = self.get_data("w")
	h = self.get_data("h")

	font = self.get_data("font")
	c_text = bee.E_RGBA["BLACK"]
	c_back = self.get_data("color")

	cx, cy = self.get_corner()
	cx, cy = int(cx), int(cy)

	bee.render.draw_quad((cx, cy, 0), (w, h, 0), -1, c_back) # Draw a box to contain the input

	input = self.get_data("input")
	if input:
		font.draw_fast(cx, cy, input, c_text)
	if self.get_data("has_focus") == True:
		if bee.get_ticks()//500 % 2: # Draw a blinking cursor that changes every 500 ticks
			font.draw_fast(cx + self.get_data("input_width"), cy, "_", c_text)

	if self.get_data("rows") == 1:
		# Draw any completions in a box below the input line
		completions = self.get_data("completions")
		if len(completions) > 1: # If completions exist, draw them
			# Draw a box to contain the completions
			bee.render.draw_quad(
				(cx, cy+h, 0),
				(w, len(completions)*h, 0),
				-1, c_back
			)

			# Iterate over the completions
			for i in range(len(completions)):
				# Prepend each completion with a space
				cmd = ' ' + completions[i]

				# If the completion is selected, replace the space with a cursor
				if i == self.get_data("completion_index"):
					cmd = '>' + cmd[1:]

				# Draw the completions
				font.draw_fast(cx, cy + h*(i+1), cmd, c_text)

	bee.render.set_is_lightable(True)

def io(self, data: list):
	if data[0] in globals():
		globals()[data[0]](self, *data[1:])

for a in dir(obj_element):
	if not a.startswith("_") and a not in globals():
		globals()[a] = getattr(obj_element, a)

def reset_completion(self):
	self.set_data("completions", [])
	self.set_data("completion_index", -1)
def set_size(self, rows: int, cols: int):
	self.set_data("rows", rows)
	self.set_data("cols", cols)

	self.set_data("w", self.get_data("char_width") * cols)
	self.set_data("h", self.get_data("char_height") * rows)
def set_input(self, new_input: str):
	self.set_data("input", new_input)
	self.set_data("input_width", len(new_input) * self.get_data("char_width"))

def complete(self, input: str):
	completions = bee.ui.text_entry_completor(self, input)

	new_input = input
	if len(completions) == 0:
		self.set_data("completion_index", -1)
	elif len(completions) == 1:
		new_input = completions[0]
		completions.clear()
		self.set_data("completion_index", -1)
	else:
		# Attempt partial completion if all completions share a common root
		# ! Note that by definition, all completions will share at least as many characters as the length of the input
		matched = "" # Define a string that contains the matched portion of multiple completions
		is_matched = True
		for i in range(len(new_input), len(completions[0])): # Iterate over the characters of the first completion, beginning at the first character that might not match
			for j in range(1, len(completions)): # Iterate over the other completions
				if completions[0][i] != completions[j][i]: # If the given character does not match each completion, then break
					is_matched = False
					break

			if not is_matched:
				break

			# If the character matched, append it to the match string
			matched += completions[0][i]

		# Replace the input with the matched portion of a completion
		new_input += matched

	self.set_data("completions", completions)
	set_input(self, new_input)
