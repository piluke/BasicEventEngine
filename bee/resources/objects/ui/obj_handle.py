###############################################################################
# Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEE.
# BEE is free software and comes with ABSOLUTELY NO WARRANTY
# See LICENSE for more details.
###############################################################################

import bee

obj_element = bee._import("resources/objects/ui/obj_element.py")

def create(self):
	obj_element.create(self)

	if not self.has_data("parent"):
		bind(self, None)
	else:
		bind(self, self.get_data("parent"))
	self.set_data("parent_mass", 0.0)

	self.set_data("mouse_offset", (0, 0))
def destroy(self):
	bee.ui.destroy_handle(self)

def step_end(self):
	parent = self.get_data("parent")
	if parent:
		self.set_corner(
			parent.get_corner()[0] - self.get_data("parent_offset")[0],
			parent.get_corner()[1] - self.get_data("parent_offset")[1]
		)

def mouse_press(self, e):
	obj_element.mouse_press(self, e)

	if self.get_data("is_pressed") == True:
		self.set_data("mouse_offset", (
			self.get_corner()[0] - e["x"],
			self.get_corner()[1] - e["y"]
		))

		parent = self.get_data("parent")
		if parent:
			parent.get_physbody().anchor()
def mouse_input(self, e):
	return
	obj_element.mouse_input(self, e)

	if self.get_data("is_pressed") != True:
		return

	if e["type"] == bee.E_SDL_EVENT_TYPE["MOUSEMOTION"] and e["state"] & bee.E_SDL_BUTTON_MASK["LEFT"]:
		nx = e["x"] + self.get_data("mouse_offset")[0]
		ny = e["y"] + self.get_data("mouse_offset")[1]

		self.set_corner(nx, ny)

		parent = self.get_data("parent")
		if parent:
			parent.set_corner(
				nx + self.get_data("parent_offset")[0],
				ny + self.get_data("parent_offset")[1]
			)

			# Reset the anchor to the current position
			parent.get_physbody().unanchor()
			parent.get_physbody().anchor()
def mouse_release(self, e):
	was_pressed = self.get_data("is_pressed")

	obj_element.mouse_release(self, e)

	parent = self.get_data("parent")
	if was_pressed and parent:
		# Reset the parent's anchoring via its computation type
		parent.set_computation_type(parent.get_computation_type())

def draw(self):
	if self.get_data("is_visible") != True:
		return

	bee.render.set_is_lightable(False)

	w = self.get_data("w")
	h = self.get_data("h")

	c_back = self.get_data("color")
	c_stripe = (255, 255, 255, 255)
	if (c_back[0]+c_back[1]+c_back[2]) / 3 > 127:
		c_stripe = (0, 0, 0, 255)

	cx, cy = self.get_corner()

	bee.render.draw_quad((cx, cy, 0), (w, h, 0), -1, c_back)

	stripe_width = 10
	stripe_amount = w // stripe_width
	for i in range(stripe_amount):
		bee.render.draw_line(
			(cx + i*stripe_width, cy+h, 0),
			(cx + (i+1)*stripe_width, cy, 0),
			c_stripe
		)

	bee.render.set_is_lightable(True)

def io(self, data: list):
	if data[0] in globals():
		globals()[data[0]](self, *data[1:])

for a in dir(obj_element):
	if not a.startswith("_") and a not in globals():
		globals()[a] = getattr(obj_element, a)

def bind(self, parent):
	self.set_data("parent", parent)
	if parent:
		self.set_data("parent_offset", (
			parent.get_corner()[0] - self.get_corner()[0],
			parent.get_corner()[1] - self.get_corner()[1]
		))
	else:
		self.set_data("parent_offset", (0, 0))
