###############################################################################
# Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEE.
# BEE is free software and comes with ABSOLUTELY NO WARRANTY
# See LICENSE for more details.
###############################################################################

import bee
util = bee._import("resources/scripts/util.py")

@util.assert_func
def statemachine1():
    global sm
    sm = bee.StateMachine()
    util._assert_eq(sm.get_states(False), "None")

    util._assert_eq(sm.add_state("1", ["2"], start=lambda: bee.console.commands.log("start1")), 0)
    util._assert_eq(sm.get_graph(), "1: 2\nNone: *")
    util._assert_eq(sm.add_state("2", ["1"], start=lambda: bee.console.commands.log("start2")), 0)
    util._assert_eq(sm.get_graph(), "1: 2\n2: 1\nNone: *")

    util._assert_eq(sm.push_state("1"), 0)
    util._assert_eq(sm.get_states(False), "None 1")
    util._assert_eq(sm.push_state("2"), 0)
    util._assert_eq(sm.get_states(False), "None 1 2")
    util._assert_eq(sm.push_state("1"), 0)
    util._assert_eq(sm.get_states(False), "None 1 2 1")

    util._assert_eq(sm.pop_state(), 0)
    util._assert_eq(sm.get_states(False), "None 1 2")
    util._assert_eq(sm.pop_state(), 0)
    util._assert_eq(sm.get_states(False), "None 1")
    util._assert_eq(sm.pop_state(), 1)
    util._assert_eq(sm.get_states(False), "None 1")
    util._assert_eq(sm.pop_state_all("1"), 1)
    util._assert_eq(sm.get_states(False), "None")

    return 0
@util.assert_func
def statemachine2():
    global sm
    util._assert_eq(sm.get_graph(), "1: 2\n2: 1\nNone: *")
    util._assert_eq(sm.get_states(False), "None")

    util._assert_eq(sm.remove_state("1"), 0)
    util._assert_eq(sm.get_graph(), "2: 1\nNone: *")
    util._assert_eq(sm.get_states(False), "None")
    util._assert_eq(sm.remove_state("2"), 0)
    util._assert_eq(sm.get_graph(), "None: *")

    del sm

    return 0
