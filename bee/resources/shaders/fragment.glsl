/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

// Version is included before OpenGL initialization

in vec4 f_position;
in vec3 f_normal;
in vec2 f_texcoord;

out vec4 f_fragment;

uniform vec3 f_camera;
uniform sampler2D f_texture;
struct Material {
	float spec_intensity;
	float spec_power;
};
uniform Material f_material;

uniform vec4 colorize = vec4(1.0, 1.0, 1.0, 1.0);
uniform int is_primitive = 0;
uniform int flip = 0;
uniform int is_lightable = 1;

struct Line {
	vec4 start;
	vec4 end;
};

// Lighting
const int BEE_LIGHT_AMBIENT = 1;
const int BEE_LIGHT_DIFFUSE = 2;
const int BEE_LIGHT_POINT = 3;
const int BEE_LIGHT_SPOT = 4;

const int BEE_MAX_LIGHTS = 8;

uniform int light_amount = 0;
struct LightData {
	int type;
	vec4 position;
	vec4 direction;
	vec4 attenuation;
	vec4 color;
};
uniform LightData lights[BEE_MAX_LIGHTS];

uniform int is_gen_lightmap = 0;
uniform sampler2D lightmap_static;
uniform sampler2D lightmap_semistatic;

// Shadows
const int BEE_MAX_LIGHTABLES = 96;
const int BEE_MAX_MASK_VERTICES = 8;

uniform int lightable_amount = 0;
struct Lightable {
	vec4 position;
	int vertex_amount;
	vec4 mask[BEE_MAX_MASK_VERTICES];
};
uniform Lightable lightables[BEE_MAX_LIGHTABLES];

float calc_attenuation(vec3 a, float d) {
	return a.x + a.y * d + a.z * d*d;
}
vec4 calc_light_ambient(LightData ld) {
	return vec4(ld.color.rgb*ld.color.a, 1.0);
}
vec4 calc_light_internal(LightData ld, vec4 direction, vec3 normal) {
	ld.direction = vec4(normalize(direction.xyz), direction.w);
	normal = normalize(normal);

	vec4 ambient = calc_light_ambient(ld);
	vec4 diffuse = vec4(0.0);
	vec4 specular = vec4(0.0);

	float diff_factor = dot(normal, -ld.direction.xyz);
	if (diff_factor > 0) {
		diffuse = vec4(ld.color.rgb * ld.direction.w * diff_factor, 1.0);

		vec3 eye = normalize(f_camera - f_position.xyz);
		vec3 reflection = normalize(reflect(ld.direction.xyz, normal));
		float spec_factor = dot(eye, reflection);
		if (spec_factor > 0) {
			spec_factor = pow(spec_factor, f_material.spec_power);
			specular = vec4(ld.color.rgb * f_material.spec_intensity * spec_factor, 1.0);
		}
	}

	return ambient + diffuse + specular;
}
vec4 calc_light_diffuse(LightData ld) {
	return calc_light_internal(ld, ld.direction, f_normal);
}
vec4 calc_light_point(LightData ld) {
	vec3 direction = f_position.xyz - ld.position.xyz;
	float dist = length(direction.xyz);

	float attenuation = calc_attenuation(ld.attenuation.xyz, dist);

	return calc_light_internal(ld, vec4(direction, ld.direction.w), f_normal) / attenuation;
}
vec4 calc_light_spot(LightData ld) {
	vec3 direction = normalize(f_position.xyz - ld.position.xyz);

	float spot_factor = dot(direction, ld.direction.xyz);
	float cone_angle = cos(radians(ld.attenuation.w));
	if (spot_factor <= cone_angle) {
		return vec4(0.0);
	}

	vec4 point = calc_light_point(ld);
	return point * (1.0 - (1.0 - spot_factor) * 1.0/(1.0 - cone_angle));
}

float length_sqr(vec4 a) {
	return a.x*a.x + a.y*a.y + a.z*a.z;
}
float distance_to_line(Line l, vec4 p) {
	vec4 dir = l.end - l.start;
	vec3 normal_dir = cross(dir.xyz, vec3(0.0, 0.0, 1.0));
	vec4 point_dir = l.start - p;
	return dot(normalize(normal_dir), point_dir.xyz);
}
bool check_is_inside(vec4 p, vec4 vertices[BEE_MAX_MASK_VERTICES]) {
	bool is_intersecting = false;
	int v = BEE_MAX_MASK_VERTICES;
	for (int i=0, e=v-1; i<v; e=i++) {
		if ((
			(vertices[i].y > p.y) != (vertices[e].y > p.y)
		)&&(
			p.x < (vertices[e].x - vertices[i].x) * (p.y - vertices[i].y) / (vertices[e].y - vertices[i].y) + vertices[i].x
		)) {
			is_intersecting = !is_intersecting;
		}
	}

	return is_intersecting;
}
float calc_shadow_point(LightData ld, Lightable s) {
	Line line = Line(ld.position, f_position);

	int c = -1;
	float d = distance_to_line(line, s.position+s.mask[0]);
	for (int i=1; i<BEE_MAX_MASK_VERTICES; i++) {
		if (i >= s.vertex_amount) {
			break;
		}

		vec4 p = s.position+s.mask[i];
		if (length_sqr(line.end-line.start) < length_sqr(p-line.end)) {
			continue;
		}
		if (length_sqr(line.end-line.start) < length_sqr(line.start-p)) {
			return 0.0;
		}

		float dd = distance_to_line(line, p);
		if (dd * d < 0) { // If the sign changed then there is an intersection
			//c = i;
			//break;

			c = 1;
		}

		if (abs(dd) < abs(d)) {
			d = dd;
		}
	}

	/*if (c >= 0) {
		vec4 p = s.position+s.mask[c];
		return abs(distance_to_line(Line(line.start, p), line.end));
	}
	return 0.0;*/
	return abs(d)*c;
	//return 20.0*c;
}

void main() {
	// Get the unlit fragment color
	f_fragment = vec4(0.0, 0.0, 0.0, 0.0);
	if (bool(is_primitive)) {
		f_fragment = colorize;
	} else {
		if (flip == 1) {
			f_fragment = texture(f_texture, vec2(1.0-f_texcoord.x, f_texcoord.y));
		} else if (flip == 2) {
			f_fragment = texture(f_texture, vec2(f_texcoord.x, 1.0-f_texcoord.y));
		} else if (flip == 3) {
			f_fragment = texture(f_texture, 1.0-f_texcoord);
		} else {
			f_fragment = texture(f_texture, f_texcoord);
		}

		f_fragment *= colorize;
	}

	// Compute lighting if necessary
	if ((bool(is_lightable))&&(bool(is_primitive) ? (bool(is_gen_lightmap)) : true)) {
		vec4 f = vec4(0.0, 0.0, 0.0, 0.0);
		if (light_amount > 0) {
			LightData ld;
			for (int i=0; i<light_amount; i++) {
				ld = lights[i];
				ld.position = vec4(ld.position.xy, ld.position.zw);
				float max_range_sqr = pow(4.0*50.0/ld.attenuation.z, 2);

				vec4 ff = vec4(0.0, 0.0, 0.0, 0.0);
				switch (ld.type) {
					case BEE_LIGHT_AMBIENT: {
						ff = calc_light_ambient(ld);
						break;
					}
					case BEE_LIGHT_DIFFUSE: {
						ff = calc_light_diffuse(ld);
						break;
					}
					case BEE_LIGHT_POINT: {
						ff = calc_light_point(ld);
						break;
					}
					case BEE_LIGHT_SPOT: {
						ff = calc_light_spot(ld);
						break;
					}
				}

				/*if (ff != vec4(0.0, 0.0, 0.0, 0.0)) {
					switch (ld.type) {
						case BEE_LIGHT_AMBIENT: {
							break;
						}
						case BEE_LIGHT_DIFFUSE: {
							break;
						}
						case BEE_LIGHT_POINT: {
							float shadow_amount = 0.0;
							for (int e=0; e<lightable_amount; e++) {
								float s = calc_shadow_point(ld, lightables[e]);
								if (s > 0) {
									shadow_amount += s/50.0;
								}
							}

							float d = length(ld.position - f_position) / 100.0;
							if (d < 1.0) {
								d = 1.0;
							}
							ff /= pow(d, shadow_amount);

							break;
						}
						case BEE_LIGHT_SPOT: {
							break;
						}
					}
				}*/
				f += ff;
			}
		}

		// Apply lightmaps
		if (bool(is_gen_lightmap)) {
			f_fragment = vec4(0.0);
			if (light_amount > 0) {
				f_fragment = f;
			}
		} else {
			if (f != vec4(0.0)) {
				//f_fragment *= vec4(f.rgb, f_fragment.a);
			}

			vec4 lms = texture(lightmap_static, f_position.xy);
			vec4 lmss = texture(lightmap_semistatic, f_position.xy);

			//f_fragment = lms;
			//f_fragment = lmss;
			//f_fragment = f;

			/*f += lms;
			f += lmss;
			f_fragment *= vec4(f.rgb, f_fragment.a);*/

			f += lms;
			f += lmss;
			if (f != vec4(0.0)) {
				f_fragment *= vec4(f.rgb, f_fragment.a);
			}
		}
	}

	// Normalize the fragment color
	if (f_fragment.r > 1.0) {
		f_fragment /= vec4(f_fragment.rrr, 1.0);
	}
	if (f_fragment.g > 1.0) {
		f_fragment /= vec4(f_fragment.ggg, 1.0);
	}
	if (f_fragment.b > 1.0) {
		f_fragment /= vec4(f_fragment.bbb, 1.0);
	}
}
