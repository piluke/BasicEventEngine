/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_RENDER_RGBA_H
#define BEE_RENDER_RGBA_H 1

#include <array>

#include <SDL2/SDL.h>

#include "../enum.hpp"

namespace bee {
	struct RGBA {
		Uint8 r, g, b, a;

		RGBA(int, int, int, int);
		RGBA(E_RGB, Uint8);
		RGBA(E_RGB);
		RGBA();

		RGBA get_inverse();

		std::array<float,3> get_hsv() const;
		float get_hue() const;
		float get_saturation() const;
		float get_value() const;

		void set_hsv(const std::array<float,3>&);
		void set_hue(float);
		void set_saturation(float);
		void set_value(float);
		void add_hue(float);
		void add_saturation(float);
		void add_value(float);

		bool operator==(const RGBA&) const;
		bool operator!=(const RGBA&) const;

		friend std::ostream& operator<<(std::ostream&, const RGBA&);
	};
}

#endif // BEE_RENDER_RGBA_H
