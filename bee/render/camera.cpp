/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include "camera.hpp"

namespace bee {
	Camera::Camera() :
		position(),
		direction(),
		orientation(),

		width(0.0),
		height(0.0),

		fov(90.0),
		z_near(1.0),
		z_far(10000.0)
	{}
	Camera::Camera(float w, float h) :
		Camera()
	{
		width = w;
		height = h;
	}
	Camera::Camera(glm::vec3 p, glm::vec3 d, glm::vec3 o) :
		Camera()
	{
		position = p;
		direction = d;
		orientation = o;
	}

	bool Camera::operator==(const Camera& other) const {
		return (
			(this->position == other.position)
			&&(this->direction == other.direction)
			&&(this->orientation == other.orientation)
			&&(this->width == other.width)
			&&(this->height == other.height)
			&&(this->fov == other.fov)
			&&(this->z_near == other.z_near)
			&&(this->z_far == other.z_far)
		);
	}
}
