/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_RENDER_H
#define BEE_RENDER_H 1

#include <string>
#include <memory>

#include <glm/glm.hpp>

#include "../enum.hpp"

namespace bee {
	class Texture;
	struct TextureDrawData;
	struct LightData;

	struct Camera;
	struct ViewPort;
	class ShaderProgram;

namespace render {
	namespace internal {
		int init_lightmaps();

		void render_texture(const TextureDrawData&);
		void set_light(int, std::shared_ptr<LightData>);

		void update_camera();
	}

	void set_is_lightable(bool);

	std::string opengl_prepend_version(const std::string&);

	bool get_3d();
	void set_3d(bool);
	const Camera& get_camera();
	int set_camera(const Camera&);
	int reset_camera();
	const glm::mat4& get_projection();
	const glm::mat4& calc_projection();

	void set_viewport(std::shared_ptr<ViewPort>);

	void queue_texture(const Texture*, const TextureDrawData&);
	size_t render_textures();

	int queue_light(E_COMPUTATION, std::shared_ptr<LightData>);
	size_t render_lights();
	int clear_lights();

	int reset_target();
	int set_target(Texture*);
	Texture* get_target();
	void set_program(ShaderProgram*);
	ShaderProgram* get_program();

	void clear();
	void render();
}}

#endif // BEE_RENDER_H
