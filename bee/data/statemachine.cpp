/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include <algorithm>

#include "statemachine.hpp" // Include the engine headers

#include "../engine.hpp"

#include "../messenger/messenger.hpp"

namespace bee {
	/**
	* Construct a State with the given name and outputs.
	* @param _name the State name
	* @param _outputs the names of valid States that can be transitioned to after this one
	*/
	State::State(const std::string& _name, const std::set<std::string>& _outputs) :
		name(_name),
		outputs(_outputs),

		start_func(nullptr),
		update_func(nullptr),
		end_func(nullptr)
	{}

	/**
	* Run the State's start_func if it's set.
	*/
	void State::start() {
		if (start_func != nullptr) {
			start_func();
		}
	}
	/**
	* Run the State's update_func if it's set.
	*/
	void State::update(Uint32 ticks) {
		if (update_func != nullptr) {
			update_func(ticks);
		}
	}
	/**
	* Run the State's end_func if it's set.
	*/
	void State::end() {
		if (end_func != nullptr) {
			end_func();
		}
	}

	/**
	* Construct an empty StateMachine to manage States.
	*/
	StateMachine::StateMachine() :
		stack(),
		graph()
	{
		init();
	}
	/**
	* Initialize the StateMachine with the None-state.
	*
	* @retval 0 success
	* @retval 1 failed to add the None-state to the graph
	*/
	int StateMachine::init() {
		State none ("None", {"*"});
		if (add_state(none)) {
			return 1;
		}
		stack.push_front(none.name);
		return 0;
	}

	/**
	* Print the currently active States.
	*/
	void StateMachine::print_state() {
		messenger::send({"engine", "statemachine"}, E_MESSAGE::INFO, get_states());
	}
	/**
	* Print the graph of possible States.
	*/
	void StateMachine::print_graph() {
		messenger::send({"engine", "statemachine"}, E_MESSAGE::INFO, get_graph());
	}

	/**
	* Add the given State to the graph of possible States.
	* @param state the State to add
	*
	* @retval 0 success
	* @retval 1 failed since the graph contains another State with the same name
	*/
	int StateMachine::add_state(const State& state) {
		if (graph.find(state.name) != graph.end()) {
			return 1;
		}

		graph.emplace(state.name, state);

		return 0;
	}
	/**
	* Remove the State with the given name from the graph of possible States.
	* @param state_name the name of the State to remove
	*
	* @retval 0 success
	* @retval 1 failed since a state with the given name couldn't be found
	*/
	int StateMachine::remove_state(const std::string& state_name) {
		std::map<std::string,State>::iterator state (graph.find(state_name));
		if (state == graph.end()) {
			return 1;
		}

		graph.erase(state);

		return 0;
	}
	/**
	* Remove all States and reinitialize the StateMachine.
	*/
	void StateMachine::clear() {
		stack.clear();
		graph.clear();
		init();
	}

	/**
	* @param n the index of the desired State
	* @returns the name of the State with the given index in the stack of States or an empty string if the given index is too large
	*/
	std::string StateMachine::get_state(size_t n) const {
		if (n >= stack.size()) {
			return {};
		}
		return (*std::next(stack.begin(), n));
	}
	/**
	* @returns the name of the State at the top of the stack of States or an empty string if the stack is empty
	*/
	std::string StateMachine::get_state() const {
		return get_state(0);
	}
	/**
	* @param is_forward whether to order the States in forward or reverse order
	*
	* @returns a string of space-separated State names on the stack
	*/
	std::string StateMachine::get_states(bool is_forward) const {
		std::string states;
		if (is_forward) {
			for (auto& s : stack) {
				states += s + " ";
			}
		} else {
			for (auto it=stack.rbegin(); it!=stack.rend(); ++it) {
				states += *it + " ";
			}
		}

		if (!states.empty()) {
			states.pop_back();
		}

		return states;
	}
	/**
	* @returns a string of space-separated State names on the stack
	*/
	std::string StateMachine::get_states() const {
		return get_states(true);
	}
	/**
	* @returns a string of possible States and their associated outputs
	*/
	std::string StateMachine::get_graph() const {
		std::string graph_str;
		for (auto& s : graph) {
			graph_str += s.first + ": ";
			for (auto& o : s.second.outputs) {
				graph_str += o + ", ";
			}
			graph_str.pop_back();
			graph_str.pop_back();

			graph_str += "\n";
		}
		if (!graph_str.empty()) {
			graph_str.pop_back();
		}
		return graph_str;
	}
	/**
	* @param state_name the name of the State to check for
	*
	* @returns whether the State with the given name is on the stack
	*/
	bool StateMachine::has_state(const std::string& state_name) const {
		for (auto& s : stack) {
			if (s == state_name) {
				return true;
			}
		}
		return false;
	}

	/**
	* Push the State with given name onto the stack.
	* @note On success, the State's start_func will be run automatically.
	* @param state_name the name of the State to attempt to push
	*
	* @retval 0 success
	* @retval 1 failed since the given name is not a valid output of the current State
	* @retval 2 failed since the given name is the same as the current State
	*/
	int StateMachine::push_state(const std::string& state_name) {
		const std::set<std::string>& outputs = graph.at(get_state()).outputs;
		if (
			(outputs.find(state_name) == outputs.end())
			&&(outputs.find("*") == outputs.end())
		) {
			return 1;
		}

		if (get_state() == state_name) {
			return 2;
		}

		stack.push_front(state_name);
		graph.at(get_state()).start();

		return 0;
	}
	/**
	* Pop the current State from the stack.
	* @note On success, the State's end_func will be run automatically.
	*
	* @retval 0 success
	* @retval 1 failed since the previous State is not a valid output of the current State
	*/
	int StateMachine::pop_state() {
		const std::set<std::string>& outputs = graph.at(get_state()).outputs;
		if (outputs.find(get_state(1)) == outputs.end()) {
			return 1;
		}

		graph.at(get_state()).end();
		stack.pop_front();

		return 0;
	}
	/**
	* Pop the State with the given name from the stack.
	* @note Only the current State is allowed to be popped.
	* @param state_name the name of the State to attempt to pop
	*
	* @retval 0 success
	* @retval 1 failed since the previous State is not a valid output of the current State
	* @retval 2 failed since the given name is not the name of the current State
	*/
	int StateMachine::pop_state(const std::string& state_name) {
		if (get_state() != state_name) {
			return 2;
		}

		return pop_state();
	}
	/**
	* Pop all instances of the State with the given name from the stack.
	* @note On success, the State's end_func will be run automatically.
	* @param state_name the name of the State to attempt to pop
	*
	* @returns the number of State instances that were popped or 0 on failure to find a State with the given name
	*/
	size_t StateMachine::pop_state_all(const std::string& state_name) {
		if ((graph.find(state_name) == graph.end())||(std::find(stack.begin(), stack.end(), state_name) == stack.end())) {
			return 0;
		}

		graph.at(state_name).end();

		size_t orig_size = stack.size();
		stack.erase(std::remove_if(stack.begin(), stack.end(), [&state_name] (const std::string& n) -> bool {
			return (n == state_name);
		}), stack.end());

		return orig_size - stack.size();
	}
	/**
	* Pop all States from the stack.
	* @note If the None-state exists, it will be repushed.
	*/
	void StateMachine::pop_all() {
		stack.clear();
		if (graph.find("None") != graph.end()) {
			stack.push_front("None");
		}
	}

	/**
	* Call the current State's update_func.
	*/
	void StateMachine::update() {
		graph.at(get_state()).update(get_ticks());
	}
	/**
	* Call the update_func for all States on the stack.
	*/
	void StateMachine::update_all() {
		Uint32 t = get_ticks();
		for (size_t i=0; i<stack.size(); ++i) { // Use a normal for loop to allow state changes during iteration
			graph.at(stack.at(i)).update(t);
		}
	}
}
