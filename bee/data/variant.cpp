/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include <sstream>

#include "variant.hpp"

#include "../util/string.hpp"
#include "../util/platform.hpp"
#include "../util/template/string.hpp"

#include "../enum.hpp"
#include "../engine.hpp"

#include "../messenger/messenger.hpp"

namespace bee {
	Variant::Variant() :
		Variant(E_DATA_TYPE::NONE)
	{}
	Variant::Variant(E_DATA_TYPE _type) :
		type(_type),
		ptype(std::type_index(typeid(nullptr)))
	{
		switch (type) {
			case E_DATA_TYPE::NONE:
			case E_DATA_TYPE::POINTER: {
				p = nullptr;
				break;
			}
			case E_DATA_TYPE::CHAR: {
				c = '\0';
				break;
			}
			case E_DATA_TYPE::INTEGER: {
				i = 0;
				break;
			}
			case E_DATA_TYPE::FLOATING: {
				f = 0.0;
				break;
			}
			case E_DATA_TYPE::STRING: {
				new (&s) std::string;
				break;
			}
			case E_DATA_TYPE::VECTOR: {
				new (&v) std::vector<Variant>;
				break;
			}
			case E_DATA_TYPE::MAP: {
				new (&m) std::map<Variant,Variant>;
				break;
			}
			case E_DATA_TYPE::SERIAL: {
				new (&sd) SerialData;
				break;
			}
			case E_DATA_TYPE::SHARED_PTR: {
				new (&sp) std::shared_ptr<void>;
				break;
			}
		}
	}
	Variant::Variant(const Variant& var) :
		type(var.type),
		ptype(std::type_index(typeid(nullptr)))
	{
		switch (type) {
			case E_DATA_TYPE::NONE:
			case E_DATA_TYPE::POINTER: {
				p = var.p;
				ptype = var.get_ptype();
				break;
			}
			case E_DATA_TYPE::CHAR: {
				c = var.c;
				break;
			}
			case E_DATA_TYPE::INTEGER: {
				i = var.i;
				break;
			}
			case E_DATA_TYPE::FLOATING: {
				f = var.f;
				break;
			}
			case E_DATA_TYPE::STRING: {
				new (&s) std::string(var.s);
				break;
			}
			case E_DATA_TYPE::VECTOR: {
				new (&v) std::vector<Variant>(var.v);
				break;
			}
			case E_DATA_TYPE::MAP: {
				new (&m) std::map<Variant,Variant>(var.m);
				break;
			}
			case E_DATA_TYPE::SERIAL: {
				new (&sd) SerialData(var.sd);
				break;
			}
			case E_DATA_TYPE::SHARED_PTR: {
				new (&sp) std::shared_ptr<void>(var.sp);
				ptype = var.get_ptype();
				break;
			}
		}
	}
	Variant::Variant(const std::string& _s, bool should_interpret) :
		Variant()
	{
		if (should_interpret) {
			interpret(_s);
		} else {
			type = E_DATA_TYPE::STRING;
			new (&s) std::string(_s);
		}
	}
	Variant::Variant(const char* _s) :
		Variant(std::string(_s))
	{}
	Variant::Variant(std::nullptr_t np) :
		Variant(E_DATA_TYPE::POINTER)
	{}
	/*Variant::Variant(std::initializer_list<Variant> il) :
		Variant()
	{
		std::vector<Variant> v;
		v.reserve(il.size());
		for (auto& e : il) {
			v.push_back(e);
		}

		*this = Variant(v);
	}*/

	Variant::Variant(unsigned char _c) :
		type(E_DATA_TYPE::CHAR),
		ptype(std::type_index(typeid(nullptr))),
		c(_c)
	{}
	Variant::Variant(bool _b) :
		Variant(static_cast<long>(_b))
	{}
	Variant::Variant(int _i) :
		Variant(static_cast<long>(_i))
	{}
	Variant::Variant(long _i) :
		type(E_DATA_TYPE::INTEGER),
		ptype(std::type_index(typeid(nullptr))),
		i(_i)
	{}
	Variant::Variant(float _f) :
		Variant(static_cast<double>(_f))
	{}
	Variant::Variant(double _f) :
		type(E_DATA_TYPE::FLOATING),
		ptype(std::type_index(typeid(nullptr))),
		f(_f)
	{}
	Variant::Variant(const std::string& _s) :
		type(E_DATA_TYPE::STRING),
		ptype(std::type_index(typeid(nullptr))),
		s(_s)
	{}
	Variant::Variant(const std::vector<Variant>& _v) :
		type(E_DATA_TYPE::VECTOR),
		ptype(std::type_index(typeid(nullptr))),
		v(_v)
	{}
	Variant::Variant(const std::map<Variant,Variant>& _m) :
		type(E_DATA_TYPE::MAP),
		ptype(std::type_index(typeid(nullptr))),
		m(_m)
	{}
	Variant::Variant(const SerialData& _sd) :
		type(E_DATA_TYPE::SERIAL),
		ptype(std::type_index(typeid(nullptr))),
		sd(_sd)
	{}
	// See variant.hpp for Variant(T*)
	Variant::Variant(const std::shared_ptr<void>& _sp, const std::type_index& pt) :
		type(E_DATA_TYPE::SHARED_PTR),
		ptype(pt),
		sp(_sp)
	{}

	Variant::~Variant() {
		reset();
	}
	int Variant::reset() {
		switch (type) {
			case E_DATA_TYPE::STRING: {
				s.clear();
				s.~basic_string();
				break;
			}
			case E_DATA_TYPE::VECTOR: {
				v.clear();
				v.~vector<Variant>();
				break;
			}
			case E_DATA_TYPE::MAP: {
				m.clear();
				m.~map<Variant,Variant>();
				break;
			}
			case E_DATA_TYPE::SERIAL: {
				sd.reset();
				sd.~SerialData();
				break;
			}
			case E_DATA_TYPE::SHARED_PTR: {
				sp.reset();
				sp.~shared_ptr<void>();
				break;
			}
			default: {}
		}

		type = E_DATA_TYPE::NONE;
		ptype = std::type_index(typeid(nullptr));
		p = nullptr;

		return 0;
	}

	E_DATA_TYPE Variant::get_type() const {
		return type;
	}
	std::type_index Variant::get_ptype() const {
		return ptype;
	}

	int Variant::interpret(const std::string& ns) {
		reset();
		if (ns.empty()) {
			return 0;
		}

		try {
			std::string _ns (util::trim(ns));
			if ((_ns[0] == '"')&&(_ns[_ns.length()-1] == '"')) { // String
				_ns = _ns.substr(1, _ns.length()-2);
				type = E_DATA_TYPE::STRING;
				new (&s) std::string(util::string::unescape(_ns));
			} else if ((_ns[0] == '[')&&(_ns[_ns.length()-1] == ']')) { // Vector
				type = E_DATA_TYPE::VECTOR;
				new (&v) std::vector<Variant>;
				util::vector_deserialize(_ns, &v);
			} else if ((_ns[0] == '{')&&(_ns[_ns.length()-1] == '}')) { // Map
				type = E_DATA_TYPE::MAP;
				new (&m) std::map<Variant,Variant>;
				util::map_deserialize(_ns, &m);
			} else if (util::string::is_integer(_ns)) { // Long
				type = E_DATA_TYPE::INTEGER;
				i = std::stol(_ns);
			} else if (util::string::is_floating(_ns)) { // Double
				type = E_DATA_TYPE::FLOATING;
				f = std::stod(_ns);
			} else if ((_ns == "true")||(_ns == "false")) { // Boolean
				type = E_DATA_TYPE::INTEGER;
				i = (_ns == "true");
			} else { // Probably a string
				messenger::send({"engine", "variant"}, E_MESSAGE::WARNING, "Variant type not determined, storing as string: \"" + _ns + "\"");
				type = E_DATA_TYPE::STRING;
				new (&s) std::string(_ns);
			}
		} catch (const std::invalid_argument&) {}

		if (type == E_DATA_TYPE::NONE) { // No possible type, this will only occur when std::stod or std::stoi fails
			messenger::send({"engine", "variant"}, E_MESSAGE::WARNING, "Failed to determine Variant type, storing as string: \"" + ns + "\"");
			type = E_DATA_TYPE::STRING;
			new (&s) std::string(ns);
		}

		return 0; // Return 0 on success
	}
	std::string Variant::to_str(bool should_pretty_print) const {
		switch (type) {
			case E_DATA_TYPE::NONE: {
				return {};
			}
			case E_DATA_TYPE::CHAR: {
				return std::string(1, c);
			}
			case E_DATA_TYPE::INTEGER: {
				return std::to_string(i);
			}
			case E_DATA_TYPE::FLOATING: {
				return std::to_string(f);
			}
			case E_DATA_TYPE::STRING: {
				return "\"" + util::string::escape(s) + "\"";
			}
			case E_DATA_TYPE::VECTOR: {
				return util::vector_serialize(v, should_pretty_print);
			}
			case E_DATA_TYPE::MAP: {
				return util::map_serialize(m, should_pretty_print);
			}
			case E_DATA_TYPE::SERIAL: {
				return util::vector_serialize(sd.get(), should_pretty_print);
			}
			case E_DATA_TYPE::POINTER: {
				std::stringstream ss;
				ss << p;
				return ss.str();
			}
			case E_DATA_TYPE::SHARED_PTR: {
				std::stringstream ss;
				ss << sp.get();
				return "shared<"+ ss.str() +">";
			}
			default: {
				throw std::runtime_error("Error: Attempt to stringify Variant of invalid type");
			}
		}
	}
	std::string Variant::to_str() const {
		return to_str(false);
	}

	Variant& Variant::operator=(const Variant& rhs) {
		if (this != &rhs) {
			this->reset();

			this->type = rhs.type;
			switch (this->type) {
				case E_DATA_TYPE::NONE:
				case E_DATA_TYPE::POINTER: {
					this->p = rhs.p;
					this->ptype = rhs.get_ptype();
					break;
				}
				case E_DATA_TYPE::CHAR: {
					this->c = rhs.c;
					break;
				}
				case E_DATA_TYPE::INTEGER: {
					this->i = rhs.i;
					break;
				}
				case E_DATA_TYPE::FLOATING: {
					this->f = rhs.f;
					break;
				}
				case E_DATA_TYPE::STRING: {
					new (&s) std::string(rhs.s);
					break;
				}
				case E_DATA_TYPE::VECTOR: {
					new (&v) std::vector<Variant>(rhs.v);
					break;
				}
				case E_DATA_TYPE::MAP: {
					new (&m) std::map<Variant,Variant>(rhs.m);
					break;
				}
				case E_DATA_TYPE::SERIAL: {
					new (&sd) SerialData(rhs.sd);
					break;
				}
				case E_DATA_TYPE::SHARED_PTR: {
					new (&sp) std::shared_ptr<void>(rhs.sp);
					ptype = rhs.get_ptype();
					break;
				}
			}
		}
		return *this;
	}
	Variant& Variant::operator=(std::nullptr_t np) {
		*this = Variant(E_DATA_TYPE::POINTER);
		return *this;
	}
	Variant& Variant::operator=(unsigned char _c) {
		*this = Variant(_c);
		return *this;
	}
	Variant& Variant::operator=(bool _i) {
		*this = Variant(_i);
		return *this;
	}
	Variant& Variant::operator=(int _i) {
		*this = Variant(_i);
		return *this;
	}
	Variant& Variant::operator=(long _i) {
		*this = Variant(_i);
		return *this;
	}
	Variant& Variant::operator=(float _f) {
		*this = Variant(_f);
		return *this;
	}
	Variant& Variant::operator=(double _f) {
		*this = Variant(_f);
		return *this;
	}
	Variant& Variant::operator=(const std::string& _s) {
		*this = Variant(_s);
		return *this;
	}
	Variant& Variant::operator=(const char* _s) {
		*this = Variant(_s);
		return *this;
	}
	Variant& Variant::operator=(const std::vector<Variant>& _v) {
		*this = Variant(_v);
		return *this;
	}
	Variant& Variant::operator=(const std::map<Variant,Variant>& _m) {
		*this = Variant(_m);
		return *this;
	}
	Variant& Variant::operator=(const SerialData& _sd) {
		*this = Variant(_sd);
		return *this;
	}
	// See variant.hpp for operator=(T*) and operator(const std::shared_ptr<T>&)

	bool Variant::operator==(const Variant& rhs) const {
		if (this->type != rhs.type) {
			return false;
		}

		switch (this->type) {
			case E_DATA_TYPE::NONE: {
				return true;
			}
			case E_DATA_TYPE::CHAR: {
				return (this->c == rhs.c);
			}
			case E_DATA_TYPE::INTEGER: {
				return (this->i == rhs.i);
			}
			case E_DATA_TYPE::FLOATING: {
				return (this->f == rhs.f);
			}
			case E_DATA_TYPE::STRING: {
				return (this->s == rhs.s);
			}
			case E_DATA_TYPE::VECTOR: {
				return (this->v == rhs.v);
			}
			case E_DATA_TYPE::MAP: {
				return (this->m == rhs.m);
			}
			case E_DATA_TYPE::SERIAL: {
				//return (this->sd == rhs.sd);
				return false;
			}
			case E_DATA_TYPE::POINTER: {
				return (this->p == rhs.p);
			}
			case E_DATA_TYPE::SHARED_PTR: {
				return (this->sp == rhs.sp);
			}
		}

		return false;
	}
	bool Variant::operator!=(const Variant& rhs) const {
		return !(*this == rhs);
	}

	bool operator<(const Variant& a, const Variant& b) {
		if (a.type != b.type) {
			return (static_cast<unsigned char>(a.type) < static_cast<unsigned char>(b.type)); // Compare the type values so different types can be used in an ordered map
		}

		switch (a.type) {
			case E_DATA_TYPE::NONE: {
				return false;
			}
			case E_DATA_TYPE::CHAR: {
				return (a.c < b.c);
			}
			case E_DATA_TYPE::INTEGER: {
				return (a.i < b.i);
			}
			case E_DATA_TYPE::FLOATING: {
				return (a.f < b.f);
			}
			case E_DATA_TYPE::STRING: {
				return (a.s < b.s);
			}
			case E_DATA_TYPE::VECTOR: {
				return (a.v < b.v);
			}
			case E_DATA_TYPE::MAP: {
				return (a.m < b.m);
			}
			case E_DATA_TYPE::SERIAL: {
				//return (a.sd < b.sd);
				return false;
			}
			case E_DATA_TYPE::POINTER: {
				return (a.p < b.p);
			}
			case E_DATA_TYPE::SHARED_PTR: {
				return (a.sp < b.sp);
			}
			default: {
				throw std::runtime_error("Error: Attempt to compare Variants of invalid types");
			}
		}
	}
	std::ostream& operator<<(std::ostream& os, const Variant& var) {
		os << var.to_str();
		return os;
	}
	std::istream& operator>>(std::istream& is, Variant& var) {
		std::string s (std::istreambuf_iterator<char>(is), {});
		var.interpret(s);
		return is;
	}
}
