/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_PYTHON_H
#define BEE_PYTHON_H 1

#ifdef PYTHON_ENABLED

#include <string>

#include <Python.h>

#include "../data/variant.hpp"

namespace bee { namespace python {
	int init();
	int close();

	int run_string(const std::string&);
	int run_file(const std::string&);

	std::string get_traceback();

	void set_displayhook(PyObject*);
	Variant get_displayhook();

	Variant pyobj_to_variant(PyObject*);
	PyObject* variant_to_pyobj(Variant);
}}

#else // PYTHON_ENABLED

namespace bee { namespace python {
	int init();
	int close();
}}

#endif // PYTHON_ENABLED

#endif // BEE_PYTHON_H
