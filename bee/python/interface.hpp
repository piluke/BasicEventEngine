/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_PYTHON_INTERFACE_H
#define BEE_PYTHON_INTERFACE_H 1

#include <string>

#include <Python.h>

#include "../resource/script.hpp"

namespace bee {
	class PythonScriptInterface: public ScriptInterface {
		std::string path;
		PyObject* module;
	public:
		PythonScriptInterface(const std::string&);
		PythonScriptInterface(PyObject*);
		~PythonScriptInterface();

		int load();
		void free();
		void release();

		int run_string(const std::string&, const std::string&, Variant*, int);
		int run_string(const std::string&, Variant*);
		int run_file(const std::string&);
		int run_func(const std::string&, const Variant&, Variant*);

		int set_var(const std::string&, PyObject*);
		int set_var(const std::string&, const Variant&);
		bool has_var(const std::string&) const;
		Variant get_var(const std::string&) const;

		std::vector<Variant> complete(const std::string&) const;
	};
}

#endif // BEE_PYTHON_INTERFACE_H
