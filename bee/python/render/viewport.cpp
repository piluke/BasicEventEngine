/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include <Python.h>
#include <structmember.h>

#include "viewport.hpp"

#include "../structs.hpp"

#include "../../render/viewport.hpp"

#include "../../resource/texture.hpp"

namespace bee { namespace python {
	PyObject* ViewPort_from(std::weak_ptr<ViewPort> vp) {
		PyObject* py_vp = internal::ViewPort_new(&internal::ViewPortType, nullptr, nullptr);
		internal::ViewPortObject* _py_vp = reinterpret_cast<internal::ViewPortObject*>(py_vp);

		_py_vp->vp = vp;

		return py_vp;
	}
	bool ViewPort_check(PyObject* obj) {
		return PyObject_TypeCheck(obj, &internal::ViewPortType);
	}
namespace internal {
	PyMethodDef ViewPortMethods[] = {
		{"load", reinterpret_cast<PyCFunction>(ViewPort_load), METH_NOARGS, "Load the interior Texture as a target the size of the window"},

		{"get_is_active", reinterpret_cast<PyCFunction>(ViewPort_get_is_active), METH_NOARGS, "Return whether the ViewPort will be drawn"},
		{"get_view", reinterpret_cast<PyCFunction>(ViewPort_get_view), METH_NOARGS, "Return the view, the in-Room area"},
		{"get_port", reinterpret_cast<PyCFunction>(ViewPort_get_port), METH_NOARGS, "Return the port, the on-screen area"},
		{"get_offset", reinterpret_cast<PyCFunction>(ViewPort_get_offset), METH_NOARGS, "Get the offset of the ViewPort from display coordinates"},

		{"set_is_active", reinterpret_cast<PyCFunction>(ViewPort_set_is_active), METH_VARARGS, "Set whether the ViewPort will be drawn"},
		{"set_view", reinterpret_cast<PyCFunction>(ViewPort_set_view), METH_VARARGS, "Set the in-Room area to draw"},
		{"set_view_center", reinterpret_cast<PyCFunction>(ViewPort_set_view_center), METH_VARARGS, "Center the view on the given coordinates"},
		{"set_port", reinterpret_cast<PyCFunction>(ViewPort_set_port), METH_VARARGS, "Set the on-screen area to draw in"},
		{"set_update_func", reinterpret_cast<PyCFunction>(ViewPort_set_update_func), METH_VARARGS, "Set the update callback that runs before drawing"},

		{"update", reinterpret_cast<PyCFunction>(ViewPort_update), METH_NOARGS, "Run the update callback manually"},
		{"draw", reinterpret_cast<PyCFunction>(ViewPort_draw), METH_NOARGS, "Draw the interior Texture with the interior shader"},

		{nullptr, nullptr, 0, nullptr}
	};

	PyTypeObject ViewPortType = {
		PyVarObject_HEAD_INIT(NULL, 0)
		"bee.ViewPort",
		sizeof(ViewPortObject), 0,
		ViewPort_dealloc,
		0,
		0, 0,
		0,
		0,
		0, 0, 0,
		0,
		0,
		0,
		0, 0,
		0,
		Py_TPFLAGS_DEFAULT,
		"ViewPort objects",
		0,
		0,
		0,
		0,
		0, 0,
		ViewPortMethods,
		0,
		0,
		0,
		0,
		0, 0,
		0,
		0,
		0, ViewPort_new,
		0, 0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0
	};

	PyObject* PyInit_bee_render_viewport(PyObject* module) {
		ViewPortType.tp_new = PyType_GenericNew;
		if (PyType_Ready(&ViewPortType) < 0) {
			return nullptr;
		}

		Py_INCREF(&ViewPortType);
		PyModule_AddObject(module, "ViewPort", reinterpret_cast<PyObject*>(&ViewPortType));

		return reinterpret_cast<PyObject*>(&ViewPortType);
	}

	std::weak_ptr<ViewPort> as_viewport(ViewPortObject* self) {
		return self->vp;
	}
	std::weak_ptr<ViewPort> as_viewport(PyObject* self) {
		if (ViewPort_check(self)) {
			return as_viewport(reinterpret_cast<ViewPortObject*>(self));
		}
		return {};
	}

	void ViewPort_dealloc(PyObject* self) {
		Py_TYPE(self)->tp_free(self);
	}
	PyObject* ViewPort_new(PyTypeObject* type, PyObject* args, PyObject* kwds) {
		ViewPortObject* self;

		self = reinterpret_cast<ViewPortObject*>(type->tp_alloc(type, 0));
		if (self != nullptr) {
			self->vp = {};
		}

		return reinterpret_cast<PyObject*>(self);
	}

	PyObject* ViewPort_load(ViewPortObject* self, PyObject* args) {
		auto vp = as_viewport(self).lock();
		if (vp == nullptr) {
			return nullptr;
		}

		return PyLong_FromLong(vp->load());
	}

	PyObject* ViewPort_get_is_active(ViewPortObject* self, PyObject* args) {
		auto vp = as_viewport(self).lock();
		if (vp == nullptr) {
			return nullptr;
		}

		return PyBool_FromLong(vp->is_active);
	}
	PyObject* ViewPort_get_view(ViewPortObject* self, PyObject* args) {
		auto vp = as_viewport(self).lock();
		if (vp == nullptr) {
			return nullptr;
		}

		return SDL_Rect_from(vp->view);
	}
	PyObject* ViewPort_get_port(ViewPortObject* self, PyObject* args) {
		auto vp = as_viewport(self).lock();
		if (vp == nullptr) {
			return nullptr;
		}

		return SDL_Rect_from(vp->port);
	}
	PyObject* ViewPort_get_offset(ViewPortObject* self, PyObject* args) {
		auto vp = as_viewport(self).lock();
		if (vp == nullptr) {
			return nullptr;
		}

		auto o = vp->get_offset();

		return Py_BuildValue("(ii)", o.first, o.second);
	}

	PyObject* ViewPort_set_is_active(ViewPortObject* self, PyObject* args) {
		int is_active;

		if (!PyArg_ParseTuple(args, "p", &is_active)) {
			return nullptr;
		}

		bool _is_active = is_active;

		auto vp = as_viewport(self).lock();
		if (vp == nullptr) {
			return nullptr;
		}

		vp->is_active = _is_active;

		Py_RETURN_NONE;
	}
	PyObject* ViewPort_set_view(ViewPortObject* self, PyObject* args) {
		PyObject* view;

		if (!PyArg_ParseTuple(args, "O", &view)) {
			return nullptr;
		}

		auto vp = as_viewport(self).lock();
		if (vp == nullptr) {
			return nullptr;
		}

		as_rect(view, &vp->view);

		Py_RETURN_NONE;
	}
	PyObject* ViewPort_set_view_center(ViewPortObject* self, PyObject* args) {
		int x, y;

		if (!PyArg_ParseTuple(args, "ii", &x, &y)) {
			return nullptr;
		}

		auto vp = as_viewport(self).lock();
		if (vp == nullptr) {
			return nullptr;
		}

		auto pos = vp->set_view_center(x, y);

		return Py_BuildValue("(ii)", pos.first, pos.second);
	}
	PyObject* ViewPort_set_port(ViewPortObject* self, PyObject* args) {
		PyObject* port;

		if (!PyArg_ParseTuple(args, "O", &port)) {
			return nullptr;
		}

		auto vp = as_viewport(self).lock();
		if (vp == nullptr) {
			return nullptr;
		}

		as_rect(port, &vp->port);

		if (vp->texture->get_is_loaded()) {
			vp->texture->free();
			return PyLong_FromLong(vp->load());
		}

		return PyLong_FromLong(0);
	}
	PyObject* ViewPort_set_update_func(ViewPortObject* self, PyObject* args) {
		PyObject* func;

		if (!PyArg_ParseTuple(args, "O", &func)) {
			return nullptr;
		}

		if (!PyCallable_Check(func)) {
			PyErr_SetString(PyExc_TypeError, "parameter must be callable");
			return nullptr;
		}

		auto vp = as_viewport(self).lock();
		if (vp == nullptr) {
			return nullptr;
		}

		Py_INCREF(func);
		vp->update_func = [func] (std::shared_ptr<ViewPort> _self) {
			PyObject* arg_tup = Py_BuildValue("(N)", ViewPort_from(_self));
			if (PyEval_CallObject(func, arg_tup) == nullptr) {
				PyErr_Print();
			}
			Py_DECREF(arg_tup);
		};

		Py_RETURN_NONE;
	}

	PyObject* ViewPort_update(ViewPortObject* self, PyObject* args) {
		auto vp = as_viewport(self).lock();
		if (vp == nullptr) {
			return nullptr;
		}

		vp->update(vp);

		Py_RETURN_NONE;
	}
	PyObject* ViewPort_draw(ViewPortObject* self, PyObject* args) {
		auto vp = as_viewport(self).lock();
		if (vp == nullptr) {
			return nullptr;
		}

		vp->draw(vp);

		Py_RETURN_NONE;
	}
}}}
