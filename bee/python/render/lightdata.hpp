/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_PYTHON_RENDER_LIGHTDATA_H
#define BEE_PYTHON_RENDER_LIGHTDATA_H 1

#include <Python.h>

#include "../../resource/light.hpp"

namespace bee { namespace python {
	PyObject* LightData_from(std::shared_ptr<LightData>);
	bool LightData_check(PyObject*);
namespace internal {
	typedef struct {
		PyObject_HEAD
		std::shared_ptr<LightData> ld;
	} LightDataObject;

	extern PyTypeObject LightDataType;

	PyObject* PyInit_bee_render_lightdata(PyObject*);

	std::shared_ptr<LightData> as_lightdata(LightDataObject*);
	std::shared_ptr<LightData> as_lightdata(PyObject*);

	void LightData_dealloc(PyObject*);
	PyObject* LightData_new(PyTypeObject*, PyObject*, PyObject*);
	int LightData_init(LightDataObject*, PyObject*, PyObject*);

	// LightData methods
	PyObject* LightData_repr(LightDataObject*);
	PyObject* LightData_str(LightDataObject*);

	PyObject* LightData_get_type(LightDataObject*, PyObject*);
	PyObject* LightData_get_position(LightDataObject*, PyObject*);
	PyObject* LightData_get_direction(LightDataObject*, PyObject*);
	PyObject* LightData_get_attenuation(LightDataObject*, PyObject*);
	PyObject* LightData_get_color(LightDataObject*, PyObject*);

	PyObject* LightData_set_position(LightDataObject*, PyObject*);
	PyObject* LightData_set_direction(LightDataObject*, PyObject*);
	PyObject* LightData_set_attenuation(LightDataObject*, PyObject*);
	PyObject* LightData_set_color(LightDataObject*, PyObject*);

	PyObject* LightData_draw(LightDataObject*, PyObject*);
}}}

#endif // BEE_PYTHON_RENDER_LIGHTDATA_H
