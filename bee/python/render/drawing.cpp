/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include "drawing.hpp"

#include "../../render/drawing.hpp"

namespace bee { namespace python { namespace internal {
	PyObject* render_draw_triangle(PyObject* self, PyObject* args) {
		double v1[3], v2[3], v3[3];
		RGBA color;
		int is_filled;

		if (!PyArg_ParseTuple(args, "(ddd)(ddd)(ddd)(BBBB)p", &v1[0], &v1[1], &v1[2], &v2[0], &v2[1], &v2[2], &v3[0], &v3[1], &v3[2], &color.r, &color.g, &color.b, &color.a, &is_filled)) {
			return nullptr;
		}

		glm::vec3 _v1 (v1[0], v1[1], v1[2]);
		glm::vec3 _v2 (v2[0], v2[1], v2[2]);
		glm::vec3 _v3 (v3[0], v3[1], v3[2]);

		bool _is_filled = is_filled;

		render::draw_triangle(_v1, _v2, _v3, color, _is_filled);

		Py_RETURN_NONE;
	}
	PyObject* render_draw_line(PyObject* self, PyObject* args) {
		double v1[3], v2[3];
		RGBA color;

		if (!PyArg_ParseTuple(args, "(ddd)(ddd)(BBBB)", &v1[0], &v1[1], &v1[2], &v2[0], &v2[1], &v2[2], &color.r, &color.g, &color.b, &color.a)) {
			return nullptr;
		}

		glm::vec3 _v1 (v1[0], v1[1], v1[2]);
		glm::vec3 _v2 (v2[0], v2[1], v2[2]);

		render::draw_line(_v1, _v2, color);

		Py_RETURN_NONE;
	}
	PyObject* render_draw_quad(PyObject* self, PyObject* args) {
		double pos[3], dim[3];
		int border_width;
		RGBA color;

		if (!PyArg_ParseTuple(args, "(ddd)(ddd)i(BBBB)", &pos[0], &pos[1], &pos[2], &dim[0], &dim[1], &dim[2], &border_width, &color.r, &color.g, &color.b, &color.a)) {
			return nullptr;
		}

		glm::vec3 _pos (pos[0], pos[1], pos[2]);
		glm::vec3 _dim (dim[0], dim[1], dim[2]);

		render::draw_quad(_pos, _dim, border_width, color);

		Py_RETURN_NONE;
	}
	PyObject* render_draw_polygon(PyObject* self, PyObject* args) {
		double center[3];
		double radius;
		double ang_start, ang_span;
		unsigned int segment_amount;
		int border_width;
		RGBA color;

		if (!PyArg_ParseTuple(args, "(ddd)dddIi(BBBB)", &center[0], &center[1], &center[2], &radius, &ang_start, &ang_span, &segment_amount, &border_width, &color.r, &color.g, &color.b, &color.a)) {
			return nullptr;
		}

		glm::vec3 _center (center[0], center[1], center[2]);

		render::draw_polygon(_center, radius, ang_start, ang_span, segment_amount, border_width, color);

		Py_RETURN_NONE;
	}
	PyObject* render_draw_arc(PyObject* self, PyObject* args) {
		double center[3];
		double radius;
		double ang_start, ang_span;
		int border_width;
		RGBA color;

		if (!PyArg_ParseTuple(args, "(ddd)dddi(BBBB)", &center[0], &center[1], &center[2], &radius, &ang_start, &ang_span, &border_width, &color.r, &color.g, &color.b, &color.a)) {
			return nullptr;
		}

		glm::vec3 _center (center[0], center[1], center[2]);

		render::draw_arc(_center, radius, ang_start, ang_span, border_width, color);

		Py_RETURN_NONE;
	}
	PyObject* render_draw_circle(PyObject* self, PyObject* args) {
		double center[3];
		double radius;
		int border_width;
		RGBA color;

		if (!PyArg_ParseTuple(args, "(ddd)di(BBBB)", &center[0], &center[1], &center[2], &radius, &border_width, &color.r, &color.g, &color.b, &color.a)) {
			return nullptr;
		}

		glm::vec3 _center (center[0], center[1], center[2]);

		render::draw_circle(_center, radius, border_width, color);

		Py_RETURN_NONE;
	}

	PyObject* render_draw_set_color(PyObject* self, PyObject* args) {
		RGBA color;

		if (!PyArg_ParseTuple(args, "(BBBB)", &color.r, &color.g, &color.b, &color.a)) {
			return nullptr;
		}

		render::draw_set_color(color);

		Py_RETURN_NONE;
	}
	PyObject* render_draw_get_color(PyObject* self, PyObject* args) {
		RGBA color (render::draw_get_color());
		return Py_BuildValue("(bbbb)", color.r, color.g, color.b, color.a);
	}
	PyObject* render_draw_set_blend(PyObject* self, PyObject* args) {
		int sfactor, dfactor;

		if (!PyArg_ParseTuple(args, "ii", &sfactor, &dfactor)) {
			return nullptr;
		}

		render::draw_set_blend(sfactor, dfactor);

		Py_RETURN_NONE;
	}
	PyObject* render_draw_get_blend(PyObject* self, PyObject* args) {
		auto factors = render::draw_get_blend();

		return Py_BuildValue("(ii)", factors.first, factors.second);
	}

	PyObject* render_get_pixel_color(PyObject* self, PyObject* args) {
		int x, y;

		if (!PyArg_ParseTuple(args, "ii", &x, &y)) {
			return nullptr;
		}

		RGBA color (render::get_pixel_color(x, y));

		return Py_BuildValue("(bbbb)", color.r, color.g, color.b, color.a);
	}
	PyObject* render_save_screenshot(PyObject* self, PyObject* args) {
		PyObject* filename;

		if (!PyArg_ParseTuple(args, "U", &filename)) {
			return nullptr;
		}

		std::string _filename (PyUnicode_AsUTF8(filename));

		return PyLong_FromLong(render::save_screenshot(_filename));
	}
}}}
