/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_PYTHON_RENDER_DRAWING_H
#define BEE_PYTHON_RENDER_DRAWING_H 1

#include <Python.h>

namespace bee { namespace python { namespace internal {
	PyObject* render_draw_triangle(PyObject*, PyObject*);
	PyObject* render_draw_line(PyObject*, PyObject*);
	PyObject* render_draw_quad(PyObject*, PyObject*);
	PyObject* render_draw_polygon(PyObject*, PyObject*);
	PyObject* render_draw_arc(PyObject*, PyObject*);
	PyObject* render_draw_circle(PyObject*, PyObject*);

	PyObject* render_draw_set_color(PyObject*, PyObject*);
	PyObject* render_draw_get_color(PyObject*, PyObject*);
	PyObject* render_draw_set_blend(PyObject*, PyObject*);
	PyObject* render_draw_get_blend(PyObject*, PyObject*);

	PyObject* render_get_pixel_color(PyObject*, PyObject*);
	PyObject* render_save_screenshot(PyObject*, PyObject*);
}}}

#endif // BEE_PYTHON_RENDER_DRAWING_H
