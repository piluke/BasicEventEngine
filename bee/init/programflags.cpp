/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include <iostream>

#include "programflags.hpp"

#include "info.hpp"
#include "gameoptions.hpp"

#include "../util/string.hpp"
#include "../util/platform.hpp"

#include "../messenger/messenger.hpp"

#include "../core/enginestate.hpp"
#include "../core/window.hpp"

namespace bee {
	namespace internal {
		std::list<ProgramFlag*> flags;
	}

	ProgramFlag::ProgramFlag() :
		longopt(),
		shortopt('\0'),
		pre_init(true),
		arg_type(E_FLAGARG::NONE),
		description(),
		func(nullptr)
	{}
	ProgramFlag::ProgramFlag(const std::string& l, char s, bool p, E_FLAGARG a, const std::string& d, const std::function<void (const std::string&)>& f) :
		longopt(l),
		shortopt(s),
		pre_init(p),
		arg_type(a),
		description(d),
		func(f)
	{}

	/**
	* @param longopt the longopt to search for
	* @returns the flag from the list which matches the given longopt
	*/
	ProgramFlag* internal::get_long_flag(const std::string& longopt) {
		for (auto& flag : flags) {
			std::string flo (flag->longopt);
			flo += " ";
			size_t space = flo.find_first_of(' ');

			if (flo.substr(0, space) == longopt) {
				return flag;
			}
		}
		return nullptr;
	}
	/**
	* @param shortopt the shortopt to search for
	* @returns the flag from the list which matches the given shortopt
	*/
	ProgramFlag* internal::get_short_flag(char shortopt) {
		for (auto& flag : flags) {
			if (flag->shortopt == shortopt) {
				return flag;
			}
		}
		return nullptr;
	}

	/**
	* Add a program flag for post-init parsing
	* @param flag the flag to add
	*/
	void add_flag(ProgramFlag* flag) {
		internal::flags.push_back(flag);
	}
	/**
	* Handle each flag's callback
	* @param pre_init whether this function is being called before or after engine initialization
	*
	* @returns the number of flags successfully processed, the value will be negated if a flag throws an exception
	*/
	int handle_flags(bool pre_init) {
		int amount = 0;
		const size_t arg_amount = engine->argc;
		try {
			for (size_t i=1; i<arg_amount; ++i) {
				const std::string arg (engine->argv[i]);
				if (arg[0] == '-') { // If the argument is a flag
					ProgramFlag* flag = nullptr;
					if (arg[1] == '-') { // If the argument is a long flag
						flag = internal::get_long_flag(arg.substr(2));
					} else if (arg[2] == '\0') { // If the argument is a short flag
						flag = internal::get_short_flag(arg[1]);
					}

					if (flag != nullptr) {
						std::string optarg;
						if (flag->arg_type != E_FLAGARG::NONE) {
							if ((i+1 < arg_amount)&&(engine->argv[i+1][0] != '-')) {
								++i;
								optarg = std::string(engine->argv[i]);
							}
						}

						if (flag->pre_init == pre_init) {
							if (flag->func != nullptr) {
								if (flag->arg_type != E_FLAGARG::NONE) {
									if ((flag->arg_type == E_FLAGARG::REQUIRED)&&(optarg.empty())) {
										throw std::string("Missing required argument for " + arg);
									}

									flag->func(optarg);
								} else {
									flag->func(std::string());
								}
							}
							amount++;
						}
					} else if (!pre_init) {
						messenger::send({"engine", "programflags"}, E_MESSAGE::WARNING, "Unknown flag: \"" + arg + "\"");
					}
				}
			}
		} catch (const std::string&) {
			return -(amount+1);
		}

		return amount;
	}
	const std::list<ProgramFlag*>& get_flags() {
		return internal::flags;
	}

	/**
	* Initializes the default ProgramFlags.
	* @note To add more flags, use add_flag().
	*/
	void init_standard_flags() {
		if (internal::flags.empty()) {
			add_flag(new ProgramFlag(
				"help", 'h', true, E_FLAGARG::NONE, "Output this help text",
				[] (const std::string& arg) {
					std::cerr << get_usage_text();
					throw std::string("help quit");
				}
			));
			add_flag(new ProgramFlag(
				"debug", 'd', true, E_FLAGARG::NONE, "Enable debug mode for wireframes",
				[] (const std::string& arg) {
					set_option("is_debug_enabled", true);
				}
			));
			add_flag(new ProgramFlag(
				"verbose", 'v', true, E_FLAGARG::NONE, "Enable verbose mode for extra output",
				[] (const std::string& arg) {
					messenger::set_level(E_OUTPUT::VERBOSE);
				}
			));
			add_flag(new ProgramFlag(
				"dimensions wxh", '\0', true, E_FLAGARG::REQUIRED, "Set the dimensions of the window to width w and height h",
				[] (const std::string& arg) {
					int w = std::stoi(arg.substr(0, arg.find("x")));
					int h = std::stoi(arg.substr(arg.find("x")+1));
					set_window_size(w, h);
				}
			));
			add_flag(new ProgramFlag(
				"fullscreen", 'f', true, E_FLAGARG::NONE, "Enable fullscreen mode, this will resize the window to fit the screen",
				[] (const std::string& arg) {
					set_option("is_fullscreen", true);
				}
			));
			add_flag(new ProgramFlag(
				"no-assert", '\0', true, E_FLAGARG::NONE, "Disable the test case assertions",
				[] (const std::string& arg) {
					set_option("should_assert", false);
				}
			));
			add_flag(new ProgramFlag(
				"extensive-assert", '\0', true, E_FLAGARG::NONE, "Enable extensive test cases that may modify the program state or require user input",
				[] (const std::string& arg) {
					set_option("extensive_assert", true);
				}
			));
			add_flag(new ProgramFlag(
				"single-run", '\0', true, E_FLAGARG::OPTIONAL, "Run the main loop a single time to verify assertions and initialization",
				[] (const std::string& arg) {
					set_option("single_run", true);
					if (!arg.empty()) {
						set_option("single_run_path", arg);
					}
				}
			));
			add_flag(new ProgramFlag(
				"windowed", 'w', true, E_FLAGARG::NONE, "Disable fullscreen mode",
				[] (const std::string& arg) {
					set_option("is_fullscreen", false);
				}
			));
			add_flag(new ProgramFlag(
				"headless", '\0', true, E_FLAGARG::NONE, "Run the engine in headless mode without any SDL or OpenGL initialization",
				[] (const std::string& arg) {
					set_option("is_headless", true);
				}
			));
		}
	}
	/**
	* Free the list of the default ProgramFlags.
	*/
	int free_standard_flags() {
		if (!internal::flags.empty()) {
			for (auto& flag : internal::flags) {
				delete flag;
			}
			internal::flags.clear();
			return 0;
		}
		return 1;
	}
}
