/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include "info.hpp"

#include "../defines.hpp"

#include "../data/variant.hpp"

#include "../util/platform.hpp"
#include "../util/files.hpp"

#include "programflags.hpp"

#define BEE_VERSION_MAJOR 0
#define BEE_VERSION_MINOR 4
#define BEE_VERSION_PATCH 1

namespace bee {
	namespace internal {
		bool is_initialized = false;

		unsigned int build_id = 0;
		unsigned int game_id = 0;

		std::string game_name {"BasicEventEngine"};
		VersionInfo engine_version {BEE_VERSION_MAJOR, BEE_VERSION_MINOR, BEE_VERSION_PATCH};
		VersionInfo game_version {engine_version};

		int init() {
			build_id = util::directory_get_checksum("main/resources", {"objects", "rooms", "scripts", "timelines"});
			game_id = build_id >> 16;

			Variant defs (util::file_get_contents("config.json"), true);
			game_name = defs.m["game_name"].s;
			game_version.major = defs.m["game_version"].v[0].i;
			game_version.minor = defs.m["game_version"].v[1].i;
			game_version.patch = defs.m["game_version"].v[2].i;

			is_initialized = true;

			return 0;
		}
	}

	/**
	* @returns the version as a string in the format "Major.Minor.Patch"
	*/
	std::string VersionInfo::to_str() const {
		return std::to_string(major) + "." + std::to_string(minor) + "." + std::to_string(patch);
	}

	/**
	* @returns information about how to run the program
	*/
	std::string get_usage_text() {
		std::string args;
		for (auto& f : get_flags()) {
			args += "    --" + f->longopt;
			if (f->shortopt != '\0') {
				args += ", -" + std::string(1, f->shortopt);
			}
			args += "\n";
			if (!f->description.empty()) {
				args += "        " + f->description + "\n";
			}
		}

		return
			util::platform::get_path() + "\n"
			"A basic event-driven OpenGL game engine\n"
			"\n"
			"Usage:\n"
			"    " + util::file_basename(util::platform::get_path()) + " [OPTION]...\n"
			"Options:\n" + args +
			"    Other options may be added during initialization.\n"
			"Exit Status:\n"
			"    0    Success\n"
			"    1    Failure to initialize the engine\n"
			"    2    Unknown exception during game loop\n"
		;
	}
	/**
	* @returns the engine version in a struct
	*/
	VersionInfo get_engine_version() {
		return internal::engine_version;
	}

	/**
	* @returns the full build ID, a checksum of the resource files
	*/
	unsigned int get_build_id() {
		if (!internal::is_initialized) {
			internal::init();
		}
		return internal::build_id;
	}
	/**
	* @returns the game ID, which is a truncated version of the build ID
	*/
	unsigned int get_game_id() {
		if (!internal::is_initialized) {
			internal::init();
		}
		return internal::game_id;
	}

	/**
	* @returns the game name as a string
	*/
	std::string get_game_name() {
		if (!internal::is_initialized) {
			internal::init();
		}
		return internal::game_name;
	}
	/**
	* @returns the game version in a struct
	*/
	VersionInfo get_game_version() {
		if (!internal::is_initialized) {
			internal::init();
		}
		return internal::game_version;
	}
}
