/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include "filepath.hpp"

#include "../util/files.hpp"

namespace bee {
	FilePath::FilePath(const std::string& _path, const std::string& _mapname) :
		path(_path),
		mapname(_mapname)
	{}
	FilePath::FilePath(const std::string& _path) :
		FilePath(_path, "")
	{}
	FilePath::FilePath() :
		FilePath("", "")
	{}

	std::string FilePath::get_path() const {
		return path;
	}
	std::string FilePath::get_mapname() const {
		return mapname;
	}
	FilePath FilePath::get_parent_dir() const {
		return FilePath(util::file_dirname(get_path()), get_mapname());
	}

	bool FilePath::exists() const {
		return util::file_exists(path);
	}
	size_t FilePath::size() const {
		return util::file_size(path);
	}
	std::string FilePath::get() const {
		return util::file_get_contents(path);
	}
	/**
	* Get the SDL_RWops* version of the file string.
	* @note The caller is responsible for deleting the file string when done.
	*
	* @returns a pair of the SDL_RWops* and the file string that it references
	*/
	std::pair<SDL_RWops*,std::string*> FilePath::get_rwops() const {
		std::string* contents = new std::string(get());
		return std::make_pair(
			SDL_RWFromConstMem(contents->c_str(), static_cast<int>(contents->size())),
			contents
		);
	}

	std::streamoff FilePath::put(const std::string& contents) {
		return util::file_put_contents(path, contents);
	}
}
