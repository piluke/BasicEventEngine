/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include "python.hpp"

#include "../util/string.hpp"

#include "../messenger/messenger.hpp"

#include "fs.hpp"

namespace bee { namespace fs { namespace python {
	namespace internal {
		std::map<std::string,PyObject*> modules;

		PyMethodDef fp_get_ml {
			"FilePath_get",
			[] (PyObject* self, PyObject* args) -> PyObject* {
				PyObject* path = PyDict_GetItemString(self, "path");
				std::string _path (PyUnicode_AsUTF8(path));
				PyObject* mapname = PyDict_GetItemString(self, "mapname");
				std::string _mapname (PyUnicode_AsUTF8(mapname));

				std::string data (FilePath(_path, _mapname).get());

				PyObject* r = PyUnicode_FromStringAndSize(data.c_str(), data.size());
				if (r == nullptr) {
					PyErr_Clear();
					r = PyBytes_FromStringAndSize(data.c_str(), data.size());
				}
				return r;
			},
			METH_NOARGS,
			nullptr
		};
	}

	PyObject* internal::get_file_callable(PyObject* file) {
		return PyCFunction_New(&fp_get_ml, file);
	}

	/**
	* Import the file with the given path as a Python module.
	* @note The caller is responsible for calling Py_XDECREF() when done with the module.
	* @param fname the file path
	* @param mapname the map to find the file in
	*
	* @returns the PyObject* representing the module or nullptr on failure
	*/
	PyObject* import(const std::string& fname, const std::string& mapname) {
		if (!fs::exists(fname, mapname)) {
			PyErr_SetString(PyExc_FileNotFoundError, (std::string("no file named \"") + fname + "\" in map \"" + mapname + "\"").c_str());
			return nullptr;
		}

		std::string modname = fname;
		modname = util::string::replace(modname, ".py", "");
		modname = util::string::replace(modname, "/", ".");

		auto m = internal::modules.find(modname);
		if (m != internal::modules.end()) {
			Py_INCREF(m->second);
			return m->second;
		}

		PyObject* code = Py_CompileString(fs::get_file(fname, mapname).get().c_str(), fname.c_str(), Py_file_input);
		if (code == nullptr) {
			return nullptr;
		}
		PyObject* module = PyImport_ExecCodeModule(modname.c_str(), code);
		if (module == nullptr) {
			return nullptr;
		}

		internal::modules.emplace(modname, module);

		return module;
	}
}}}
