/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include <unordered_map>

#include "fs.hpp"

#include "../util/files.hpp"
#include "../util/string.hpp"
#include "../util/archive.hpp"

#include "../messenger/messenger.hpp"

#include "../core/console.hpp"
#include "../core/instance.hpp"
#include "../core/rooms.hpp"
#include "assimp.hpp"

#include "../input/kb.hpp"

#include "../physics/world.hpp"

#include "../render/background.hpp"

#include "../resource/texture.hpp"
#include "../resource/sound.hpp"
#include "../resource/font.hpp"
#include "../resource/path.hpp"
#include "../resource/timeline.hpp"
#include "../resource/mesh.hpp"
#include "../resource/light.hpp"
#include "../resource/script.hpp"
#include "../resource/object.hpp"
#include "../resource/room.hpp"

#include "../resources/objects/obj_script.hpp"
#include "../resources/rooms/rm_script.hpp"

namespace bee { namespace fs {
	namespace internal {
		std::unordered_map<std::string,FileMap> filemaps;
		std::unordered_map<std::string,std::vector<FilePath>> files;
	}

	/**
	* Scan the given path for files to map.
	* @param path the real path to map
	* @param root the root relation of the FileMap
	* @param mapname the name of the current FileMap being scanned
	*
	* @returns the number of mapped files
	*/
	int internal::scan_files(const std::string& path, FilePath root, const std::string& mapname) {
		int amount = 0;
		if ((path != ".")&&(util::directory_exists(path))) {
			// Add subdirectories
			std::vector<std::string> dir_files = util::directory_get_files(path);
			for (auto& f : dir_files) {
				amount += scan_files(path+"/"+f, root, mapname);
			}
		} else if (util::file_exists(path)) {
			// Add files
			if ((util::file_extname(path) == ".xz")&&(util::file_extname(util::file_plainname(path)) == ".tar")) {
				amount += scan_archive(path, root, mapname);
			} else {
				FilePath fp (path, mapname);
				if ((!root.get_path().empty())&&(root.get_path() != ".")) {
					std::string new_path (util::string::replace(fp.get_path(), root.get_path(), ""));
					if (new_path.front() == '/') {
						new_path = new_path.substr(1);
					}
					fp = FilePath(new_path, mapname);
				}

				std::unordered_map<std::string,std::vector<FilePath>>::iterator filevec;
				std::tie(filevec, std::ignore) = files.emplace(fp.get_path(), std::vector<FilePath>());
				filevec->second.push_back(FilePath(path, mapname));
				++amount;
			}
		}

		return amount;
	}
	/**
	* Scan the given archive for files to map.
	* @param path the archive path
	* @param root the root relation of the FileMap
	* @param mapname the name of the current FileMap being scanned
	*
	* @returns the number of mapped files
	*/
	int internal::scan_archive(const std::string& path, FilePath root, const std::string& mapname) {
		std::string tarfile = util::archive::xz_decompress_temp(path);
		if (tarfile.empty()) {
			messenger::send({"engine", "fs"}, E_MESSAGE::WARNING, "Failed to decompress archive \"" + path + "\" for scanning");
			return 0;
		}
		std::string dir = util::archive::tar_extract_temp(tarfile);
		if (dir.empty()) {
			messenger::send({"engine", "fs"}, E_MESSAGE::WARNING, "Failed to extract archive \"" + tarfile + "\" for scanning");
			return 0;
		}

		root = FilePath(dir, root.get_mapname());

		return scan_files(dir, root, mapname);
	}

	/**
	* Initialize the filesystem with default mappings.
	*/
	int init() {
		fs::add_filemap("__default_bee_resources", "bee/resources", E_FS_ROOT_TYPE::IS_ROOT);

		assimp::init();

		return 0;
	}

	/**
	* Add a FileMap with the given name.
	* @param name the name to use for the FileMap
	* @param path the FileMap path
	* @param root_type the root relation of the FileMap
	*
	* @retval 0 success
	* @retval 1 success but a FileMap with the same name was overwritten
	*/
	int add_filemap(const std::string& name, const std::string& path, E_FS_ROOT_TYPE root_type) {
		FilePath root (".");
		if (root_type == E_FS_ROOT_TYPE::HAS_ROOTS) {
			root = FilePath(path+"/");
		} else if (root_type == E_FS_ROOT_TYPE::IS_ROOT) {
			root = FilePath(path).get_parent_dir();
		}

		auto fm = internal::filemaps.find(name);
		if (fm != internal::filemaps.end()) {
			fm->second.set_path(path);
			internal::scan_files(fm->second.get_path(), root, name);
			return 1;
		}

		std::tie(fm, std::ignore) = internal::filemaps.emplace(name, FileMap(name, path));
		internal::scan_files(fm->second.get_path(), root, name);

		return 0;
	}
	/**
	* Add a FileMap with the given name.
	* @param name the name to use for the FileMap
	* @param path the FileMap path
	*
	* @retval 0 success
	* @retval 1 success but a FileMap with the same name was overwritten
	*/
	int add_filemap(const std::string& name, const std::string& path) {
		return add_filemap(name, path, E_FS_ROOT_TYPE::NOT_ROOT);
	}
	/**
	* Remove all files mapped from the FileMap with the given name.
	* @param name the name of the FileMap to unmap
	*
	* @retval 0 success
	*/
	void remove_filemaps(const std::string& name) {
		for (auto fpv = internal::files.begin(); fpv != internal::files.end(); ) {
			fpv->second.erase(std::remove_if(fpv->second.begin(), fpv->second.end(), [&name] (const FilePath& fp) -> bool {
				const std::string mn (fp.get_mapname());
				return (mn == name) || (mn.rfind(name+"/", 0) == 0);
			}), fpv->second.end());

			if (fpv->second.empty()) {
				fpv = internal::files.erase(fpv);
			} else {
				++fpv;
			}
		}

		for (auto fm = internal::filemaps.begin(); fm != internal::filemaps.end(); ) {
			const std::string mn (fm->second.get_name());
			if ((mn == name)||(mn.rfind(name+"/", 0) == 0)) {
				fm = internal::filemaps.erase(fm);
			} else {
				++fm;
			}
		}
	}
	/**
	* Remove all mapped files.
	*
	* @retval 0 success
	* @retval >0 failed to unmap some FileMaps
	*/
	void remove_all_filemaps() {
		auto fm = internal::filemaps.begin();
		while (fm != internal::filemaps.end()) {
			remove_filemaps(fm->second.get_name());
			fm = internal::filemaps.begin();
		}
	}

	/**
	* @param path the path to check
	* @param mapname the name of the specific FileMap that mapped the path
	*
	* @returns whether a file has been mapped to the given path
	*/
	bool exists(const std::string& path, const std::string& mapname) {
		auto fp = internal::files.find(FilePath(path).get_path());
		if (fp == internal::files.end()) {
			return false;
		}

		if (mapname.empty()) {
			return fp->second.back().exists();
		}

		for (auto& f : fp->second) {
			if (f.get_mapname() == mapname) {
				return f.exists();
			}
		}

		return false;
	}
	/**
	* @param path the path to check
	*
	* @returns whether a file has been mapped to the given path
	*/
	bool exists(const std::string& path) {
		return exists(path, "");
	}
	/**
	* @param path the path to get
	* @param mapname the FileMap name to fetch from
	*
	* @returns the primary FilePath of the mapped file
	*/
	FilePath get_file(const std::string& path, const std::string& mapname) {
		auto fp = internal::files.find(FilePath(path).get_path());
		if (fp == internal::files.end()) {
			messenger::send({"engine", "fs"}, E_MESSAGE::WARNING, "FilePath \"" + path + "\" not mapped");
			return FilePath();
		}

		if (mapname.empty()) {
			return fp->second.back();
		}

		for (auto& f : fp->second) {
			if (f.get_mapname() == mapname) {
				return f;
			}
		}

		return FilePath();
	}
	FilePath get_file(const std::string& path) {
		return get_file(path, "");
	}

	int load_instance_map(const std::vector<Variant>& instmap) {
		for (auto& inst : instmap) {
			btVector3 pos (
				static_cast<btScalar>(inst.v[1].v[0].f),
				static_cast<btScalar>(inst.v[1].v[1].f),
				static_cast<btScalar>(inst.v[1].v[2].f)
			);
			std::vector<Object*> comps;
			for (auto& c : inst.v[0].v) {
				comps.push_back(Object::get_by_name(c.s));
			}

			auto instprop = inst.v[2].m;
			std::map<std::string,Variant> instdata;
			auto it = instprop.find("data");
			if (it != instprop.end()) {
				for (auto& d : it->second.m) {
					instdata.emplace(d.first.s, d.second);
				}
			}

			Instance* _inst = get_current_room()->add_instance(comps, pos, instdata);

			if (!instprop.empty()) {
				it = instprop.find("name");
				if (it != instprop.end()) {
					_inst->set_name(it->second.s);
				}
				it = instprop.find("sprite");
				if (it != instprop.end()) {
					_inst->set_sprite(Texture::get_by_name(it->second.s));
				}
				it = instprop.find("computation_type");
				if (it != instprop.end()) {
					_inst->set_computation_type(static_cast<E_COMPUTATION>(it->second.i));
				}
				it = instprop.find("is_persistent");
				if (it != instprop.end()) {
					_inst->set_is_persistent(it->second.i);
				}
				it = instprop.find("mass");
				if (it != instprop.end()) {
					_inst->set_mass(it->second.f);
				}
				it = instprop.find("friction");
				if (it != instprop.end()) {
					_inst->set_friction(it->second.f);
				}
				it = instprop.find("gravity");
				if (it != instprop.end()) {
					btVector3 g (
						static_cast<btScalar>(it->second.v[0].f),
						static_cast<btScalar>(it->second.v[1].f),
						static_cast<btScalar>(it->second.v[2].f)
					);
					_inst->set_gravity(g);
				}
			}
		}
		return 0;
	}
	int load_instance_map(const std::string& instpath) {
		std::string _instpath (instpath);
		if ((!instpath.empty())&&(instpath.front() == '$')) {
			_instpath = "resources/rooms"+instpath.substr(1);
		}
		Variant instmap (fs::get_file(_instpath).get(), true);
		return load_instance_map(instmap.v);
	}
	/**
	* Load the given level and Room.
	* @param name the name of the level
	* @param path the path to the levelmap
	* @param room the name of the first Room to change to after loading
	* @param are_scripts_enabled whether to allow the level to load Scripts
	*
	* @retval 0 success
	* @retval 1 failed to load some Resources
	* @retval 2 failed since the first Room couldn't be changed to
	*/
	int load_level(const std::string& name, const std::string& path, const std::string& room, bool are_scripts_enabled) {
		std::string _room (room);

		add_filemap(name, path, E_FS_ROOT_TYPE::HAS_ROOTS);

		if (exists("resources.json", name)) {
			FilePath res = get_file("resources.json", name);
			int amount_failed = 0;
			Variant resources (res.get(), true);
			std::map<Variant,Variant> resmap = resources.m["resources"].m;

			bool prev_new_enabled = Script::set_is_new_enabled(are_scripts_enabled);

			try {
				std::map<Variant,Variant>::iterator it;
				for (auto& t : resmap["textures"].v) {
					Texture* _t = new Texture(t.m["name"].s, t.m["path"].s);

					it = t.m.find("speed");
					if (it != t.m.end()) {
						_t->set_speed(it->second.f);
					}
					it = t.m.find("origin");
					if (it != t.m.end()) {
						if (it->second.get_type() == E_DATA_TYPE::VECTOR) {
							_t->set_origin(it->second.v[0].i, it->second.v[1].i);
						} else if (it->second.get_type() == E_DATA_TYPE::STRING) {
							if (it->second.s == "center") {
								_t->set_origin_center();
							}
						}
					}
					it = t.m.find("rotate");
					if (it != t.m.end()) {
						_t->set_rotate(it->second.v[0].f, it->second.v[1].f);
					}
					it = t.m.find("subimage_amount");
					if (it != t.m.end()) {
						_t->set_subimage_amount(it->second.v[0].i, it->second.v[1].i);
					}
					it = t.m.find("crop");
					if (it != t.m.end()) {
						if ((it->second.v[2].i > 0)&&(it->second.v[3].i > 0)) {
							SDL_Rect r {
								static_cast<int>(it->second.v[0].i),
								static_cast<int>(it->second.v[1].i),
								static_cast<int>(it->second.v[2].i),
								static_cast<int>(it->second.v[3].i)
							};
							_t->crop_image(r);
						}
					}

					amount_failed += (_t->load() > 0) ? 1 : 0;
				}
				for (auto& s : resmap["sounds"].v) {
					Sound* _s = new Sound(s.m["name"].s, s.m["path"].s, s.m["is_music"].i);

					it = s.m.find("volume");
					if (it != s.m.end()) {
						_s->set_volume(it->second.f);
					}
					it = s.m.find("pan");
					if (it != s.m.end()) {
						_s->set_pan(it->second.f);
					}
					it = s.m.find("is_music");
					if (it != s.m.end()) {
						_s->set_is_music(it->second.i);
					}
					it = s.m.find("effects");
					if (it != s.m.end()) {
						for (auto& e : it->second.m) {
							SoundEffect se (
								e.first.s,
								static_cast<E_SOUNDEFFECT>(e.second.m["type"].i),
								e.second.m["params"].m
							);
							_s->effect_add(se);
						}
					}

					amount_failed += (_s->load() > 0) ? 1 : 0;
				}
				for (auto& f : resmap["fonts"].v) {
					Font* _f = new Font(f.m["name"].s, f.m["path"].s, f.m["font_size"].i);

					it = f.m.find("font_size");
					if (it != f.m.end()) {
						_f->set_font_size(it->second.i);
					}
					it = f.m.find("style");
					if (it != f.m.end()) {
						_f->set_style(static_cast<E_FONT_STYLE>(it->second.i));
					}
					it = f.m.find("lineskip");
					if (it != f.m.end()) {
						_f->set_lineskip(it->second.i);
					}

					amount_failed += (_f->load() > 0) ? 1 : 0;
				}
				for (auto& p : resmap["paths"].v) {
					Path* _p = new Path(p.m["name"].s, p.m["path"].s);
					amount_failed += (_p->load() > 0) ? 1 : 0;
				}
				for (auto& t : resmap["timelines"].v) {
					Timeline* _t = new Timeline(t.m["name"].s, t.m["path"].s);
					amount_failed += (_t->load() > 0) ? 1 : 0;
				}
				for (auto& m : resmap["meshes"].v) {
					Mesh* _m = new Mesh(m.m["name"].s, m.m["path"].s);

					it = m.m.find("mesh_index");
					if (it != m.m.end()) {
						amount_failed += (_m->load(it->second.i) > 0) ? 1 : 0;
					} else {
						amount_failed += (_m->load() > 0) ? 1 : 0;
					}
				}
				for (auto& l : resmap["lights"].v) {
					Light* _l = new Light(l.m["name"].s, l.m["path"].s);
					amount_failed += (_l->load() > 0) ? 1 : 0;
				}
				for (auto& s : resmap["scripts"].v) {
					Script* _s = new Script(s.m["name"].s, s.m["path"].s);
					amount_failed += (_s->load() > 0) ? 1 : 0;
				}
				for (auto& o : resmap["objects"].v) {
					Object* _o = new ObjScript(o.m["path"].s);

					it = o.m.find("sprite");
					if (it != o.m.end()) {
						_o->set_sprite(Texture::get_by_name(it->second.s));
					}
					it = o.m.find("is_persistent");
					if (it != o.m.end()) {
						_o->set_is_persistent(it->second.i);
					}
					it = o.m.find("depth");
					if (it != o.m.end()) {
						_o->set_depth(it->second.i);
					}
					it = o.m.find("parent");
					if (it != o.m.end()) {
						_o->set_parent(Object::get_by_name(it->second.s));
					}
					it = o.m.find("mask_offset");
					if (it != o.m.end()) {
						_o->set_mask_offset(std::make_pair(it->second.v[0].i, it->second.v[1].i));
					}
					it = o.m.find("is_pausable");
					if (it != o.m.end()) {
						_o->set_is_pausable(it->second.i);
					}

					amount_failed += (_o->load() > 0) ? 1 : 0;
				}
				if ((_room.empty())&&(!resources.m["first_room"].s.empty())) {
					_room = resources.m["first_room"].s;
				}
				for (auto& r : resmap["rooms"].v) {
					Room* _r = new RmScript(r.m["path"].s);

					it = r.m.find("width");
					if (it != r.m.end()) {
						_r->set_width(it->second.i);
					}
					it = r.m.find("height");
					if (it != r.m.end()) {
						_r->set_height(it->second.i);
					}
					it = r.m.find("is_persistent");
					if (it != r.m.end()) {
						_r->set_is_persistent(it->second.i);
					}

					amount_failed += (_r->load() > 0) ? 1 : 0;
				}
			} catch (int) {
				messenger::send({"engine", "fs"}, E_MESSAGE::WARNING, "Failed to init some resources for the level \"" + name + "\" at \"" + path + "\"");
				return 1;
			}

			Script::set_is_new_enabled(prev_new_enabled);

			if (amount_failed != 0) {
				messenger::send({"engine", "fs"}, E_MESSAGE::WARNING, "Failed to load some resources for the level \"" + name + "\" at \"" + path + "\"");
				return 2;
			}
		}

		if (!_room.empty()) {
			kb::unbind_all();
			int r = change_room(name, _room);
			console::internal::run("execfile(\"cfg/binds.py\")", true);
			if (r != 0) {
				return 3;
			}
		}

		return 0;
	}
	/**
	* Load the given level.
	* @param name the name of the level
	* @param path the path to the levelmap
	*
	* @retval 0 successfully loaded level
	* @retval >0 see load_level(const std::string&, const std::string&, const std::string&, bool)
	*/
	int load_level(const std::string& name, const std::string& path) {
		return load_level(name, path, "", false);
	}
	/**
	* Load the Room and its Instance map from the given level.
	* @param name the name of the level
	* @param room the name of the Room to change to
	*
	* @retval 0 success
	* @retval -1 changed to Room with empty Instance map
	* @retval 1 failed to find the Room in the level map
	* @retval 2 failed to change to the Room
	*/
	int change_room(const std::string& name, const std::string& room) {
		Room* rm = Room::get_by_name(room);
		if ((rm == nullptr)||(!rm->get_is_loaded())) {
			messenger::send({"engine", "fs"}, E_MESSAGE::WARNING, "Failed to find the Room called \"" + room + "\" for level \"" + name + "\"");
			return 1;
		}

		std::string instmap;
		if (exists("resources.json", name)) {
			FilePath res = get_file("resources.json", name);
			Variant resources (res.get(), true);
			std::map<Variant,Variant> resmap = resources.m["resources"].m;

			auto rmit = std::find_if(resmap["rooms"].v.begin(), resmap["rooms"].v.end(), [&room] (const Variant& _rm) -> bool {
				if (_rm.m.find("path") != _rm.m.end()) {
					return (util::file_plainname(util::file_basename(_rm.m.at("path").s)) == room);
				}
				return false;
			});
			if (rmit != resmap["rooms"].v.end()) {
				instmap = rmit->m["instancemap"].s;
			}
		}

		if (bee::change_room(rm, get_is_ready(), [&instmap] () {
			Room* _rm = get_current_room();
			_rm->init();

			if (!instmap.empty()) {
				load_instance_map(instmap);
			}
		}) != 0) {
			messenger::send({"engine", "fs"}, E_MESSAGE::WARNING, "Failed to change to the Room \"" + room + "\" for level \"" + name + "\"");
			return 2;
		}

		if (instmap.empty()) {
			return -1;
		}
		return 0;
	}
	/**
	* Restart the current Room and load its Instance map from the given level.
	* @param name the name of the level
	*
	* @see change_room() for return values
	*/
	int restart_room(const std::string& name) {
		return change_room(name, get_current_room()->get_name());
	}
	/**
	* Load the given level and Room after unloading the previous level.
	* @param name the name of the level
	* @param path the path to the levelmap
	* @param room the name of the first Room to change to after loading
	* @param are_scripts_enabled whether to allow the level to load Scripts
	*
	* @retval -2 failed to unload the previous level
	* @retval -1 restarted the current Room
	* @retval 0 successfully loaded level
	* @retval >0 see load_level(const std::string&, const std::string&, const std::string&, bool)
	*/
	int switch_level(const std::string& name, const std::string& path, const std::string& room, bool are_scripts_enabled) {
		if (
			((get_current_room() != nullptr)&&(get_current_room()->get_name() == room))
			||((room.empty())&&(exists("resources.json", name)))
		) {
			kb::unbind_all();
			console::internal::run("execfile(\"cfg/binds.py\")", true);

			Variant instmap;
			FilePath res = get_file("resources.json", name);
			if (res.exists()) {
				Variant resources (res.get(), true);
				std::map<Variant,Variant> resmap = resources.m["resources"].m;
				for (auto& r : resmap["rooms"].v) {
					std::string rm_name = util::file_plainname(util::file_basename(r.m.at("path").s));
					auto it = r.m.find("instancemap");
					if ((it != r.m.end())&&(rm_name == get_current_room()->get_name())) {
						instmap = it->second;
						break;
					}
				}
			}

			bee::restart_room([instmap] () {
				Room* _rm = get_current_room();
				_rm->init();

				if (instmap.get_type() == E_DATA_TYPE::STRING) {
					load_instance_map(instmap.s);
				} else if (instmap.get_type() == E_DATA_TYPE::VECTOR) {
					load_instance_map(instmap.v);
				} else if (instmap.get_type() != E_DATA_TYPE::NONE) {
					messenger::send({"engine", "fs"}, E_MESSAGE::WARNING, "Failed to failed to load instmap from " + instmap.to_str());
				}
			});

			return -1;
		}

		if (unload_level(name)) {
			return -2;
		}

		return load_level(name, path, room, are_scripts_enabled);
	}
	/**
	* Unload the level with the given name.
	* @param name the name of the level to unload
	*
	* @retval 0 success
	* @retval 1 failed to unload some Resources
	*/
	int unload_level(const std::string& name) {
		if (exists("resources.json", name)) {
			FilePath res = get_file("resources.json", name);
			Variant resources (res.get(), true);
			std::map<Variant,Variant> resmap = resources.m["resources"].m;

			try {
				for (auto& t : resmap["textures"].v) {
					Texture* _t = Texture::get_by_name(t.m["name"].s);
					delete _t;
				}
				for (auto& s : resmap["sounds"].v) {
					Sound* _s = Sound::get_by_name(s.m["name"].s);
					delete _s;
				}
				for (auto& f : resmap["fonts"].v) {
					Font* _f = Font::get_by_name(f.m["name"].s);
					delete _f;
				}
				for (auto& p : resmap["paths"].v) {
					Path* _p = Path::get_by_name(p.m["name"].s);
					delete _p;
				}
				for (auto& t : resmap["timelines"].v) {
					Timeline* _t = Timeline::get_by_name(t.m["name"].s);
					delete _t;
				}
				for (auto& m : resmap["meshes"].v) {
					Mesh* _m = Mesh::get_by_name(m.m["name"].s);
					delete _m;
				}
				for (auto& l : resmap["lights"].v) {
					Light* _l = Light::get_by_name(l.m["name"].s);
					delete _l;
				}
				for (auto& s : resmap["scripts"].v) {
					Script* _s = Script::get_by_name(s.m["name"].s);
					delete _s;
				}
				for (auto& o : resmap["objects"].v) {
					Object* _o = Object::get_by_name(
						util::file_plainname(util::file_basename(
							o.m["path"].s
						))
					);
					delete _o;
				}
				for (auto& r : resmap["rooms"].v) {
					Room* _r = Room::get_by_name(
						util::file_plainname(util::file_basename(
							r.m["path"].s
						))
					);
					delete _r;
				}
			} catch (int) {
				messenger::send({"engine", "fs"}, E_MESSAGE::WARNING, "Failed to destroy some resources for the level \"" + name + "\"");
				return 1;
			}
		}

		remove_filemaps(name);

		return 0;
	}
	/**
	* Unload all levels.
	*
	* @returns how many levels failed to unload
	*/
	int unload_all_levels() {
		int r = 0;

		const auto fms (internal::filemaps);
		for (auto fm = fms.begin(); fm != fms.end(); ++fm) {
			const std::string name (fm->second.get_name());
			if (exists("resources.json", name)) {
				r += (unload_level(name)) ? 1 : 0;
			}
		}

		return r;
	}

	/**
	* Print the mapped files and the name of their associated FileMap.
	*/
	void print_mappings() {
		std::map<Variant,Variant> info;
		for (auto& fp : internal::files) {
			info[Variant(fp.first)] = "<" + fp.second.back().get_mapname() + ">";
		}

		messenger::send({"engine", "fs"}, E_MESSAGE::INFO, Variant(info).to_str(true));
	}
}}
