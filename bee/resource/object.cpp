/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include "object.hpp" // Include the class resource header

#include "../engine.hpp"

#include "../util/string.hpp"
#include "../util/platform.hpp"
#include "../util/debug.hpp"

#include "../messenger/messenger.hpp"

#include "../core/instance.hpp"

#include "../network/network.hpp"

#include "texture.hpp"

#include "../resources/objects/obj_script.hpp"

namespace bee {
	std::map<size_t,Object*> Object::list;
	size_t Object::next_id = 0;

	/**
	* Default construct the Object.
	* @note This constructor should only be directly used for temporary Objects, the other constructor should be used for all other cases.
	*/
	Object::Object() :
		Resource(),

		id(-1),
		name(),
		path(),
		sprite(nullptr),
		is_persistent(false),
		depth(0),
		parent(nullptr),
		draw_offset({0, 0}),
		is_pausable(true),

		instances(),
		current_instance(nullptr),
		s(nullptr),

		implemented_events({
			E_EVENT::UPDATE,
			E_EVENT::DESTROY,
			E_EVENT::STEP_BEGIN,
			E_EVENT::COLLISION
		})
	{}
	/**
	* Construct the Object, add it to the Object resource list, and set the new name and path.
	* @param _name the name to use for the Object
	* @param _path the path of the Object's header file
	*
	* @throws int(-1) Failed to initialize Resource
	*/
	Object::Object(const std::string& _name, const std::string& _path) :
		Object() // Default initialize all variables
	{
		if (add_to_resources() == static_cast<size_t>(-1)) { // Attempt to add the Object to its resource list
			messenger::send({"engine", "resource"}, E_MESSAGE::WARNING, "Failed to add Object resource: \"" + _name + "\" from " + _path);
			throw -1;
		}

		set_name(_name);
		set_path(_path);
	}
	/**
	* Remove the Object from the resource list.
	*/
	Object::~Object() {
		list.erase(id);
	}

	/**
	* @returns the number of Object resources
	*/
	size_t Object::get_amount() {
		return list.size();
	}
	/**
	* @param id the resource to get
	*
	* @returns the resource with the given id or nullptr if not found
	*/
	Object* Object::get(size_t id) {
		if (list.find(id) != list.end()) {
			return list[id];
		}
		return nullptr;
	}
	/**
	* @param name the name of the desired Object
	*
	* @returns the Object resource with the given name or nullptr if not found
	*/
	Object* Object::get_by_name(const std::string& name) {
		for (auto& obj : list) { // Iterate over the Objects in order to find the first one with the given name
			Object* o = obj.second;
			if (o != nullptr) {
				if (o->get_name() == name) {
					return o; // Return the desired Object on success
				}
			}
		}
		return nullptr;
	}
	/**
	* Initiliaze and return a newly created Object resource.
	* @param name the name to initialize the Object with
	* @param path the path to initialize the Object with
	*
	* @returns the newly created Object
	*/
	Object* Object::add(const std::string& name, const std::string& path) {
		Object* new_object = new ObjScript(path);
		new_object->set_name(name);
		new_object->load();
		return new_object;
	}

	/**
	* Add the Object to the appropriate resource list.
	*
	* @returns the Object id
	*/
	size_t Object::add_to_resources() {
		if (id == static_cast<size_t>(-1)) { // If the resource needs to be added to the resource list
			id = next_id++;
			list.emplace(id, this); // Add the resource with its new id
		}

		return id;
	}
	/**
	* Reset all resource variables for reinitialization.
	*
	* @retval 0 success
	*/
	int Object::reset() {
		// Reset all properties
		name = "";
		path = "";
		sprite = nullptr;
		is_persistent = false;
		depth = 0;
		parent = nullptr;
		draw_offset = {0, 0};
		is_pausable = true;

		// Clear Instance data
		instances.clear();
		current_instance = nullptr;
		s = nullptr;

		return 0;
	}

	/**
	* @returns a map of all the information required to restore the Object
	*/
	std::map<Variant,Variant> Object::serialize() const {
		std::map<Variant,Variant> info;

		info["id"] = static_cast<int>(id);
		info["name"] = name;
		info["path"] = path;

		info["sprite"] = "";
		if (sprite != nullptr) {
			info["sprite"] = sprite->get_name();
		}
		info["is_persistent"] = is_persistent;
		info["depth"] = depth;
		info["parent"] = "";
		if (parent != nullptr) {
			info["parent"] = parent->get_name();
		}
		info["offsetx"] = draw_offset.first;
		info["offsety"] = draw_offset.second;
		info["is_pausable"] = is_pausable;

		std::vector<Variant> events;
		for (auto& e : implemented_events) {
			events.emplace_back(static_cast<int>(e));
		}
		info["implemented_events"] = events;

		return info;
	}
	/**
	* Restore the Object from serialized data.
	* @param m the map of data to use
	*
	* @retval 0 success
	* @retval 1 failed to load the Object
	*/
	int Object::deserialize(std::map<Variant,Variant>& m) {
		id = m["id"].i;
		name = m["name"].s;
		path = m["path"].s;

		sprite = Texture::get_by_name(m["sprite"].s);
		is_persistent = m["is_persistent"].i;
		depth = m["depth"].i;
		parent = Object::get_by_name(m["parent"].s);
		draw_offset = std::make_pair(m["offsetx"].i, m["offsety"].i);
		is_pausable = m["is_pausable"].i;

		for (auto& e : m["implemented_events"].v) {
			implemented_events.emplace(static_cast<E_EVENT>(e.i));
		}

		return 0;
	}
	/**
	* Print all relevant information about the resource.
	*/
	void Object::print() const {
		Variant m (serialize());
		messenger::send({"engine", "object"}, E_MESSAGE::INFO, "Object " + m.to_str(true));
	}

	size_t Object::get_id() const {
		return id;
	}
	std::string Object::get_name() const {
		return name;
	}
	std::string Object::get_path() const {
		return path;
	}
	Texture* Object::get_sprite() const {
		return sprite;
	}
	bool Object::get_is_persistent() const {
		return is_persistent;
	}
	int Object::get_depth() const {
		return depth;
	}
	Object* Object::get_parent() const {
		return parent;
	}
	std::pair<int,int> Object::get_mask_offset() const {
		return draw_offset;
	}
	bool Object::get_is_pausable() const {
		return is_pausable;
	}
	const std::set<E_EVENT>& Object::get_events() const {
		return implemented_events;
	}

	void Object::set_name(const std::string& _name) {
		name = _name;
	}
	/**
	* Set the relative or absolute resource path.
	* @param _path the new path to use
	* @note If the first character is '$' then the path will be relative to
	*       the Objects resource directory.
	*/
	void Object::set_path(const std::string& _path) {
		path = _path;
		if ((!_path.empty())&&(_path.front() == '$')) {
			path = "resources/objects"+_path.substr(1);
		}
	}
	void Object::set_sprite(Texture* _sprite) {
		sprite = _sprite;
	}
	void Object::set_is_persistent(bool _is_persistent) {
		is_persistent = _is_persistent;
	}
	/**
	* Set the sorting depth for all of the Object's Instances.
	* @param _depth the desired depth, lower values are processed first
	*/
	void Object::set_depth(int _depth) {
		depth = _depth;

		for (auto i : instances) { // Iterate over all current instances and adjust their depth
			i.second->set_depth(depth);
		}
	}
	void Object::set_parent(Object* _parent) {
		parent = _parent;
	}
	void Object::set_mask_offset(const std::pair<int,int>& _offset) {
		draw_offset = _offset;
	}
	void Object::set_is_pausable(bool _is_pausable) {
		is_pausable = _is_pausable;
	}

	/**
	* Add an Instance of this Object to its list.
	* @param index the Instance identifier
	* @param new_instance the Instance pointer
	*
	* @retval 0 success
	* @retval 1 failed since the Instance has an incorrect Object type
	*/
	int Object::add_instance(size_t index, Instance* new_instance) {
		if (!new_instance->has_component(this)) { // Do not attempt to add the instance if it's the wrong type
			return 1;
		}

		// Overwrite any previous instance with the same id
		instances.erase(index);
		instances.emplace(index, new_instance); // Emplace the instance in the list

		return 0;
	}
	void Object::remove_instance(size_t index) {
		instances.erase(index);
	}
	void Object::clear_instances() {
		instances.clear();
	}
	const std::map<size_t,Instance*>& Object::get_instances() const {
		return instances;
	}
	size_t Object::get_instance_amount() const {
		return instances.size();
	}
	/**
	* @param inst_number the position of the Instance in the Object's Instance list, NOT the Instance id
	*
	* @returns a pointer to the nth Instance of this Object or nullptr if not found
	*/
	Instance* Object::get_instance_at(size_t inst_number) const {
		if (inst_number >= instances.size()) { // If the desired Instance position is greater than the list size
			return nullptr;
		}

		size_t i = 0;
		for (auto& inst : instances) { // Iterate over the Instance list, counting how many there are
			if (i == inst_number) {
				return inst.second;
			}
			i++;
		}

		return nullptr;
	}

	/**
	* Update the Instance pointer and data map pointer to the given Instance, usually before a call to one of the Instance's events.
	* @param inst the Instance to update the pointers for
	*/
	void Object::update(Instance* inst) {
		current_instance = inst;
		s = &inst->get_data();
	}
	/**
	* Remove the data for the given Instance.
	* @param inst the Instance to remove data for
	*/
	void Object::destroy(Instance* inst) {
		net::internal::destroy_instance(inst);
	}
	/**
	* Compare Instance collision filter masks.
	* @see Room::check_collision_filter(btBroadphaseProxy*, btBroadphaseProxy*)
	* @param self the first Instance
	* @param other the second Instance
	*
	* @retval true the collision masks and computation types overlap
	* @retval false the collision masks and computation types do not overlap
	*/
	bool Object::check_collision_filter(const Instance* self, const Instance* other) const {
		return self->can_collide(other);
	}
}
