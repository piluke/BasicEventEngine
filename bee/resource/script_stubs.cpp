/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include "script.hpp"

namespace bee {
	Script::Script() {}
	Script::Script(const std::string& _name, const std::string& _path) :
		Script()
	{}
	Script::~Script() {}

	size_t Script::get_amount() {
		return 0;
	}
	Script* Script::get(size_t id) {
		return nullptr;
	}
	Script* Script::get_by_name(const std::string& name) {
		return nullptr;
	}
	Script* Script::add(const std::string& name, const std::string& path) {
		return nullptr;
	}

	bool Script::get_is_new_enabled() {
		return false;
	}
	bool Script::set_is_new_enabled(bool _is_new_enabled) {
		return false;
	}

	E_SCRIPT_TYPE Script::get_type(const std::string& path) {
		return E_SCRIPT_TYPE::INVALID;
	}

	size_t Script::add_to_resources() {
		return -1;
	}
	int Script::reset() {
		return -1;
	}

	std::map<Variant,Variant> Script::serialize() const {
		return {};
	}
	int Script::deserialize(std::map<Variant,Variant>& m) {
		return -1;
	}
	void Script::print() const {}

	size_t Script::get_id() const {
		return -1;
	}
	std::string Script::get_name() const {
		return "";
	}
	std::string Script::get_path() const {
		return "";
	}
	ScriptInterface* Script::get_interface() const {
		return nullptr;
	}
	bool Script::get_is_loaded() const {
		return false;
	}

	void Script::set_name(const std::string& _name) {}
	void Script::set_path(const std::string& _path) {}

	int Script::load() {
		return -1;
	}
	int Script::free() {
		return -1;
	}

	int Script::run_string(const std::string& code, Variant* retval) {
		return -2;
	}
	int Script::run_file(const std::string& filename) {
		return -2;
	}
	int Script::run_func(const std::string& funcname, const Variant& args, Variant* retval) {
		return -2;
	}
}
