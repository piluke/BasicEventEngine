/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_UTIL_H
#define BEE_UTIL_H 1

#include "util/platform.hpp"
#include "util/real.hpp"
#include "util/string.hpp"
#include "util/dates.hpp"
#include "util/collision.hpp"
#include "util/messagebox.hpp"
#include "util/files.hpp"
#include "util/archive.hpp"
#include "util/networking.hpp"
#include "util/curl.hpp"

#include "util/template.hpp" // Include functions which require templates

#endif // BEE_UTIL_H
