/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef TESTS_TEST_H
#define TESTS_TEST_H 1

bool verify_assertions(int, char**);
bool verify_assertions();

#endif // TESTS_TEST_H
