/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef TESTS_UTIL_CURL_H
#define TESTS_UTIL_CURL_H 1

#include "doctest.h" // Include the required unit testing library

#include "../../bee/util/curl.hpp"
#include "../../bee/util/files.hpp"

TEST_SUITE_BEGIN("util");

TEST_CASE("curl") {
	if (!bee::get_option("extensive_assert").i) {
		return;
	}

	const std::string url ("https://toni.blue/BasicEventEngine");

	std::string tmpdir = util::directory_get_temp();
	REQUIRE(util::directory_exists(tmpdir) == true);
	REQUIRE(util::directory_create(tmpdir+"maps/") == 0);
	std::string filename (tmpdir+"maps/new_map.tar.xz");

	bool prev_is_verbose = util::curl::set_is_verbose(false);
	long prev_timeout_connect = util::curl::set_timeout_connect(3);
	long prev_timeout_transfer = util::curl::set_timeout_transfer(3);

	WARN(util::curl::download(url+"/maps/lv_test.tar.xz", filename, [] (curl_off_t total, curl_off_t now) -> int {
		double percent = (double)now/total*100;
		INFO("\rdownloading " << percent << "%...     ");
		return 0;
	}) == 0);
	INFO("\n");
	if (util::file_exists(filename)) {
		WARN(util::curl::upload(url+"/upload.php/new_map.tar.xz?test", filename, [] (curl_off_t total, curl_off_t now) -> int {
			double percent = (double)now/total*100;
			INFO("\ruploading " << percent << "%...     ");
			return 0;
		}) == 0);
		INFO("\n");
	}

	util::curl::set_is_verbose(prev_is_verbose);
	util::curl::set_timeout_connect(prev_timeout_connect);
	util::curl::set_timeout_transfer(prev_timeout_transfer);
}

TEST_SUITE_END();

#endif // TESTS_UTIL_CURL_H
