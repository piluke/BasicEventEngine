/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef TESTS_UTIL_NETWORKING_H
#define TESTS_UTIL_NETWORKING_H 1

#include "doctest.h" // Include the required unit testing library

#include "../../bee/util/networking.hpp"

#include "../../bee/init/gameoptions.hpp"

TEST_SUITE_BEGIN("util");

TEST_CASE("networking/general") {
	if ((!bee::get_option("extensive_assert").i)||(!bee::get_option("is_network_enabled").i)) {
		return;
	}

	REQUIRE(util::network::init() == 0);
	int port = 3054;
	IPaddress* ipa = util::network::resolve_host("127.0.0.1", port);
	REQUIRE(ipa != nullptr);
	CHECK(util::network::get_address(ipa->host) == "127.0.0.1");
	CHECK(util::network::get_port(ipa->port) == port);
	delete ipa;

	TCPsocket tcp = util::network::tcp_open("", port);
	REQUIRE(tcp != nullptr);
	util::network::tcp_close(&tcp);
	CHECK(tcp == nullptr);

	UDPsocket udp = util::network::udp_open(port);
	REQUIRE(udp != nullptr);
	CHECK(util::network::udp_bind(&udp, -1, "127.0.0.1", port) != -1);
	util::network::udp_unbind(&udp, -1);
	util::network::udp_close(&udp);
	CHECK(udp == nullptr);

	util::network::close();
}
TEST_CASE("networking/packets") {
	if ((!bee::get_option("extensive_assert").i)||(!bee::get_option("is_network_enabled").i)) {
		return;
	}

	REQUIRE(util::network::init() == 0);

	UDPpacket* packet = util::network::packet_alloc(64);
	CHECK(packet != nullptr);
	UDPpacket* new_packet = util::network::packet_resize(packet, 128);
	CHECK(packet == new_packet);
	CHECK(packet != nullptr);
	util::network::packet_free(packet);

	UDPpacket** packet_vector = util::network::packet_alloc_vector(4, 64);
	CHECK(packet_vector != nullptr);
	util::network::packet_free_vector(packet_vector);

	util::network::close();
}

TEST_SUITE_END();

#endif // TESTS_UTIL_NETWORKING_H
