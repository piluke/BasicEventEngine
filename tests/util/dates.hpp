/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef TESTS_UTIL_DATES_H
#define TESTS_UTIL_DATES_H 1

#include "doctest.h" // Include the required unit testing library

#include "../../bee/util/dates.hpp"

TEST_SUITE_BEGIN("util");

TEST_CASE("dates/setters") {
	time_t t = util::date::create_datetime(2015, 11, 01, 11, 45, 24);
	CHECK(util::date::date_of(t) == util::date::create_date(2015, 11, 01));
	CHECK(util::date::time_of(t) == util::date::create_time(11, 45, 24));

	CHECK(util::date::inc_year(t, 5) == util::date::create_datetime(2020, 11, 01, 11, 45, 24));
	CHECK(util::date::inc_month(t, 5) == util::date::create_datetime(2015, 16, 01, 11, 45, 24));
	CHECK(util::date::inc_month(t, 5) == util::date::create_datetime(2016, 4, 01, 11, 45, 24));
	CHECK(util::date::inc_week(t, 5) == util::date::create_datetime(2015, 11, 36, 11, 45, 24));
	CHECK(util::date::inc_day(t, 5) == util::date::create_datetime(2015, 11, 06, 11, 45, 24));
	CHECK(util::date::inc_hour(t, 5) == util::date::create_datetime(2015, 11, 01, 16, 45, 24));
	CHECK(util::date::inc_minute(t, 5) == util::date::create_datetime(2015, 11, 01, 11, 50, 24));
	CHECK(util::date::inc_second(t, 5) == util::date::create_datetime(2015, 11, 01, 11, 45, 29));
}
TEST_CASE("dates/getters") {
	time_t t = util::date::create_datetime(2015, 11, 01, 11, 45, 24);
	CHECK(util::date::get_year(t) == 2015);
	CHECK(util::date::get_month(t) == 11);
	CHECK(util::date::get_week(t) == 44);
	CHECK(util::date::get_day(t) == 01);
	CHECK(util::date::get_hour(t) == 11);
	CHECK(util::date::get_minute(t) == 45);
	CHECK(util::date::get_second(t) == 24);
	CHECK(util::date::get_weekday(t) == 0);
	CHECK(util::date::get_day_of_year(t) == 304);
	CHECK(util::date::get_hour_of_year(t) == 7307);
	CHECK(util::date::get_minute_of_year(t) == 438465);
	CHECK(util::date::get_second_of_year(t) == 26307924);

	time_t ot = util::date::create_datetime(1997, 01, 24, 04, 25, 00);
	CHECK(util::date::year_span(t, ot) == doctest::Approx(18.78).epsilon(0.01));
	CHECK(util::date::month_span(t, ot) == doctest::Approx(228.51).epsilon(0.01));
	CHECK(util::date::week_span(t, ot) == doctest::Approx(979.33).epsilon(0.01));
	CHECK(util::date::day_span(t, ot) == doctest::Approx(6855.31).epsilon(0.01));
	CHECK(util::date::hour_span(t, ot) == doctest::Approx(164527.34).epsilon(0.01));
	CHECK(util::date::minute_span(t, ot) == doctest::Approx(9871640.40).epsilon(0.01));
	CHECK(util::date::second_span(t, ot) == doctest::Approx(592298424.00).epsilon(0.01));

	CHECK(util::date::compare_datetime(t, ot) == -1);
	CHECK(util::date::compare_date(t, ot) == -1);
	CHECK(util::date::compare_time(t, ot) == -1);

	CHECK(util::date::datetime_string(t) == "Sun Nov 01 11:45:24 2015");
	CHECK(util::date::date_string(t) == "Sun Nov 01 2015");
	CHECK(util::date::time_string(t) == "11:45:24");

	CHECK(util::date::is_leap_year(t) == false);
	CHECK(util::date::is_leap_year() == true);
	time_t lt = util::date::create_datetime(2016, 02, 16, 15, 37, 20);
	CHECK(util::date::is_leap_year(lt) == true);

	CHECK(util::date::is_today(t) == false);
	CHECK(util::date::is_today(util::date::current_datetime()) == true);
	CHECK(util::date::is_today(util::date::current_date()) == true);
	CHECK(util::date::is_today(util::date::current_time()) == false);

	CHECK(util::date::days_in_month(t) == 30);
	CHECK(util::date::days_in_month(ot) == 31);
	CHECK(util::date::days_in_month(lt) == 29);
	CHECK(util::date::days_in_month(util::date::inc_year(lt, -1)) == 28);
	CHECK(util::date::days_in_year(t) == 365);
	CHECK(util::date::days_in_year(lt) == 366);
}

TEST_CASE("dates/stopwatch") {
	if (!bee::get_option("extensive_assert").i) {
		return;
	}

	auto test1 = [] () {
		long sum = 0;
		for (long i=0; i<1'000'000; ++i) {
			sum += i;
		}
	};
	CHECK(util::stopwatch("test1", test1, false) > doctest::Approx(0.0));
}

TEST_SUITE_END();

#endif // TESTS_UTIL_DATES_H
