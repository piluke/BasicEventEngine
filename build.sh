#!/bin/bash
# Builds BEE with CMake

# Change to the top source directory
cd "$(dirname $0)"

source config.sh # Defines several build variables

download_dependencies()
{
	git submodule update --init --recursive
}
build_dependencies()
{
	set -e

	download_dependencies

	# Build Bullet
	cd lib/bullet3
	cp ../bullet3.CMakeLists.txt ./CMakeLists.txt
	cmake .
	make -j5

	# Configure CPython
	cd ../cpython
	if $PYTHON_ENABLED; then
		EXTRA_CMAKE_OPTS+="-DPYTHON_ENABLED=ON "

		if [ ! -f ./pyconfig.h ]; then
			./configure
			cp pyconfig.h Include/pyconfig.h
		fi
		if [ ! -f ./libpython3.7m.a ]; then
			make -j5
		fi
	else
		EXTRA_CMAKE_OPTS+="-DPYTHON_ENABLED=OFF "
	fi

	cd ../..

	set +e
}
clean_dependencies()
{
	echo "cleaning dependencies..."

	# Clean Bullet
	cd lib/bullet3
	git clean -fd
	git reset HEAD --hard

	# Clean CPython
	cd ../cpython
	rm -f pyconfig.h Include/pyconfig.h libpython3.7m.a python
	rm -rf build
	git clean -fd
	git reset HEAD --hard

	cd ../..
}

clean()
{
	echo "cleaning dir: $1"

	rm -rf "$1"

	rm -f "*.log" "gmon.out" "vgcore.*"
}

assert_build_type()
{
	if [ -f "$2/Makefile" ]; then
		touch "${2}/last_build_type.txt"
		if [[ $1 != $(cat "${2}/last_build_type.txt") ]]; then
			rm "$2/Makefile"
		fi
	fi
}

debug()
{
	echo "debug dir: $1"

	mkdir -p "$1"
	cd "$1"

	assert_build_type "debug" "$1"
	echo -n "debug" > "$1/last_build_type.txt"

	cmake \
		-DCMAKE_BUILD_TYPE=Debug \
		-DEXECUTABLE="$EXECUTABLE" \
		$EXTRA_CMAKE_OPTS \
		..

	if [ "$2" == "nomake" ]; then
		return
	fi

	make -j5
	if [ $? -ne 0 ]; then
		echo "Debug build failed!"
		exit 3
	fi

	cd ..
	if [ "$2" == "norun" ]; then
		return
	fi

	if [ "$2" ]; then
		exec $2 "$1/$EXECUTABLE" $3
	else
		exec "$1/$EXECUTABLE" $3
	fi
}
release()
{
	echo "release dir: $1"

	mkdir -p "$1"
	cd "$1"

	assert_build_type "release" "$1"
	echo -n "release" > "$1/last_build_type.txt"

	cmake \
		-DCMAKE_BUILD_TYPE=Release \
		-DEXECUTABLE="$EXECUTABLE" \
		$EXTRA_CMAKE_OPTS \
		..

	if [ "$2" == "nomake" ]; then
		return
	fi

	make -j5
	if [ $? -ne 0 ]; then
		echo "Release build failed!"
		exit 4
	fi

	strip "$EXECUTABLE"

	cd ..
	if [ "$2" == "norun" ]; then
		return
	fi

	exec "$1/$EXECUTABLE" $3
}

cmd="release"
if [ -n "$1" ] && [[ "$1" != --* ]]; then
	cmd="$1"
fi

build_dir="./build"
if [ -n "$2" ] && [[ "$2" != --* ]]; then
	build_dir="$2"
fi
build_dir="$([[ ${build_dir} = /* ]] && echo "${build_dir}" || echo "$PWD/${build_dir#./}")"

subcmd=""
if [ -n "$3" ] && [[ "$3" != --* ]]; then
	subcmd="$3"
fi

while [ -n "$1" ] && [[ "$1" != --* ]]; do
	shift
done
args="$@"

if [ "$cmd" == "release" ]; then
	build_dependencies
	release "$build_dir" "" "$args"
elif [ "$cmd" == "run" ]; then
	exec "$build_dir/$EXECUTABLE" $args
elif [ "$cmd" == "nomake" ]; then
	build_dependencies
	if [ -f "$build_dir/last_build_type.txt" ]; then
		$(cat "$build_dir/last_build_type.txt") "$build_dir" "nomake"
	else
		release "$build_dir" "nomake"
	fi
elif [ "$cmd" == "norun" ]; then
	build_dependencies
	if [ -f "$build_dir/last_build_type.txt" ]; then
		$(cat "$build_dir/last_build_type.txt") "$build_dir" "norun"
	else
		release "$build_dir" "norun"
	fi
elif [ "$cmd" == "debug" ]; then
	EXTRA_CMAKE_OPTS+="-DCOVERAGE_ENABLED=OFF "

	build_dependencies
	if [ -n "$subcmd" ]; then
		debug "$build_dir" "$subcmd"
	else
		debug "$build_dir" "" "$args"
	fi
elif [ "$cmd" == "valgrind" ]; then
	tool="memcheck --track-origins=yes --leak-check=full"
	if [ -n "$subcmd" ]; then
		tool="$subcmd"
	fi

	build_dependencies
	debug "$build_dir" "norun"
	exec valgrind \
		--tool=$tool \
		--suppressions=valgrind.supp \
		"$build_dir/$EXECUTABLE" $args > valgrind.log
elif [ "$cmd" == "coverage" ]; then
	if ! which gcovr; then
		echo "Can't find gcovr for coverage reporting"
		exit 5
	fi

	EXTRA_CMAKE_OPTS+="-DCOVERAGE_ENABLED=ON "

	build_dependencies
	debug "$build_dir" "norun"

	cd "$build_dir"
	exec make bee_coverage
elif [ "$cmd" == "clean" ]; then
	clean_dependencies
	clean "$build_dir"
else
	echo "Invalid argument: \"$cmd\""
	exit 2
fi

exit 0
