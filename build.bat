@rem ===
@rem Builds BEE with CMake
@rem ===

@rem ---
@rem Config options
@rem ---
@set "executable=engine"

@set "mode=Debug"

@rem ---
@rem Programs
@rem ---
@set "git=C:\Program Files\Git\bin\git.exe"
@set "cmake=C:\Program Files\CMake\bin\cmake.exe"
@set "vcvars=C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvars64.bat"
@set "msbuild=C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\MSBuild.exe"
@set "vcver=14.16.27023"
@set "mslib=C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\%vcver%\bin\Hostx64\x64\lib.exe"
@set "nmake=C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\%vcver%\bin\Hostx64\x64\nmake.exe"

@rem ---
@rem Build Dependencies
@rem ---
@rem Download dependencies
"%git%" submodule update --init --recursive || (exit /b)

@rem Build Bullet
@cd lib\bullet3
@copy /Y ..\bullet3.CMakeLists.txt .\CMakeLists.txt >nul
"%cmake%" -G "Visual Studio 15 2017" -A x64 -DCMAKE_BUILD_TYPE=%mode% . || (cd ..\.. && exit /b)
"%cmake%" --build . --target ALL_BUILD --config %mode% -j 5 || (cd ..\.. && exit /b)
@cd ..

@rem Configure CPython
@cd .\cpython
call .\PCbuild\get_externals.bat
"%msbuild%" /p:Configuration=%mode% /p:Platform=x64 .\PCbuild\pcbuild.sln || (cd ..\.. && exit /b)
@copy /Y .\PC\pyconfig.h .\Include\pyconfig.h >nul
@copy /Y .\PCbuild\amd64\python37.dll ..\..\win\bin\ >nul
@copy /Y .\PCbuild\amd64\python37_d.dll ..\..\win\bin\ >nul
@cd ..

@rem Build libarchive
@cd ..\win\libarchive
"%cmake%" -G "Visual Studio 15 2017" -A x64 -DCMAKE_BUILD_TYPE=%mode% . || (cd ..\.. && exit /b)
@rem Run cmake on the library targets (not including libarchive_test since Visual Studio doesn't support C99)
"%cmake%" --build . --target archive --config %mode% -j 5 || (cd ..\.. && exit /b)
@copy /Y .\bin\%mode%\archive.dll ..\bin\ >nul
@cd ..

@rem Build liblzma.lib
@rem @cd lib
@rem "%mslib%" /def:liblzma.def /out:liblzma.lib /machine:x64
@rem @cd ..

@rem Build libcurl
@cd curl
call buildconf.bat
@cd winbuild
call "%vcvars%"
"%nmake%" /f Makefile.vc mode=dll VC=15 ENABLE_IPV6=no MACHINE=AMD64 || (cd ..\..\.. && exit /b)
@cd ..\..
@copy /Y .\curl\builds\libcurl-vc15-AMD64-release-dll-sspi-winssl\bin\libcurl.dll .\bin\ >nul
@cd ..

@rem ---
@rem Release Build
@rem ---
@mkdir build-win
@cd build-win

@rem Build the game
"%cmake%" -G "Visual Studio 15 2017" -A x64 -DWIN32=1 -DCMAKE_BUILD_TYPE=%mode% -DEXECUTABLE="%executable%" .. || (cd .. && exit /b)
"%cmake%" --build . --target ALL_BUILD --config %mode% -j 5 || (cd ..\.. && exit /b)

@rem Generate and run start.bat
@echo off
echo @cd .. ^

@".\win\bin\%executable%.exe" ^

@pause ^

@exit > .\start.bat
@echo on

start "" /WAIT .\start.bat
@cd ..
